/*global $:false */
function add_remove_tag(tagvalue) {
	var tag_id = "tag_"+tagvalue;
	var user_tags = $('#user_tags').val();
	//alert(user_tags);
	if($("#"+tag_id).hasClass('selected')) {
		$("#"+tag_id).removeClass('selected');
		user_tags = user_tags.replace(tagvalue+",","");
	}
	else {
		$("#" + tag_id).addClass('selected');
		user_tags = user_tags+tagvalue+',';
	}
	$('#user_tags').val(user_tags);
}

$(document).ready(function() {

	$('#profile_pic').change(function(e) {
		var file = $("#profile_pic")[0].files[0];
		
		if(file.size>2200000) {
			$('#pic_update_button').hide();
			$('#err_div').show();
			$('#err_div').html('File size is more then 2 MB.');
			$('#profile_pic').val('');
		}
		else {
			$('#pic_update_button').show();
			$('#err_div').hide();
			$('#err_div').html('');
			$('#image').show();
			$('#capture_pic').val('');
			var reader = new FileReader();
			reader.onload = function(e) {
				$('#image').attr('src', e.target.result);
			}
			reader.readAsDataURL(file);				
		}
		if(file.type=='image/png' || file.type=='image/jpg' || file.type=='image/jpeg' || file.type=='image/gif') {
			$('#pic_update_button').show();
			$('#err_div').hide();
			$('#err_div').html('');
			$('#image').show();
			$('#capture_pic').val('');
			var reader = new FileReader();
			reader.onload = function(e) {
				$('#image').attr('src', e.target.result);
			}
			reader.readAsDataURL(file);			
		}
		else {
			$('#profile_pic').val('');
			$('#pic_update_button').hide();
			$('#err_div').show();
			$('#err_div').html('Please choose a valid image file.');
		}
	});
	
	"use strict";		
	
	$('.accordion-body.in').parent().addClass('active');

	$('.accordion-toggle').click(function() { 
		$('.accordion-group').removeClass('active'); 
		$(this).parent().parent().addClass('active');
	});
	
	$('.dropdown').each(function () {
		$(this).parent().eq(0).hoverIntent({
			timeout: 100,
			over: function () {
				var current = $('.dropdown:eq(0)', this);
				current.slideDown(300);
			},
			out: function () {
				var current = $('.dropdown:eq(0)', this);
				current.fadeOut(100);
			}
		});
	});

	$('.rating li').click(function(){
		if ($(this).hasClass('nr_1')) {
				$(this).siblings('li').removeClass('active');
				$(this).siblings('.nr_1').addClass('active');
			}
		if ($(this).hasClass('nr_2')) {
				$(this).siblings('li').removeClass('active');
				$(this).siblings('.nr_1').addClass('active');
				$(this).siblings('.nr_2').addClass('active');
			}
		if ($(this).hasClass('nr_3')) {
				$(this).siblings('li').removeClass('active');
				$(this).siblings('.nr_1').addClass('active');
				$(this).siblings('.nr_2').addClass('active');
				$(this).siblings('.nr_3').addClass('active');
			}
		if ($(this).hasClass('nr_4')) {
				$(this).siblings('li').removeClass('active');
				$(this).siblings('.nr_1').addClass('active');
				$(this).siblings('.nr_2').addClass('active');
				$(this).siblings('.nr_3').addClass('active');
				$(this).siblings('.nr_4').addClass('active');
			}
		if ($(this).hasClass('nr_5')) {
				$(this).siblings('li').removeClass('active');
				$(this).siblings('.nr_1').addClass('active');
				$(this).siblings('.nr_2').addClass('active');
				$(this).siblings('.nr_3').addClass('active');
				$(this).siblings('.nr_4').addClass('active');
				$(this).addClass('active');
			}
	});
	
	$('.rating li').mouseenter(function(){
		if ($(this).hasClass('nr_1')) {
				$(this).siblings('li').removeClass('hover');
				$(this).siblings('.nr_1').addClass('hover');
			}
		if ($(this).hasClass('nr_2')) {
				$(this).siblings('li').removeClass('hover');
				$(this).siblings('.nr_1').addClass('hover');
				$(this).siblings('.nr_2').addClass('hover');
			}
		if ($(this).hasClass('nr_3')) {
				$(this).siblings('li').removeClass('hover');
				$(this).siblings('.nr_1').addClass('hover');
				$(this).siblings('.nr_2').addClass('hover');
				$(this).siblings('.nr_3').addClass('hover');
			}
		if ($(this).hasClass('nr_4')) {
				$(this).siblings('li').removeClass('hover');
				$(this).siblings('.nr_1').addClass('hover');
				$(this).siblings('.nr_2').addClass('hover');
				$(this).siblings('.nr_3').addClass('hover');
				$(this).siblings('.nr_4').addClass('hover');
			}
		if ($(this).hasClass('nr_5')) {
				$(this).siblings('li').removeClass('hover');
				$(this).siblings('.nr_1').addClass('hover');
				$(this).siblings('.nr_2').addClass('hover');
				$(this).siblings('.nr_3').addClass('hover');
				$(this).siblings('.nr_4').addClass('hover');
				$(this).siblings('.nr_5').addClass('hover');
			}
	});
	
	$('.rating li').mouseleave(function(){
		$(this).siblings('li').removeClass('hover');
		$(this).removeClass('hover');
	});
			
});
