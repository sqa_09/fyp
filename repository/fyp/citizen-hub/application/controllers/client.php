<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Client extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('clientmodel');
		$this->load->library('form_validation');
		$this->load->helper('client');
		$this->load->library('recaptcha');
	}
	
	/*Home page function (Main)*/
	public function index($msg = '') {
		if(is_client_in()) {
			$data['msg'] = $msg;
			$this->load->view('Client/client',$data);
		}
		else {
			$this->login();
		}
	}
	
	public function contact_citizens() {
		$data = "";
		if(is_client_in()) {
			$contact_rules_array = array(
				array(
					'field'   => 'full_name',
					'label'   => 'User Name',
					'rules'   => 'required|min_length[3]'
				),
				array(
					'field'   => 'Subject',
					'label'   => 'Subject',
					'rules'   => 'required|min_length[5]'
				),
				array(
					'field'   => 'message',
					'label'   => 'Message',
					'rules'   => 'required|min_length[10]'
				)
			);
			$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '<button class="close" data-dismiss="alert" type="button">×</button></div>');
			$this->form_validation->set_rules($contact_rules_array);
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('Client/contact_citizen',$data);
			}
			else {
				$username = $this->input->post('full_name');
				$subject = $this->input->post('Subject');
				$message = $this->input->post('message');
				if($u_check = $this->clientmodel->get_specific_data_row('users','UserName',$username)) {
					$c_info = $this->clientmodel->get_specific_data_row('users','user_id',$this->session->userdata('clientid'));
					$this->email->from('info@citizenhub.com', 'Citizen Hub');
					$this->email->to($u_check->UserEmail);
					$this->email->subject($subject.' | Citizen Hub Client Contact');
					$this->email->message('Hi '.$u_check->UserFName.' '.$u_check->UserLName.',		
					
					'.$c_info->UserName.' sent you a message
					
					"'.$message.'"
					
					If you have any questions, feel free to contact us.
					
					Regards,
					
					The Citizen Hub Team
					+92 000 000000
					info@citizenhub.com');
					if(@$this->email->send()) {
						$this->index('<div class="alert alert-success">Your message is sent successfully.<button class="close" data-dismiss="alert" type="button">×</button></div>');
					}
					else {
						$data['done_msg'] = '<div class="alert alert-error">Something went wrong while sending your message. Kindly try again.<button class="close" data-dismiss="alert" type="button">×</button></div>';
					}
				}
				else {
					$data['done_msg'] = '<div class="alert alert-error">UserName you entered does not exist in our system.<button class="close" data-dismiss="alert" type="button">×</button></div>';
				}
				$this->load->view('Client/contact_citizen',$data);
			}
		}
		else
			$this->load->view('Client/login',$data);
	}
	
	public function people_auto_complete() {
		$search_key  = $_GET['term'];
		$search_results = $this->clientmodel->search_people('users',$search_key);
		foreach($search_results as $result) {
			$results[] = array('label' => $result['UserName']);
		}
		echo json_encode($results);
	} 
	
	public function makenewpost() {
		$data = "";
		if(is_client_in()) {
            $data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();
			$data['all_tags'] = $this->clientmodel->get_active_data('tags','TagStatus');
			$this->load->view('Client/makepost',$data);
		}
		else
			$this->load->view('Client/login',$data);
	}
	
	public function submitpost() {
		$data = "";
		if(is_client_in()) {
            $data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();
			$post_rules_array = array(
				array(
					'field'   => 'news_heading',
					'label'   => 'Heading',
					'rules'   => 'required|min_length[5]'
				),
				array(
					'field'   => 'news_description',
					'label'   => 'Description',
					'rules'   => 'required|min_length[10]'
				)
			);
			$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '<button class="close" data-dismiss="alert" type="button">×</button></div>');
			$this->form_validation->set_rules($post_rules_array);
			$resp = $this->recaptcha->recaptcha_check_answer(
						$_SERVER["REMOTE_ADDR"],
						$_POST["recaptcha_challenge_field"],
						$_POST["recaptcha_response_field"]);
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('Client/makepost',$data);
			}
			if(!$this->recaptcha->getIsValid()) {
				$data['post_error_msg'] = '<div class="alert alert-error">Captcha you entered is not valid! Enter exact words.'.
					'<button class="close" data-dismiss="alert" type="button">×</button></div>';
				$this->load->view('Client/makepost',$data);
			}
			else {
				$news_category = $this->input->post("news_category");
				$news_heading = htmlspecialchars($this->input->post("news_heading"));
				$news_description = $this->input->post("news_description");
				$news_image = 'news_default.jpg';
				$news_tags = $this->input->post("user_tags");
				$news_link = $this->input->post("news_link");
				$temp_str = $news_description;
				$tag_str = ',';
				$matches = null;
				$news_tags = $news_tags . $this->auto_tagging($news_description);
				if (preg_match_all('/(?!\b)(@\w+\b)/',$temp_str,$matches,1)) {
					foreach($matches[1] as $tag) {
						$tag = ucwords(strtolower($tag));
						$tag_check = $this->clientmodel->get_specific_data_row('tags','TagName',$tag);
						if($tag_check) {
							$tag_str = $tag_str . $tag_check->TagID . ',';
							$tagocc = $tag_check->TagOccurance;
							$tagocc++;
							$update_tag_occ = array(
								'TagOccurance' => $tagocc
							);
							$this->clientmodel->update_record('tags',$update_tag_occ,'TagID',$tag_check->TagID);
						}
						else {
							$tag_insert_data = array(
								'TagName'=> $tag,
								'TagType'=> 3,
								'TagStatus'=> 1,
								'TagOccurance' => 1
							);
							if($tag_key = $this->citizenmodel->insert_record_key('tags',$tag_insert_data)) {
								$tag_str = $tag_str . $tag_key . ',';
							}
						}
					}
					$news_tags = $news_tags . $tag_str;
				}
				$news_tags = implode(',',array_unique(explode(',', $news_tags)));
				if((isset($_FILES['news_image']) && !empty($_FILES['news_image']['name'])) || ($this->input->post('capture_pic') != '')) {
					if(isset($_FILES['news_image']) && !empty($_FILES['news_image']['name'])) {
						$config = array(
							'allowed_types' => 'jpeg|jpg|png|gif|bmp',
							'upload_path' => './data/images/',
							'max_size' => 2048,
							'encrypt_name' => TRUE,
							'overwrite' => FALSE,
						);
						$this->upload->initialize($config);
						if(!$this->upload->do_upload('news_image')) 
						{
							if (!empty($_FILES['news_image']['name'])) {
								$data['post_error_msg'] = '<div class="alert alert-error">'. $this->upload->display_errors().'
								<button class="close" data-dismiss="alert" type="button">×</button></div>';
							}
						}
						else 
						{
							$img_info = $this->upload->data();
							$news_image = $img_info['file_name'];
						}
					}
					else {
						$imgData = base64_decode($_POST['capture_pic']);	
						$path = './data/images/';

						$random_name = $this->randomstring(10);
						$file = $random_name.'.jpg';
						$file_path = $path . $file;
						$fp = fopen($file_path,'w');
						fwrite($fp, $imgData);
						fclose($fp);
						$news_image = $file;
					}
				}
				$insert_data = array(
					'news_category'=> $news_category,
					'news_tag'=> $news_tags,
					'news_heading'=> $news_heading,
					'news_text'=> $news_description,
					'updated_by'=> $this->session->userdata('clientid'),
					'news_link'=> $news_link,
					'main_image'=> $news_image
				);
				if($n_id = $this->clientmodel->insert_record_key('news_articles',$insert_data)) {
					$data['post_error_msg'] = '<div class="alert alert-success">Successfully Posted.'.
					'<button class="close" data-dismiss="alert" type="button">×</button></div>';
					//$this->load->view('makepost',$data);
					$user_lat = $this->session->userdata('user_lat');
					$user_lng = $this->session->userdata('user_lng');
					$users_for_notif = $this->clientmodel->get_two_columns_data_array('users','UserLocationLat',$user_lat,'UserLocationLng',$user_lng);
					foreach($users_for_notif as $us) {
						
						if($us['user_id']!=$this->session->userdata('clientid')) {
							$insert_notification = array(
								'user_id' => $us['user_id'],
								'poster_id' => $this->session->userdata('clientid'),
								'news_id' => $n_id,
								'notification_type' => 'area_news',
								'status' => 1
							);
							$this->clientmodel->insert_record('user_notifications',$insert_notification);
						}
					}
					$tags_id = explode(',',$news_tags);
					foreach($tags_id as $id) {
						$search_id = ','.$id.',';
						$users_for_notif = $this->clientmodel->like_query_data_array('users','UserTags',$search_id);
						foreach($users_for_notif as $us) {
							if($us['user_id']!=$this->session->userdata('clientid')) {
								$insert_notification = array(
									'user_id' => $us['user_id'],
									'poster_id' => $this->session->userdata('clientid'),
									'news_id' => $n_id,
									'notification_type' => 'tag_interest',
									'status' => 1
								);
								$this->clientmodel->insert_record('user_notifications',$insert_notification);
							}
						}
					}
					//echo '<script type="text/javascript">alert("Your post is succesfully posted.")/script>';
					$this->session->set_flashdata('post_valid', '<div class="alert alert-success">Your post is succesfully posted</div>');
					$url = base_url().'client/manage_posts';
					redirect($url, 'refresh');
				}
				else {
					$data['post_error_msg'] = '<div class="alert alert-error">Oops! Something went Wrong, Kindly try again.'.
					'<button class="close" data-dismiss="alert" type="button">×</button></div>';
					$this->load->view('Client/makepost',$data);
				}
			}
		}
		else
			$this->load->view('Client/login',$data);
	}
	
	public function search_people() {
		$search_key = $this->input->post('search_key');
		$search_results = $this->clientmodel->search_people('users',$search_key);
		foreach($search_results as $result) {
			echo '<div class="result" id="pres_'.$result['user_id'].
			'" onclick="javascript:add_people_to_text('.$result['user_id'].')">'.$result['UserName'].'</div>';
		}
	}
	
	public function search_tags() {
		$search_key = $this->input->post('search_key');
		$search_results = $this->clientmodel->like_query_data_array('tags','TagName',$search_key);
		foreach($search_results as $result) {
			echo '<div class="result" id="res_'.$result['TagID'].
			'" onclick="javascript:add_tag_to_text('.$result['TagID'].')">'.$result['TagName'].'</div>';
		}
	}
	
	function auto_tagging($auto_tag_text = '') {
		$text = strtolower($auto_tag_text);
		$text = str_replace('.',' ',$text);
		$text = str_replace('"',' ',$text);
		$tags = $this->clientmodel->get_active_data('tags','TagStatus');
		$auto_tags = "";
		foreach($tags as $tag) {
			$temp = str_replace('@','',strtolower($tag['TagName']));
			$temp = trim($temp);
			$pos = strpos($text, $temp);
			if ($pos !== false) {
				if(substr_count($text,$temp)>=1) {
					$auto_tags .= $tag['TagID'].',';
					$tagocc = $tag['TagOccurance'];
					$tagocc++;
					$update_tag_occ = array(
						'TagOccurance' => $tagocc
					);
					$this->clientmodel->update_record('tags',$update_tag_occ,'TagID',$tag['TagID']);
				}
			}
		}
		return $auto_tags;
	}
	
	public function manage_posts() {
		if(is_client_in()) {
			$key = $this->session->userdata('clientid');
			$data['title'] = 'Post Management';
			$posts = $this->clientmodel->get_specific_data_sorted_array('news_articles','updated_by',$key,'upload_time','DESC');
			$posts_view = '';
			if($posts) {
				$posts_view .= '<div class="data-item" id="light-box-bg">
				<div class="block" style="width:420px;" id="left_align">
					<span><strong>Title</strong></span>
				</div>
				<div class="block" style="width:100px;" id="left_align">
					<span><strong>Posted By</strong></span>
				</div>
				<div class="block" style="width:150px;" id="left_align">
					<span><strong>Posted On</strong></span>
				</div>
				<div class="block" style="width:100px;" id="left_align">
					<span><strong>Endorsments</strong></span>
				</div>
				<div class="block" style="width:80px;">
					<span><strong>Rating</strong></span>
				</div>
				<div class="block" style="width:80px;">
					<span><strong>Delete</strong></span>
				</div>
                </div>';
			}
			$i = 0;
			foreach($posts as $post) {
				if($i%2==0)
					$posts_view .= '<div id="post_'.$post['news_id'].'"><div class="data-item" id="light-bg">';
				else
					$posts_view .= '<div id="post_'.$post['news_id'].'"><div class="data-item">';
				$post_temp = $this->clientmodel->get_comments_sorted_array('news_id',$post['news_id'],'1','news_id','DESC');
				$user_temp = $this->clientmodel->get_specific_data_row('users','user_id',$post['updated_by']);
				$posts_view .= '<div class="block" id="left_align" style="width:420px; text-align:left">'.$post['news_heading'].'</div>
				<div class="block" id="left_align" style="width:100px;">'.$user_temp->UserName.'</div>
				<div class="block" id="left_align" style="width:150px;">'.$post['upload_time'].'</div>
				<div class="block" id="left_align" style="width:100px;">'.count($post_temp).'</div>
				<div class="block" style="width:80px;">'.$post['overall_rating'].'</div>
				<div class="block" style="width:80px;">';
				$posts_view .= '<img id="news_icon_'.$post['news_id'].'" onclick="javascript:delete_reported_post('.$post['news_id'].',0)" src="'.base_url().'html/img/delete_icon.png" />';
				$posts_view .= '</div>
				</div></div>';
				$i++;
			}
			$data['user_view'] = $posts_view;
			$this->load->view('Client/manage',$data);
		}
		else
			$this->login();
	}
	
	public function post_delete() {
		if(is_client_in()) {
			$news_id = $this->input->post('news_id');
			if($this->clientmodel->delete_record('news_articles','news_id',$news_id))
				if($this->clientmodel->delete_record('spam_reports','news_id',$news_id)) {
					$d = $this->clientmodel->delete_record('user_notifications','news_id',$news_id);
					return 1;
				}
				else
					return 0;
			else
				return 0;
		}
		else
			return 0;
	}
	
	
	public function login() {
		if(!is_client_in()) {
			$data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();
			$login_rules_array = array(
				array(
					'field'   => 'admin_user_log',
					'label'   => 'Username',
					'rules'   => 'required|min_length[4]'
				),
				array(
					'field'   => 'admin_pass_log',
					'label'   => 'Password',
					'rules'   => 'required|min_length[5]'
				)
			);
			$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '<button class="close" data-dismiss="alert" type="button">×</button></div>');
			$this->form_validation->set_rules($login_rules_array);
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('Client/login',$data);
			}
			else {
				$resp = $this->recaptcha->recaptcha_check_answer(
                                        $_SERVER["REMOTE_ADDR"],
                                        $_POST["recaptcha_challenge_field"],
                                        $_POST["recaptcha_response_field"]);
				if(!$this->recaptcha->getIsValid()) {
					$data['not_valid'] = '<div class="alert alert-error">Captcha you entered is incorrect! Re-Enter and try again.<button class="close" data-dismiss="alert" type="button">×</button></div>';
					$this->load->view('Client/login',$data);
				}
				else {
					$username = htmlspecialchars($this->input->post("admin_user_log"));
					$password = htmlspecialchars($this->input->post("admin_pass_log"));
					$result = $this->clientmodel->client_login($username,md5($password));
					if(count($result)==1) {
						$this->session->set_userdata('clientname',$result->UserName);
						$this->session->set_userdata('clientpass',$result->UserPass);
						$this->session->set_userdata('clientid',$result->user_id);
						$this->index();
					}
					else {
						$data['not_valid'] = '<div class="alert alert-error">Username or Password is Incorrect.<button class="close" data-dismiss="alert" type="button">×</button></div>';
						$this->load->view('Client/login',$data);
					}
				}
			}
		}
		else
			$this->index();
	}
	
	public function logout() {
		$this->session->unset_userdata('clientname');
		$this->session->unset_userdata('clientpass');
		$this->session->unset_userdata('clientid');
		$this->session->sess_destroy();
		redirect(base_url().'client/login','location');
	}
}
