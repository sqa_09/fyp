<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class notification extends CI_Controller {

	/**
	 * Citizen Class (Index Page for this controller).
	 *
	 * Maps to the following URL
	 * 		http://citizenhub.com/citizen/home
	 *	- or -
	 * 		http://citizenhub.com/index.php/citizen/home
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://citizenhub.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /citizen/<method_name>
	 */
	 
	/*Constructer of citizen class*/
	function __construct() {
		parent::__construct();
		$this->load->model('citizenmodel');
		$this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->helper('user');
		$this->load->library('recaptcha');
	}
	public function make_notifications() {
		if($us->updated_by!=$user_id) {
			$insert_notification = array(
				'user_id' => $us->updated_by,
				'poster_id' => $user_id,
				'news_id' => $news_id,
				'notification_type' => 'endorsment',
				'status' => 1
			);
			$this->citizenmodel->insert_record('user_notifications',$insert_notification);
		}
	}
}
