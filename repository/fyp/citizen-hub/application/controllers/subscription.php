<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class subscription extends CI_Controller {

	/**
	 * Citizen Class (Index Page for this controller).
	 *
	 * Maps to the following URL
	 * 		http://citizenhub.com/citizen/home
	 *	- or -
	 * 		http://citizenhub.com/index.php/citizen/home
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://citizenhub.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /citizen/<method_name>
	 */
	 
	/*Constructer of citizen class*/
	function __construct() {
		parent::__construct();
		$this->load->model('citizenmodel');
		$this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->helper('user');
		$this->load->library('recaptcha');
	}
	public function subscribe_post() {
		$news_id = $this->input->post('news_id');
		$sub_type = $this->input->post('subscribe_status');
		$user_id = $this->session->userdata('userid');
		if($sub_type==2) {
			$delete_data = array(
				'news_id'=> $news_id,
				'user_id'=> $user_id
			);
			if($this->citizenmodel->delete_record_array('user_subscription',$delete_data))
				echo '3';
			else
				echo '0';
		}
		else {
			$insert_data = array(
				'news_id'=> $news_id,
				'user_id'=> $user_id
			);
			if($this->citizenmodel->insert_record('user_subscription',$insert_data))
				echo '1';
			else
				echo '0';
		}
	}
}
