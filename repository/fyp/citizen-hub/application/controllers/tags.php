<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tags extends CI_Controller {

	/**
	 * Citizen Class (Index Page for this controller).
	 *
	 * Maps to the following URL
	 * 		http://citizenhub.com/citizen/home
	 *	- or -
	 * 		http://citizenhub.com/index.php/citizen/home
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://citizenhub.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /citizen/<method_name>
	 */
	 
	/*Constructer of citizen class*/
	function __construct() {
		parent::__construct();
		$this->load->model('citizenmodel');
		$this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->helper('user');
		$this->load->library('recaptcha');
	}
	function get_tags_from_ids($key = '') {
		$tags_id = explode(',',$key);
		$tag_str = '';
		$ci =& get_instance();
		$ci->load->database();
		foreach($tags_id as $id) {
			$tag = $ci->citizenmodel->get_specific_data_row('tags','TagID',$id);
			if($tag)
				$tag_str = $tag_str . '<li><a href="'.base_url().'citizen/search/tag/'.$tag->TagID.'/'.$tag->TagName.'">'.$tag->TagName.'</a></li>';
		}
		if($tag_str=='')
			return "There are no tags in this post.";
		else
			return $tag_str;
	}
	
	function get_all_tags() {
		$ci =& get_instance();
		$ci->load->database();
		//$tags = $ci->citizenmodel->get_active_data('tags','TagStatus');
		$tags = $ci->citizenmodel->get_active_sort_data_limit('tags','TagStatus','TagOccurance','DESC',10);
		return $tags;
	}
}
