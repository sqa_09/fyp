<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Posts extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('citizenmodel');
		$this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->helper('user');
		$this->load->library('recaptcha');
	}
	public function index() {
		$data = "";
		if(is_logged_in()) {
        	$this->load->view('makepost',$data);
		}
		else
			$this->load->view('login',$data);
	}
	
	public function makepost() {
		$data = "";
		if(is_logged_in()) {
            $data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();
			$data['all_tags'] = $this->citizenmodel->get_active_data('tags','TagStatus');
			$this->load->view('makepost',$data);
		}
		else
			$this->load->view('login',$data);
	}
	
	public function submitpost() {
		$data = "";
		if(is_logged_in()) {
            $data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();
			$post_rules_array = array(
				array(
					'field'   => 'news_heading',
					'label'   => 'Heading',
					'rules'   => 'required|min_length[5]'
				),
				array(
					'field'   => 'news_description',
					'label'   => 'Description',
					'rules'   => 'required|min_length[10]'
				)
			);
			$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '<button class="close" data-dismiss="alert" type="button">×</button></div>');
			$this->form_validation->set_rules($post_rules_array);
			$resp = $this->recaptcha->recaptcha_check_answer(
						$_SERVER["REMOTE_ADDR"],
						$_POST["recaptcha_challenge_field"],
						$_POST["recaptcha_response_field"]);
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('makepost',$data);
			}
			if(!$this->recaptcha->getIsValid()) {
				$data['post_error_msg'] = '<div class="alert alert-error">Captcha you entered is not valid! Enter exact words.'.
					'<button class="close" data-dismiss="alert" type="button">×</button></div>';
				$this->load->view('makepost',$data);
			}
			else {
				$news_category = $this->input->post("news_category");
				$news_heading = htmlspecialchars($this->input->post("news_heading"));
				$news_description = $this->input->post("news_description");
				$news_image = 'news_default.jpg';
				$news_tags = $this->input->post("user_tags");
				$news_link = $this->input->post("news_link");
				$temp_str = $news_description;
				$tag_str = ',';
				$matches = null;
				$news_tags = $news_tags . $this->auto_tagging($news_description);
				if (preg_match_all('/(?!\b)(@\w+\b)/',$temp_str,$matches,1)) {
					foreach($matches[1] as $tag) {
						$tag = ucwords(strtolower($tag));
						$tag_check = $this->citizenmodel->get_specific_data_row('tags','TagName',$tag);
						if($tag_check) {
							$tag_str = $tag_str . $tag_check->TagID . ',';
							$tagocc = $tag_check->TagOccurance;
							$tagocc++;
							$update_tag_occ = array(
								'TagOccurance' => $tagocc
							);
							$this->citizenmodel->update_record('tags',$update_tag_occ,'TagID',$tag_check->TagID);
						}
						else {
							$tag_insert_data = array(
								'TagName'=> $tag,
								'TagType'=> 3,
								'TagStatus'=> 1,
								'TagOccurance' => 1
							);
							if($tag_key = $this->citizenmodel->insert_record_key('tags',$tag_insert_data)) {
								$tag_str = $tag_str . $tag_key . ',';
							}
						}
					}
					$news_tags = $news_tags . $tag_str;
				}
				$news_tags = implode(',',array_unique(explode(',', $news_tags)));
				if((isset($_FILES['news_image']) && !empty($_FILES['news_image']['name'])) || ($this->input->post('capture_pic') != '')) {
					if(isset($_FILES['news_image']) && !empty($_FILES['news_image']['name'])) {
						$config = array(
							'allowed_types' => 'jpeg|jpg|png|gif|bmp',
							'upload_path' => './data/images/',
							'max_size' => 2048,
							'encrypt_name' => TRUE,
							'overwrite' => FALSE,
						);
						$this->upload->initialize($config);
						if(!$this->upload->do_upload('news_image')) 
						{
							if (!empty($_FILES['news_image']['name'])) {
								$data['post_error_msg'] = '<div class="alert alert-error">'. $this->upload->display_errors().'
								<button class="close" data-dismiss="alert" type="button">×</button></div>';
							}
						}
						else 
						{
							$img_info = $this->upload->data();
							$news_image = $img_info['file_name'];
						}
					}
					else {
						$imgData = base64_decode($_POST['capture_pic']);	
						$path = './data/images/';
						$random_name = $this->randomstring(10);
						$file = $random_name.'.jpg';
						$file_path = $path . $file;
						$fp = fopen($file_path,'w');
						fwrite($fp, $imgData);
						fclose($fp);
						$news_image = $file;
					}
				}
				$insert_data = array(
					'news_category'=> $news_category,
					'news_tag'=> $news_tags,
					'news_heading'=> $news_heading,
					'news_text'=> $news_description,
					'updated_by'=> $this->session->userdata('userid'),
					'news_link'=> $news_link,
					'main_image'=> $news_image
				);
				if($n_id = $this->citizenmodel->insert_record_key('news_articles',$insert_data)) {
					$data['post_error_msg'] = '<div class="alert alert-success">Successfully Posted.'.
					'<button class="close" data-dismiss="alert" type="button">×</button></div>';
					//$this->load->view('makepost',$data);
					$user_lat = $this->session->userdata('user_lat');
					$user_lng = $this->session->userdata('user_lng');
					$users_for_notif = $this->citizenmodel->get_two_columns_data_array('users','UserLocationLat',$user_lat,'UserLocationLng',$user_lng);
					foreach($users_for_notif as $us) {
						
						if($us['user_id']!=$this->session->userdata('userid')) {
							$insert_notification = array(
								'user_id' => $us['user_id'],
								'poster_id' => $this->session->userdata('userid'),
								'news_id' => $n_id,
								'notification_type' => 'area_news',
								'status' => 1
							);
							$this->citizenmodel->insert_record('user_notifications',$insert_notification);
						}
					}
					$tags_id = explode(',',$news_tags);
					foreach($tags_id as $id) {
						$search_id = ','.$id.',';
						$users_for_notif = $this->citizenmodel->like_query_data_array('users','UserTags',$search_id);
						foreach($users_for_notif as $us) {
							if($us['user_id']!=$this->session->userdata('userid')) {
								$insert_notification = array(
									'user_id' => $us['user_id'],
									'poster_id' => $this->session->userdata('userid'),
									'news_id' => $n_id,
									'notification_type' => 'tag_interest',
									'status' => 1
								);
								$this->citizenmodel->insert_record('user_notifications',$insert_notification);
							}
						}
					}
					//echo '<script type="text/javascript">alert("Your post is succesfully posted.")/script>';
					$this->session->set_flashdata('post_valid', '<div class="alert alert-success">Your post is succesfully posted</div>');
					$url = base_url().'citizen/';
					redirect($url, 'refresh');
				}
				else {
					$data['post_error_msg'] = '<div class="alert alert-error">Oops! Something went Wrong, Kindly try again.'.
					'<button class="close" data-dismiss="alert" type="button">×</button></div>';
					$this->load->view('makepost',$data);
				}
			}
		}
		else
			$this->load->view('login',$data);
	}
	
	public function search_people() {
		$search_key = $this->input->post('search_key');
		$search_results = $this->citizenmodel->search_people('users',$search_key);
		foreach($search_results as $result) {
			echo '<div class="result" id="pres_'.$result['user_id'].
			'" onclick="javascript:add_people_to_text('.$result['user_id'].')">'.$result['UserName'].'</div>';
		}
	}
	
	public function search_tags() {
		$search_key = $this->input->post('search_key');
		$search_results = $this->citizenmodel->like_query_data_array('tags','TagName',$search_key);
		foreach($search_results as $result) {
			echo '<div class="result" id="res_'.$result['TagID'].
			'" onclick="javascript:add_tag_to_text('.$result['TagID'].')">'.$result['TagName'].'</div>';
		}
	}
	
	public function endorsepost() {
		if(is_logged_in()) {
			$news_id = $this->input->post('news_id');
			$endorse_type = $this->input->post('endorse_type');
			$user_id = $this->session->userdata('userid');
			if($endorse_type == 2)
			{
				$comment_text = htmlspecialchars($this->input->post("postcomment"));
				$no_of_comments = htmlspecialchars($this->input->post("no_of_comments"));
				$no_of_comments ++;
				$insert_data = array(
					'news_id'=> $news_id,
					'endrosed_by'=> $user_id,
					'edrosement_type'=> $endorse_type,
					'comments'=> $comment_text
				);
				if($last_comment_key = $this->citizenmodel->insert_record_key('endorsements',$insert_data)) {
					$update_data = array(
						'total_comments'=> $no_of_comments
					);
					$asdf = $this->citizenmodel->update_record('news_articles',$update_data,'news_id',$news_id);
					$comment = $this->citizenmodel->get_specific_data_row('endorsements','endrosement_id',$last_comment_key);
					$us = $this->citizenmodel->get_specific_data_row('news_articles','news_id',$news_id);
					if($us->updated_by!=$user_id) {
						$insert_notification = array(
							'user_id' => $us->updated_by,
							'poster_id' => $user_id,
							'news_id' => $news_id,
							'notification_type' => 'comment',
							'status' => 1
						);
						$this->citizenmodel->insert_record('user_notifications',$insert_notification);
					}
					$post_time= strtotime($comment->post_time);
					$comment_by = $comment->endrosed_by;
					echo '<li><span class="comment"><span class="date"><span>('.date("H", $post_time).':'.date("i", $post_time).')<br />'.date("d", $post_time).' '.date("M", $post_time).' '.date("Y", $post_time).'</span></span><span class="content"><span class="title"><b>'.get_username($comment_by).'</b>, wrote:</span><br>'.$comment->comments.'</span><span class="clear"></span></span></li>';
				}
			}
			else {
				$insert_data = array(
					'news_id'=> $news_id,
					'endrosed_by'=> $user_id,
					'edrosement_type'=> $endorse_type
				);
				if($this->citizenmodel->insert_record('endorsements',$insert_data)) {
					$us = $this->citizenmodel->get_specific_data_row('news_articles','news_id',$news_id);
					if($us->updated_by!=$user_id) {
						$insert_notification = array(
							'user_id' => $us->updated_by,
							'poster_id' => $user_id,
							'news_id' => $news_id,
							'notification_type' => 'endorsment',
							'status' => 1
						);
						$this->citizenmodel->insert_record('user_notifications',$insert_notification);
					}
					echo '1';
				}
				else
					echo '0';
			}
		}
		else
			echo '0';
	}
	
	public function subscribepost() {
		$news_id = $this->input->post('news_id');
		$sub_type = $this->input->post('subscribe_status');
		$user_id = $this->session->userdata('userid');
		if($sub_type==2) {
			$delete_data = array(
				'news_id'=> $news_id,
				'user_id'=> $user_id
			);
			if($this->citizenmodel->delete_record_array('user_subscription',$delete_data))
				echo '3';
			else
				echo '0';
		}
		else {
			$insert_data = array(
				'news_id'=> $news_id,
				'user_id'=> $user_id
			);
			if($this->citizenmodel->insert_record('user_subscription',$insert_data))
				echo '1';
			else
				echo '0';
		}
	}
	
	public function report_abuse() {
		if(is_logged_in()) {
			$user_id = $this->session->userdata('userid');
			$news_id = $this->input->post('news_id');
			$insert_data = array(
				'news_id'=> $news_id,
				'spam_reporter'=> $user_id,
				'spam_status'=> 1
			);
			if($this->citizenmodel->insert_record('spam_reports',$insert_data))
				echo 1;
			else
				echo 0;
		}
	}
	
	public function ratepost() {
		if(is_logged_in()) {
			$news_id = intval($_POST['idBox']);
			$rate = floatval($_POST['rate']);
			$user_id = $this->session->userdata('userid');
			$insert_data = array(
				'news_id'=> $news_id,
				'endrosed_by'=> $user_id,
				'edrosement_type'=> 3,
				'ratings' => $rate
			);
			if($this->citizenmodel->insert_record('endorsements',$insert_data)) {
				$rating = $this->citizenmodel->get_rating($news_id,3);
				$update_data = array(
					'overall_rating'=> $rating
				);
				if($this->citizenmodel->update_record('news_articles',$update_data,'news_id',$news_id))
					$success = true;
			}
		}
	}
	
	function auto_tagging($auto_tag_text = '') {
		$text = strtolower($auto_tag_text);
		$text = str_replace('.',' ',$text);
		$text = str_replace('"',' ',$text);
		$tags = $this->citizenmodel->get_active_data('tags','TagStatus');
		$auto_tags = ",";
		foreach($tags as $tag) {
			$temp = str_replace('@','',strtolower($tag['TagName']));
			$temp = trim($temp);
			$pos = strpos($text, $temp);
			if ($pos !== false) {
				if(substr_count($text,$temp)>=1) {
					$auto_tags .= $tag['TagID'].',';
					$tagocc = $tag['TagOccurance'];
					$tagocc++;
					$update_tag_occ = array(
						'TagOccurance' => $tagocc
					);
					$this->citizenmodel->update_record('tags',$update_tag_occ,'TagID',$tag['TagID']);
				}
			}
		}
		return $auto_tags;
	}
	
	/*Generates a random string of given input length*/
	function randomstring($length = 15) {
		$charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		$str = '';
		$count = strlen($charset);
		while ($length--) {
			$str .= $charset[mt_rand(0, $count-1)];
		}
		return $str;
	}
	
}