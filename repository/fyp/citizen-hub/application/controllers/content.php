<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class content extends CI_Controller {

	/**
	 * Citizen Class (Index Page for this controller).
	 *
	 * Maps to the following URL
	 * 		http://citizenhub.com/citizen/home
	 *	- or -
	 * 		http://citizenhub.com/index.php/citizen/home
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://citizenhub.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /citizen/<method_name>
	 */
	 
	/*Constructer of citizen class*/
	function __construct() {
		parent::__construct();
		$this->load->model('citizenmodel');
		$this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->helper('user');
		$this->load->library('recaptcha');
	}
	public function add_news() {
		$data = "";
		if(is_logged_in()) {
            $data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();
			$post_rules_array = array(
				array(
					'field'   => 'news_heading',
					'label'   => 'Heading',
					'rules'   => 'required|min_length[5]'
				),
				array(
					'field'   => 'news_description',
					'label'   => 'Description',
					'rules'   => 'required|min_length[10]'
				)
			);
			$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '<button class="close" data-dismiss="alert" type="button">×</button></div>');
			$this->form_validation->set_rules($post_rules_array);
			$resp = $this->recaptcha->recaptcha_check_answer(
						$_SERVER["REMOTE_ADDR"],
						$_POST["recaptcha_challenge_field"],
						$_POST["recaptcha_response_field"]);
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('makepost',$data);
			}
			if(!$this->recaptcha->getIsValid()) {
				$data['post_error_msg'] = '<div class="alert alert-error">Captcha you entered is not valid! Enter exact words.'.
					'<button class="close" data-dismiss="alert" type="button">×</button></div>';
				$this->load->view('makepost',$data);
			}
			else {
				$news_category = $this->input->post("news_category");
				$news_heading = htmlspecialchars($this->input->post("news_heading"));
				$news_description = $this->input->post("news_description");
				$news_image = 'news_default.jpg';
				$news_tags = $this->input->post("user_tags");
				$news_link = $this->input->post("news_link");
				$temp_str = $news_description;
				$tag_str = ',';
				$matches = null;
				$news_tags = $news_tags . $this->auto_tagging($news_description);
				if (preg_match_all('/(?!\b)(@\w+\b)/',$temp_str,$matches,1)) {
					foreach($matches[1] as $tag) {
						$tag = ucwords(strtolower($tag));
						$tag_check = $this->citizenmodel->get_specific_data_row('tags','TagName',$tag);
						if($tag_check) {
							$tag_str = $tag_str . $tag_check->TagID . ',';
							$tagocc = $tag_check->TagOccurance;
							$tagocc++;
							$update_tag_occ = array(
								'TagOccurance' => $tagocc
							);
							$this->citizenmodel->update_record('tags',$update_tag_occ,'TagID',$tag_check->TagID);
						}
						else {
							$tag_insert_data = array(
								'TagName'=> $tag,
								'TagType'=> 3,
								'TagStatus'=> 1,
								'TagOccurance' => 1
							);
							if($tag_key = $this->citizenmodel->insert_record_key('tags',$tag_insert_data)) {
								$tag_str = $tag_str . $tag_key . ',';
							}
						}
					}
					$news_tags = $news_tags . $tag_str;
				}
				$news_tags = implode(',',array_unique(explode(',', $news_tags)));
				if((isset($_FILES['news_image']) && !empty($_FILES['news_image']['name'])) || ($this->input->post('capture_pic') != '')) {
					if(isset($_FILES['news_image']) && !empty($_FILES['news_image']['name'])) {
						$config = array(
							'allowed_types' => 'jpeg|jpg|png|gif|bmp',
							'upload_path' => './data/images/',
							'max_size' => 2048,
							'encrypt_name' => TRUE,
							'overwrite' => FALSE,
						);
						$this->upload->initialize($config);
						if(!$this->upload->do_upload('news_image')) 
						{
							if (!empty($_FILES['news_image']['name'])) {
								$data['post_error_msg'] = '<div class="alert alert-error">'. $this->upload->display_errors().'
								<button class="close" data-dismiss="alert" type="button">×</button></div>';
							}
						}
						else 
						{
							$img_info = $this->upload->data();
							$news_image = $img_info['file_name'];
						}
					}
					else {
						$imgData = base64_decode($_POST['capture_pic']);	
						$path = './data/images/';
						$random_name = $this->randomstring(10);
						$file = $random_name.'.jpg';
						$file_path = $path . $file;
						$fp = fopen($file_path,'w');
						fwrite($fp, $imgData);
						fclose($fp);
						$news_image = $file;
					}
				}
				$insert_data = array(
					'news_category'=> $news_category,
					'news_tag'=> $news_tags,
					'news_heading'=> $news_heading,
					'news_text'=> $news_description,
					'updated_by'=> $this->session->userdata('userid'),
					'news_link'=> $news_link,
					'main_image'=> $news_image
				);
				if($n_id = $this->citizenmodel->insert_record_key('news_articles',$insert_data)) {
					$data['post_error_msg'] = '<div class="alert alert-success">Successfully Posted.'.
					'<button class="close" data-dismiss="alert" type="button">×</button></div>';
					//$this->load->view('makepost',$data);
					$user_lat = $this->session->userdata('user_lat');
					$user_lng = $this->session->userdata('user_lng');
					$users_for_notif = $this->citizenmodel->get_two_columns_data_array('users','UserLocationLat',$user_lat,'UserLocationLng',$user_lng);
					foreach($users_for_notif as $us) {
						
						if($us['user_id']!=$this->session->userdata('userid')) {
							$insert_notification = array(
								'user_id' => $us['user_id'],
								'poster_id' => $this->session->userdata('userid'),
								'news_id' => $n_id,
								'notification_type' => 'area_news',
								'status' => 1
							);
							$this->citizenmodel->insert_record('user_notifications',$insert_notification);
						}
					}
					$tags_id = explode(',',$news_tags);
					foreach($tags_id as $id) {
						$search_id = ','.$id.',';
						$users_for_notif = $this->citizenmodel->like_query_data_array('users','UserTags',$search_id);
						foreach($users_for_notif as $us) {
							if($us['user_id']!=$this->session->userdata('userid')) {
								$insert_notification = array(
									'user_id' => $us['user_id'],
									'poster_id' => $this->session->userdata('userid'),
									'news_id' => $n_id,
									'notification_type' => 'tag_interest',
									'status' => 1
								);
								$this->citizenmodel->insert_record('user_notifications',$insert_notification);
							}
						}
					}
					//echo '<script type="text/javascript">alert("Your post is succesfully posted.")/script>';
					$this->session->set_flashdata('post_valid', '<div class="alert alert-success">Your post is succesfully posted</div>');
					$url = base_url().'citizen/';
					redirect($url, 'refresh');
				}
				else {
					$data['post_error_msg'] = '<div class="alert alert-error">Oops! Something went Wrong, Kindly try again.'.
					'<button class="close" data-dismiss="alert" type="button">×</button></div>';
					$this->load->view('makepost',$data);
				}
			}
		}
		else
			$this->load->view('login',$data);
	}
	public function retrieve_news($category='', $cat_id = 0, $news_id = 0) {
		$user_id = $this->session->userdata('userid');
		$temp_user_data = $this->citizenmodel->get_specific_data_row('users','user_id',$user_id);
		$data['post'] = $post = $this->citizenmodel->get_specific_data_row('news_articles','news_id',$news_id);
		$data['related_news'] = $this->citizenmodel->get_active_data_sorted_array('news_articles','news_status','updated_by',$post->updated_by,'upload_time','DESC',4);
		
		$data['cat_name'] = $category;
		$data['comments'] = $this->citizenmodel->get_comments_sorted_array('news_id',$news_id,2,'post_time','asc');
		$rating_check = $this->citizenmodel->checkendorsement($user_id,$news_id,3);
		if(count($rating_check)>0)
			$data['rating_check'] = false;
		else
			$data['rating_check'] = true;
		$tags = $post->news_tag;
		$this->load->view('single_post',$data);
	}
}
