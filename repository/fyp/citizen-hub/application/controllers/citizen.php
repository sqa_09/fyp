<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Citizen extends CI_Controller {

	/**
	 * Citizen Class (Index Page for this controller).
	 *
	 * Maps to the following URL
	 * 		http://citizenhub.com/citizen/home
	 *	- or -
	 * 		http://citizenhub.com/index.php/citizen/home
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://citizenhub.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /citizen/<method_name>
	 */
	 
	/*Constructer of citizen class*/
	function __construct() {
		parent::__construct();
		$this->load->model('citizenmodel');
		$this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->helper('user');
		$this->load->library('recaptcha');
	}
	
	/*Home page function (Main)*/
	public function index($msg = '') {
		$data = "";
		if($this->session->flashdata('post_valid')) {
			$data['posted'] = $this->session->flashdata('post_valid');
		}
		$rss_results = $data['latest_news'] = $this->citizenmodel->get_active_sort_data_limit('news_articles','news_status','upload_time','DESC',4);
		$data['hot_news'] = $this->citizenmodel->get_active_sort_data_limit('news_articles','news_status','overall_rating','DESC',4);
		$data['popular_news'] = $this->citizenmodel->get_active_sort_data_limit('news_articles','news_status','total_comments','DESC',4);
		$all_cat_news = '';
		$al_cnt = 1;
		$categories = $this->citizenmodel->get_active_data('news_categories','news_cat_status');
		foreach($categories as $cat) {
			$cat_news = $this->citizenmodel->get_active_data_sorted_array('news_articles','news_status','news_category',$cat['news_cat_id'],'upload_time','DESC',2);
			if($cat_news) {
				if($al_cnt%2==0)
                	$all_cat_news .= '<div class="row-fluid">';
				$all_cat_news .= '<div class="span2">
				<div class="marked-title">
					<h3>'.$cat['news_cat_text'].'</h3>
				</div>';
				$i = 1;
				foreach($cat_news as $news) {
					$timestamp = strtotime($news['upload_time']);
					if($i == count($cat_news))
						$all_cat_news .= '<article class="small last">';
					else
						$all_cat_news .= '<article class="small">';
					$all_cat_news .= '<div class="post-thumb">
						<a href="'.base_url().'citizen/singlepost/popular/'.$news['news_category'].'/'.$news['news_id'].'">
						<img src="'.base_url().'data/images/'.$news['main_image'].'" alt=""></a>
					</div>
					<div class="cat-post-desc">
						<p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp)
						.' // '.$news['total_comments'].' Comments</p>
						<h3><a href="'.base_url().'citizen/singlepost/'.$cat['news_cat_text'].'/'.$news['news_category'].'/'.$news['news_id'].'">
						'.$news['news_heading'].'</a></h3>
					</div>
					</article>';
					$i++;
				}
				$all_cat_news .= '</div>';
				if($al_cnt%2 == 0)
					$all_cat_news .= '</div>';
				$al_cnt++;
			}
		}
		$data['all_cat_news'] = $all_cat_news;
		//$data['entertainment_news'] = $this->citizenmodel->get_active_data_sorted_array('news_articles','news_status','news_category',1,'upload_time','DESC',2);
		//$data['business_news'] = $this->citizenmodel->get_active_data_sorted_array('news_articles','news_status','news_category',2,'upload_time','DESC',2);
		//$data['science_news'] = $this->citizenmodel->get_active_data_sorted_array('news_articles','news_status','news_category',4,'upload_time','DESC',2);
		if(is_logged_in()) {
			$username = is_logged_in();
			$userdetail = $this->citizenmodel->get_specific_data_row('users','UserName',$username);
			$data['recommended'] = $this->collaborative_filtering($userdetail->UserTags);
			$user_id = $this->session->userdata('userid');
			$notif_results = $this->citizenmodel->get_notifications('user_notifications','user_id',$user_id,'status',1);
			$notification_results = '';
			foreach($notif_results as $not_res)	{
				$post_time= strtotime($not_res['notification_time']);
				$comment_by = $not_res['poster_id'];
				$notification_results .= '
					<div style="width:100%; float:left; border-bottom:1px solid #CCC; margin-bottom:5px;">
						<div style="float:left; text-align:center; width:25%; background:#ccc;">
							<span>('.date("H", $post_time).':'.date("i", $post_time).')<br />'.date("d", $post_time).' '.date("M", $post_time).' '.date("Y", $post_time).'</div>
						<div style=" float:right; width:73%;">'.ucfirst(get_username($comment_by)).'
							<a href="'.base_url().'citizen/singlepost/not/0/'.$not_res['news_id'].'">';
							if($not_res['notification_type']=='comment')
								$notification_results .= 'Comment on your post.';
							else if($not_res['notification_type']=='area_news')
								$notification_results .= 'Posted a news about your area.';
							else if($not_res['notification_type']=='endorsment')
								$notification_results .= 'Endorsed your post.';
							else if($not_res['notification_type']=='tag_interest')
								$notification_results .= 'Posted a news of your Interest.';
							$notification_results .= '</a>
						</div>
					</div>';
			}
			$data['notification_results'] = $notification_results;
			
		}
		$this->create_rss_feeds($rss_results , 'Latest News');
		$this->load->view('home',$data);
	}
	
	/*News Categories*/
	public function category($cat_name = '') {
		//$data['all_tags'] = $this->citizenmodel->get_active_data('tags','TagStatus');
		$cat_result = $this->citizenmodel->get_specific_data_row('news_categories','news_cat_text',$cat_name);
		$cat_id = $cat_result->news_cat_id;
		$rss_name = $data['cat_name'] = $cat_result->news_cat_text;
		$temp_results = $this->citizenmodel->get_specific_data_sorted_array('news_articles','news_category',$cat_id,'upload_time','DESC');
		$news_results = '';
		foreach ($temp_results as $news_result) {
			$news_id = $news_result['news_id'];
			$timestamp = strtotime($news_result['upload_time']);
			$news_results = $news_results . '<article class="small">
				<div class="post-thumb">
					<a href="'.base_url().'citizen/singlepost/'.$cat_result->news_cat_text.'/'.$cat_id.'/'.$news_id.'">
					<img src="'.base_url().'data/images/'.$news_result['main_image'].'" alt=""></a>
				</div>
				<div class="cat-post-desc">
					<h3><a href="'.base_url().'citizen/singlepost/'.$cat_result->news_cat_text.'/'.$cat_id.'/'.$news_id.'">'.$news_result['news_heading'].'</a></h3>
					<p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp)
					.' ('.date("H", $timestamp).':'.date("i", $timestamp).')  // '.$news_result['total_comments'].' Comments</p>
					<p class="endorse">';
					if(is_logged_in()) { 
						$user_id = $this->session->userdata('userid');
						$news_results = $news_results . '<span id="endorse_'.$news_id.'">';
						$endorsement_check = $this->citizenmodel->checkendorsement($user_id,$news_id,1);
						if(count($endorsement_check)==1)
							{ $news_results = $news_results . '<a>You Endorsed This post.</a>'; }
						else
							{ $news_results = $news_results . '<a onclick="javascript:endorsepost('.$news_id.',1)">Endorse this</a>'; }
						$news_results = $news_results . '</span>  // ';
					}
					$rating = $this->citizenmodel->get_rating($news_id,3);
					$news_results = $news_results . '<span id="endorse_count_'.$news_id.'">'.
					count($this->citizenmodel->get_comments_sorted_array('news_id',$news_id,1,'post_time','asc')) . 
					'</span> endorsments // Rating: '.$rating.'</p>';
					
					$news_results = $news_results . '<p class="endorse">';
					if(is_logged_in()) {
						$user_id = $this->session->userdata('userid');
						$news_results = $news_results . '<span id="subscribe_'.$news_id.'">';
						$endorsement_check = $this->citizenmodel->checksubsucription($user_id,$news_id);
						if(count($endorsement_check)>0)
							{ $news_results = $news_results . '<a onclick="javascript:subscribepost('.$news_id.',2)">Unsubscribe This post</a>'; }
						else
							{ $news_results = $news_results . '<a onclick="javascript:subscribepost('.$news_id.',1)">Subscribe this Post</a>'; }
						$news_results = $news_results . '</span>  // ';
					}
					$rating = $this->citizenmodel->get_rating($news_id,3);
					$news_results = $news_results . '<span id="subscribe_count_'.$news_id.'">'.
					count($this->citizenmodel->get_specific_data_array('user_subscription','news_id',$news_id)) . 
					'</span> Subscriptions</p>
					
					<p>'.substr($news_result['news_text'],0,370).' ... . 
					<a href="'.base_url().'citizen/singlepost/'.$cat_result->news_cat_text.'/'.$cat_id.'/'.$news_id.'">Read More</a></p>
				</div>
			</article>';
		}
		$data['news_results'] = $news_results;
		$this->create_rss_feeds($temp_results,$rss_name);
		$this->load->view('category',$data);
	}
	
	/*Single Post*/
	public function singlepost($category='', $cat_id = 0, $news_id = 0) {
		$user_id = $this->session->userdata('userid');
		$temp_user_data = $this->citizenmodel->get_specific_data_row('users','user_id',$user_id);
		$data['post'] = $post = $this->citizenmodel->get_specific_data_row('news_articles','news_id',$news_id);
		$data['related_news'] = $this->citizenmodel->get_active_data_sorted_array('news_articles','news_status','updated_by',$post->updated_by,'upload_time','DESC',4);
		
		$data['cat_name'] = $category;
		$data['comments'] = $this->citizenmodel->get_comments_sorted_array('news_id',$news_id,2,'post_time','asc');
		$rating_check = $this->citizenmodel->checkendorsement($user_id,$news_id,3);
		if(count($rating_check)>0)
			$data['rating_check'] = false;
		else
			$data['rating_check'] = true;
		$tags = $post->news_tag;
		$this->load->view('single_post',$data);
	}
	
	/*Search News/Posts*/
	public function search($search_type = '', $tag_id = '',$tag_name = '') {
		$rss_term = '';
		if($search_type == 'tag' && $tag_id!='') {
			$rss_term = $data['cat_name'] = "tag '".$tag_name."'";
			$temp_results = $this->citizenmodel->like_query_data_array('news_articles','news_tag',$tag_id);
		}
		else {
			$search_key = $this->input->post('search_key');
			$rss_term = $data['cat_name'] = "term '".$search_key."'";
			$temp_results = $this->citizenmodel->search_data_array($search_key);
		}
		$news_results = '';
		if(count($temp_results)>0) {
			foreach ($temp_results as $news_result) {
				$news_id = $news_result['news_id'];
				$cat_id = $news_result['news_category'];
				$timestamp = strtotime($news_result['upload_time']);
				$news_results = $news_results . '<article class="small">
					<div class="post-thumb">
						<a href="'.base_url().'citizen/singlepost/Search/'.$cat_id.'/'.$news_id.'">
						<img src="'.base_url().'data/images/'.$news_result['main_image'].'" alt=""></a>
					</div>
					<div class="cat-post-desc">
						<h3><a href="'.base_url().'citizen/singlepost/Search/'.$cat_id.'/'.$news_id.'">'.$news_result['news_heading'].'</a></h3>
						<p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp)
						.' ('.date("H", $timestamp).':'.date("i", $timestamp).')  // '.$news_result['total_comments'].' Comments</p>
						<p class="endorse">';
						if(is_logged_in()) { 
							$user_id = $this->session->userdata('userid');
							$news_results = $news_results . '<span id="endorse_'.$news_id.'">';
							$endorsement_check = $this->citizenmodel->checkendorsement($user_id,$news_id,1);
							if(count($endorsement_check)==1)
								{ $news_results = $news_results . '<a>You Endorsed This post.</a>'; }
							else
								{ $news_results = $news_results . '<a onclick="javascript:endorsepost('.$news_id.',1)">Endorse this</a>'; }
							$news_results = $news_results . '</span>  // ';
						}
						$rating = $this->citizenmodel->get_rating($news_id,3);
						$news_results = $news_results . '<span id="endorse_count_'.$news_id.'">'.
						count($this->citizenmodel->get_comments_sorted_array('news_id',$news_id,1,'post_time','asc')) . 
						'</span> endorsments // Rating: '.$rating.'</p>';
					
						$news_results = $news_results . '<p class="endorse">';
						if(is_logged_in()) {
							$user_id = $this->session->userdata('userid');
							$news_results = $news_results . '<span id="subscribe_'.$news_id.'">';
							$endorsement_check = $this->citizenmodel->checksubsucription($user_id,$news_id);
							if(count($endorsement_check)>0)
								{ $news_results = $news_results . '<a onclick="javascript:subscribepost('.$news_id.',2)">Unsubscribe This post</a>'; }
							else
								{ $news_results = $news_results . '<a onclick="javascript:subscribepost('.$news_id.',1)">Subscribe this Post</a>'; }
							$news_results = $news_results . '</span>  // ';
						}
						$rating = $this->citizenmodel->get_rating($news_id,3);
						$news_results = $news_results . '<span id="subscribe_count_'.$news_id.'">'.
						count($this->citizenmodel->get_specific_data_array('user_subscription','news_id',$news_id)) . 
						'</span> Subscriptions</p>
						
						<p>'.substr($news_result['news_text'],0,370).' ... . 
						<a href="'.base_url().'citizen/singlepost/Search/'.$cat_id.'/'.$news_id.'">Read More</a></p>
					</div>
				</article>';
			}
		}
		else
			{$news_results = '<article class="small">No data found.</article>';}
		$data['news_results'] = $news_results;
		$this->create_rss_feeds($temp_results,$rss_term);
		$this->load->view('search',$data);
	}
	
	/*View My Profile*/
	public function myprofile($username = '') {
		if(is_logged_in()) {
			if($this->session->flashdata('not_valid')) {
				$data['not_valid'] = $this->session->flashdata('not_valid');
			}
			$data['username'] = is_logged_in();
			$user_id = $this->session->userdata('userid');
			$news_results = '';
			$data['user_rating'] = $this->citizenmodel->get_user_rating($user_id);
			$temp_results = $this->citizenmodel->get_specific_data_sorted_array('news_articles','updated_by',$user_id,'upload_time','DESC');
			foreach ($temp_results as $news_result) {
				$news_id = $news_result['news_id'];
				$cat_id = $news_result['news_category'];
				$timestamp = strtotime($news_result['upload_time']);
				$news_results = $news_results . '<article class="small">
					<div class="post-thumb">
						<a href="'.base_url().'citizen/singlepost/my/'.$cat_id.'/'.$news_id.'">
						<img src="'.base_url().'data/images/'.$news_result['main_image'].'" alt=""></a>
					</div>
					<div class="cat-post-desc">
						<h3><a href="'.base_url().'citizen/singlepost/my/'.$cat_id.'/'.$news_id.'">'.$news_result['news_heading'].'</a></h3>
						<p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp)
						.' ('.date("H", $timestamp).':'.date("i", $timestamp).')  // '.$news_result['total_comments'].' Comments</p>
						<p class="endorse">';
						if(is_logged_in()) { 
							$user_id = $this->session->userdata('userid');
							$news_results = $news_results . '<span id="endorse_'.$news_id.'">';
							$endorsement_check = $this->citizenmodel->checkendorsement($user_id,$news_id,1);
							if(count($endorsement_check)==1)
								{ $news_results = $news_results . '<a>You Endorsed This post.</a>'; }
							else
								{ $news_results = $news_results . '<a onclick="javascript:endorsepost('.$news_id.',1)">Endorse this</a>'; }
							$news_results = $news_results . '</span> / <a href="'.base_url().'citizen/delete_post/'.$news_id.'">Delete this post</a>  // ';
						}
						$rating = $this->citizenmodel->get_rating($news_id,3);
						$news_results = $news_results . '<span id="endorse_count_'.$news_id.'">'.
						count($this->citizenmodel->get_comments_sorted_array('news_id',$news_id,1,'post_time','asc')) . 
						'</span> endorsments // Rating: '.$rating.'</p>';
						
						$news_results = $news_results . '<p class="endorse">';
						if(is_logged_in()) {
							$user_id = $this->session->userdata('userid');
							$news_results = $news_results . '<span id="subscribe_'.$news_id.'">';
							$endorsement_check = $this->citizenmodel->checksubsucription($user_id,$news_id);
							if(count($endorsement_check)>0)
								{ $news_results = $news_results . '<a onclick="javascript:subscribepost('.$news_id.',2)">Unsubscribe This post</a>'; }
							else
								{ $news_results = $news_results . '<a onclick="javascript:subscribepost('.$news_id.',1)">Subscribe this Post</a>'; }
							$news_results = $news_results . '</span>  // ';
						}
						$rating = $this->citizenmodel->get_rating($news_id,3);
						$news_results = $news_results . '<span id="subscribe_count_'.$news_id.'">'.
						count($this->citizenmodel->get_specific_data_array('user_subscription','news_id',$news_id)) . 
						'</span> Subscriptions</p>
						
						<p>'.substr($news_result['news_text'],0,500).' ... . 
						<a href="'.base_url().'citizen/singlepost/my/'.$cat_id.'/'.$news_id.'">Read More</a></p>
					</div>
				</article>';
			}
			$data['news_results'] = $news_results;
			$subscription_results = '';
			$sub_results = $this->citizenmodel->get_specific_data_array('user_subscription','user_id',$user_id);
			foreach($sub_results as $sub_res)
			{
				$news_result = $this->citizenmodel->get_specific_data_row('news_articles','news_id',$sub_res['news_id']);
				$news_id = $news_result->news_id;
				$cat_id = $news_result->news_category;
				$timestamp = strtotime($news_result->upload_time);
				$subscription_results = $subscription_results . '<article class="small">
					<div class="post-thumb">
						<a href="'.base_url().'citizen/singlepost/my/'.$cat_id.'/'.$news_id.'">
						<img src="'.base_url().'data/images/'.$news_result->main_image.'" alt=""></a>
					</div>
					<div class="cat-post-desc">
						<h3><a href="'.base_url().'citizen/singlepost/my/'.$cat_id.'/'.$news_id.'">'.$news_result->news_heading.'</a></h3>
						<p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp)
						.' ('.date("H", $timestamp).':'.date("i", $timestamp).')  // '.$news_result->total_comments.' Comments</p>
						<p class="endorse">';
						if(is_logged_in()) { 
							$user_id = $this->session->userdata('userid');
							$subscription_results = $subscription_results . '<span id="sendorse_'.$news_id.'">';
							$endorsement_check = $this->citizenmodel->checkendorsement($user_id,$news_id,1);
							if(count($endorsement_check)==1)
								{ $subscription_results = $subscription_results . '<a>You Endorsed This post.</a>'; }
							else
								{ $subscription_results = $subscription_results . 
								'<a onclick="javascript:sendorsepost('.$news_id.',1)">Endorse this</a>'; }
							$subscription_results = $subscription_results . '</span>  // ';
						}
						$rating = $this->citizenmodel->get_rating($news_id,3);
						$subscription_results = $subscription_results . '<span id="sendorse_count_'.$news_id.'">'.
						count($this->citizenmodel->get_comments_sorted_array('news_id',$news_id,1,'post_time','asc')) . 
						'</span> endorsments // Rating: '.$rating.'</p>';
						
						$subscription_results = $subscription_results . '<p class="endorse">';
						if(is_logged_in()) {
							$user_id = $this->session->userdata('userid');
							$subscription_results = $subscription_results . '<span id="ssubscribe_'.$news_id.'">';
							$endorsement_check = $this->citizenmodel->checksubsucription($user_id,$news_id);
							if(count($endorsement_check)>0)
								{ $subscription_results = $subscription_results . 
								'<a onclick="javascript:ssubscribepost('.$news_id.',2)">Unsubscribe This post</a>'; }
							else
								{ $subscription_results = $subscription_results . 
								'<a onclick="javascript:ssubscribepost('.$news_id.',1)">Subscribe this Post</a>'; }
							$subscription_results = $subscription_results . '</span>  // ';
						}
						$rating = $this->citizenmodel->get_rating($news_id,3);
						$subscription_results = $subscription_results . '<span id="ssubscribe_count_'.$news_id.'">'.
						count($this->citizenmodel->get_specific_data_array('user_subscription','news_id',$news_id)) . 
						'</span> Subscriptions</p>
						
						<p>'.substr($news_result->news_text,0,500).' ... . 
						<a href="'.base_url().'citizen/singlepost/my/'.$cat_id.'/'.$news_id.'">Read More</a></p>
					</div>
				</article>';
			}
			$data['subscription_results'] = $subscription_results;
			
			$data['user_info'] = $this->citizenmodel->get_specific_data_row('users','user_id',$user_id);
			
			$this->load->view('profile',$data);
		}
		else
			$this->login();
	}
	
	public function viewprofile($username = '') {
		if($username != '') {
			$data['username'] = $username;
			$user_info = $this->citizenmodel->get_specific_data_row('users','UserName',$username);
			$user_id = $user_info->user_id;
			$data['user_info'] = $user_info;
			$news_results = '';
			$data['user_rating'] = $this->citizenmodel->get_user_rating($user_id);
			$temp_results = $this->citizenmodel->get_specific_data_sorted_array('news_articles','updated_by',$user_id,'upload_time','DESC');
			foreach ($temp_results as $news_result) {
				$news_id = $news_result['news_id'];
				$cat_id = $news_result['news_category'];
				$timestamp = strtotime($news_result['upload_time']);
				$news_results = $news_results . '<article class="small">
					<div class="post-thumb">
						<a href="'.base_url().'citizen/singlepost/my/'.$cat_id.'/'.$news_id.'">
						<img src="'.base_url().'data/images/'.$news_result['main_image'].'" alt=""></a>
					</div>
					<div class="cat-post-desc">
						<h3><a href="'.base_url().'citizen/singlepost/my/'.$cat_id.'/'.$news_id.'">'.$news_result['news_heading'].'</a></h3>
						<p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp)
						.' ('.date("H", $timestamp).':'.date("i", $timestamp).')  // '.$news_result['total_comments'].' Comments</p>
						<p class="endorse">';
						if(is_logged_in()) { 
							$user_id = $this->session->userdata('userid');
							$news_results = $news_results . '<span id="endorse_'.$news_id.'">';
							$endorsement_check = $this->citizenmodel->checkendorsement($user_id,$news_id,1);
							if(count($endorsement_check)==1)
								{ $news_results = $news_results . '<a>You Endorsed This post.</a>'; }
							else
								{ $news_results = $news_results . '<a onclick="javascript:endorsepost('.$news_id.',1)">Endorse this</a>'; }
							$news_results = $news_results . '</span>  // ';
						}
						$rating = $this->citizenmodel->get_rating($news_id,3);
						$news_results = $news_results . '<span id="endorse_count_'.$news_id.'">'.
						count($this->citizenmodel->get_comments_sorted_array('news_id',$news_id,1,'post_time','asc')) . 
						'</span> endorsments // Rating: '.$rating.'</p>';
						
						$news_results = $news_results . '<p class="endorse">';
						if(is_logged_in()) {
							$user_id = $this->session->userdata('userid');
							$news_results = $news_results . '<span id="subscribe_'.$news_id.'">';
							$endorsement_check = $this->citizenmodel->checksubsucription($user_id,$news_id);
							if(count($endorsement_check)>0)
								{ $news_results = $news_results . '<a onclick="javascript:subscribepost('.$news_id.',2)">Unsubscribe This post</a>'; }
							else
								{ $news_results = $news_results . '<a onclick="javascript:subscribepost('.$news_id.',1)">Subscribe this Post</a>'; }
							$news_results = $news_results . '</span>  // ';
						}
						$rating = $this->citizenmodel->get_rating($news_id,3);
						$news_results = $news_results . '<span id="subscribe_count_'.$news_id.'">'.
						count($this->citizenmodel->get_specific_data_array('user_subscription','news_id',$news_id)) . 
						'</span> Subscriptions</p>
						
						<p>'.substr($news_result['news_text'],0,500).' ... . 
						<a href="'.base_url().'citizen/singlepost/my/'.$cat_id.'/'.$news_id.'">Read More</a></p>
					</div>
				</article>';
			}
			$data['news_results'] = $news_results;
			$subscription_results = '';
			$sub_results = $this->citizenmodel->get_specific_data_array('user_subscription','user_id',$user_id);
			foreach($sub_results as $sub_res) {
				$news_result = $this->citizenmodel->get_specific_data_row('news_articles','news_id',$sub_res['news_id']);
				$news_id = $news_result->news_id;
				$cat_id = $news_result->news_category;
				$timestamp = strtotime($news_result->upload_time);
				$subscription_results = $subscription_results . '<article class="small">
					<div class="post-thumb">
						<a href="'.base_url().'citizen/singlepost/my/'.$cat_id.'/'.$news_id.'">
						<img src="'.base_url().'data/images/'.$news_result->main_image.'" alt=""></a>
					</div>
					<div class="cat-post-desc">

						<h3><a href="'.base_url().'citizen/singlepost/my/'.$cat_id.'/'.$news_id.'">'.$news_result->news_heading.'</a></h3>
						<p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp)
						.' ('.date("H", $timestamp).':'.date("i", $timestamp).')  // '.$news_result->total_comments.' Comments</p>
						<p class="endorse">';
						if(is_logged_in()) { 
							$user_id = $this->session->userdata('userid');
							$subscription_results = $subscription_results . '<span id="sendorse_'.$news_id.'">';
							$endorsement_check = $this->citizenmodel->checkendorsement($user_id,$news_id,1);
							if(count($endorsement_check)==1)
								{ $subscription_results = $subscription_results . '<a>You Endorsed This post.</a>'; }
							else
								{ $subscription_results = $subscription_results . 
								'<a onclick="javascript:sendorsepost('.$news_id.',1)">Endorse this</a>'; }
							$subscription_results = $subscription_results . '</span>  // ';
						}
						$rating = $this->citizenmodel->get_rating($news_id,3);
						$subscription_results = $subscription_results . '<span id="sendorse_count_'.$news_id.'">'.
						count($this->citizenmodel->get_comments_sorted_array('news_id',$news_id,1,'post_time','asc')) . 
						'</span> endorsments // Rating: '.$rating.'</p>';
						
						$subscription_results = $subscription_results . '<p class="endorse">';
						if(is_logged_in()) {
							$user_id = $this->session->userdata('userid');
							$subscription_results = $subscription_results . '<span id="ssubscribe_'.$news_id.'">';
							$endorsement_check = $this->citizenmodel->checksubsucription($user_id,$news_id);
							if(count($endorsement_check)>0)
								{ $subscription_results = $subscription_results . 
								'<a onclick="javascript:ssubscribepost('.$news_id.',2)">Unsubscribe This post</a>'; }
							else
								{ $subscription_results = $subscription_results . 
								'<a onclick="javascript:ssubscribepost('.$news_id.',1)">Subscribe this Post</a>'; }
							$subscription_results = $subscription_results . '</span>  // ';
						}
						$rating = $this->citizenmodel->get_rating($news_id,3);
						$subscription_results = $subscription_results . '<span id="ssubscribe_count_'.$news_id.'">'.
						count($this->citizenmodel->get_specific_data_array('user_subscription','news_id',$news_id)) . 
						'</span> Subscriptions</p>
						
						<p>'.substr($news_result->news_text,0,500).' ... . 
						<a href="'.base_url().'citizen/singlepost/my/'.$cat_id.'/'.$news_id.'">Read More</a></p>
					</div>
				</article>';
			}
			$data['subscription_results'] = $subscription_results;
			
			$this->load->view('profile-other',$data);
		}
		else
			redirect(base_url(),'location');
	}
	
	/* edit profile of user*/
	public function profile($username = '',$type = '') {
		if(is_logged_in()) {
			$data = '';
			$username = is_logged_in();
			if($type == '') {
				
			}
			else {
				$data['all_tags'] = $this->citizenmodel->get_active_data('tags','TagStatus');
				if($type == 'update') {
					if(isset($_FILES['profile_pic']) && !empty($_FILES['profile_pic']['name'])) {
						$config = array(
							'allowed_types' => 'jpeg|jpg|png|gif|bmp',
							'upload_path' => './data/users/',
							'max_size' => 2048,
							'encrypt_name' => TRUE,
							'overwrite' => FALSE,
						);
						$this->upload->initialize($config);
						if(!$this->upload->do_upload('profile_pic'))
						   $data['profile_update_msg'] = '<div class="alert alert-error">'. $this->upload->display_errors().'<button class="close" data-dismiss="alert" type="button">�</button></div>';
						else {
							$img_info = $this->upload->data();
							$update_data = array(
								'UserImage'=> $img_info['file_name']
							);
							if($this->citizenmodel->update_record('users',$update_data,'UserName',$username))
								$data['profile_update_msg'] = '<div class="alert alert-success">Profile Picture updated Successfully.<button class="close" data-dismiss="alert" type="button">�</button></div>';
							else
								$data['profile_update_msg'] = '<div class="alert alert-error">'. $this->upload->display_errors().'<button class="close" data-dismiss="alert" type="button">�</button></div>';
						}
					}
					else if($_POST['capture_pic']) {
						$imgData = base64_decode($_POST['capture_pic']);	
						$path = './data/users/';
						$random_name = $this->randomstring(10);
						$file = $random_name.'.jpg';
						$file_path = $path . $file;
						$fp = fopen($file_path,'w');
						fwrite($fp, $imgData);
						fclose($fp);
						$update_data = array(
							'UserImage'=> $file
						);
						if($this->citizenmodel->update_record('users',$update_data,'UserName',$username))
							$data['profile_update_msg'] = '<div class="alert alert-success">Profile Picture updated Successfully.<button class="close" data-dismiss="alert" type="button">�</button></div>';
						else
							$data['profile_update_msg'] = '<div class="alert alert-error">'. $this->upload->display_errors().'<button class="close" data-dismiss="alert" type="button">�</button></div>';
					}
					else {
						$profile_update_rules = array(
							array(
								'field'   => 'user_email',
								'label'   => 'Email',
								'rules'   => 'required|valid_email'
							),
							array(
								'field'   => 'user_fname',
								'label'   => 'First Name',
								'rules'   => 'required|min_length[3]'
							),
							array(
								'field'   => 'user_lname',
								'label'   => 'Last Name',
								'rules'   => 'required|min_length[2]'
							),
							array(
								'field'   => 'user_city',
								'label'   => 'City',
								'rules'   => 'required|min_length[4]'
							),
							array(
								'field'   => 'user_country',
								'label'   => 'Country',
								'rules'   => 'required|min_length[4]'
							)
						);
						$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '<button class="close" data-dismiss="alert" type="button">�</button></div>');
						$this->form_validation->set_rules($profile_update_rules);
						if ($this->form_validation->run() == TRUE) {
							$user_email = htmlspecialchars($this->input->post("user_email"));
							$user_fname = htmlspecialchars($this->input->post("user_fname"));
							$user_lname = htmlspecialchars($this->input->post("user_lname"));
							$user_gender = htmlspecialchars($this->input->post("user_gender"));
							$user_address = htmlspecialchars($this->input->post("user_address"));
							$user_city = htmlspecialchars($this->input->post("user_city"));
							$user_country = htmlspecialchars($this->input->post("user_country"));
							$user_tags = htmlspecialchars($this->input->post("user_tags"));
							$old_password = htmlspecialchars($this->input->post("old_password"));
							if($old_password!='') {
								$userdetail = $this->citizenmodel->get_specific_data_row('users','UserName',$username);
								if($userdetail->UserPass != md5($old_password)) {
									$data['profile_update_msg'] = '<div class="alert alert-error">Old password is not correct.<button class="close" data-dismiss="alert" type="button">�</button></div>';
								}
								else {
									$password_rules = array(
										array(
											'field'   => 'new_password',
											'label'   => 'New Password',
											'rules'   => 'required|min_length[6]'
										),
										array(
											'field'   => 'user_con_password',
											'label'   => 'Confirm Password',
											'rules'   => 'required|matches[new_password]'
										)
									);
									$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '<button class="close" data-dismiss="alert" type="button">�</button></div>');
									$this->form_validation->set_rules($password_rules);
									if ($this->form_validation->run() == TRUE) {
										$new_password = htmlspecialchars($this->input->post("new_password"));
										$update_data = array(
											'UserEmail'=> $user_email,
											'UserFName' => $user_fname ,
											'UserLName' => $user_lname ,
											'UserGender' => $user_gender,
											'UserAddress' => $user_address,
											'UserCity' => $user_city ,
											'UserCountry' => $user_country,
											'UserTags' => $user_tags,
											'UserPass' => md5($new_password)
										);
										if($this->citizenmodel->update_record('users',$update_data,'UserName',$username))
											$data['profile_update_msg'] = '<div class="alert alert-success">Profile Information Updated Successfully<button class="close" data-dismiss="alert" type="button">�</button></div>'; 
										else
											$data['profile_update_msg'] = '<div class="alert alert-error">Oops! Something went Wrong, Kindly try again.<button class="close" data-dismiss="alert" type="button">�</button></div>';
									}
								}
							}
							else {
								$update_data = array(
									'UserEmail'=> $user_email,
									'UserFName' => $user_fname ,
									'UserLName' => $user_lname ,
									'UserGender' => $user_gender,
									'UserAddress' => $user_address,
									'UserCity' => $user_city ,
									'UserCountry' => $user_country,
									'UserTags' => $user_tags,
								);
								if($this->citizenmodel->update_record('users',$update_data,'UserName',$username))
									$data['profile_update_msg'] = '<div class="alert alert-success">Profile Information Updated Successfully<button class="close" data-dismiss="alert" type="button">�</button></div>'; 
								else
									$data['profile_update_msg'] = '<div class="alert alert-error">Oops! Something went Wrong, Kindly try again.<button class="close" data-dismiss="alert" type="button">�</button></div>';
							}
						}
					}
					$userdetail = $this->citizenmodel->get_specific_data_row('users','UserName',$username);
					$data['userdetail'] = $userdetail;
				}
				else if($type=='edit') {
					$userdetail = $this->citizenmodel->get_specific_data_row('users','UserName',$username);
					$data['userdetail'] = $userdetail;
				}
				$this->load->view('editprofile',$data);
			}
		}
		else
			$this->login();
	}
		
	/*Registering a new user*/
	public function signup() {
		$data = "";
		if(!is_logged_in()) {
            $data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();
			$signup_rules_array = array(
				array(
					'field'   => 'user_name',
					'label'   => 'Username',
					'rules'   => 'required|min_length[4]|callback_username_check'
				),
				array(
					'field'   => 'user_email',
					'label'   => 'Email',
					'rules'   => 'required|valid_email|callback_email_check'
				),
				array(
					'field'   => 'user_fname',
					'label'   => 'First Name',
					'rules'   => 'required|min_length[3]'
				),
				array(
					'field'   => 'user_lname',
					'label'   => 'Last Name',
					'rules'   => 'required|min_length[2]'
				),
				array(
					'field'   => 'user_city',
					'label'   => 'City',
					'rules'   => 'required|min_length[4]'
				),
				array(
					'field'   => 'user_country',
					'label'   => 'Country',
					'rules'   => 'required|min_length[4]'
				),
				array(
					'field'   => 'user_password',
					'label'   => 'Password',
					'rules'   => 'required|min_length[6]'
				),
				array(
					'field'   => 'user_con_password',
					'label'   => 'Confirm Password',
					'rules'   => 'required|matches[user_password]'
				)
			);
			$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '<button class="close" data-dismiss="alert" type="button">�</button></div>');
			$this->form_validation->set_rules($signup_rules_array);
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('signup',$data);
			}
			else {
				$resp = $this->recaptcha->recaptcha_check_answer(
                                        $_SERVER["REMOTE_ADDR"],
                                        $_POST["recaptcha_challenge_field"],
                                        $_POST["recaptcha_response_field"]);
				if(!$this->recaptcha->getIsValid()) {
					$data['not_valid'] = '<div class="alert alert-error">Captcha you entered is incorrect! Kindly Re-Enter.<button class="close" data-dismiss="alert" type="button">�</button></div>';
					$this->load->view('signup',$data);
				}
				else {
					$username = htmlspecialchars($this->input->post("user_name"));
					$user_email = htmlspecialchars($this->input->post("user_email"));
					$user_fname = htmlspecialchars($this->input->post("user_fname"));
					$user_lname = htmlspecialchars($this->input->post("user_lname"));
					$user_gender = htmlspecialchars($this->input->post("user_gender"));
					if($user_gender=='male')
						$user_pic = 'malesample.png';
					else
						$user_pic = 'femalesample.png';
					$user_city = htmlspecialchars($this->input->post("user_city"));
					$user_country = htmlspecialchars($this->input->post("user_country"));
					$user_password = htmlspecialchars($this->input->post("user_password"));
					$random_string = $this->randomstring(15);
					$confirmation_url = base_url()."citizen/registrationconfirmation/".$username.'/'.$random_string;
					$this->email->from('info@citizenhub.com', 'Citizen Hub');
					$this->email->to($user_email);
					$this->email->subject('Registration Confirmation | Citizen Hub');
					$this->email->message('Hi '.$user_fname.' '.$user_lname.',		
					
					Click here to confirm your Registration: '.$confirmation_url.'
					
					If you have any questions, feel free to contact us.
					
					Regards,
					
					The Citizen Hub Team
					+92 000 000000
					info@citizenhub.com');
					if(@$this->email->send()) {
						$insert_data = array(
							'UserName' => $username,
							'UserImage'=> $user_pic,
							'UserEmail'=> $user_email,
							'UserFName' => $user_fname ,
							'UserLName' => $user_lname ,
							'UserGender' => $user_gender,
							'UserCity' => $user_city ,
							'UserCountry' => $user_country ,
							'UserStatus' => $random_string,//'Ok43kjht09',//$random_string,
							'UserPass' => md5($user_password)
						);
						if($this->citizenmodel->insert_record('users',$insert_data))
							$data['signup_conf_msg'] = 'A Signup confirmation link is sent in your email.<br />Kindly check your mail box and spam as well and click on the link to confirm your registration.'; 
						else
							$data['signup_conf_msg'] = 'Oops! Something went Wrong, Kindly try again for registration or contact support.';
					}
					else
						$data['signup_conf_msg'] = 'Oops! Something went Wrong, Kindly try again for registration or contact support.';
					$this->load->view('signup_confirm',$data);
				}
			}
		}
		else { 
			$this->index();
		}
	}
	
	/*Confirming newly registered user through email*/
	public function registrationconfirmation($username = '', $con_string = '') {
		if($this->citizenmodel->get_specific_data_row('users','UserName',$username)) {
			if($this->citizenmodel->get_specific_data_row('users','UserStatus',$con_string)) {
				$update_data = array(
					'UserStatus'=> 'Ok43kjht09'
				);
				if($this->citizenmodel->update_record('users',$update_data,'UserName',$username))
				$data['signup_conf_msg'] = 'Thanks for confirming your registration.<br /><a href="'.base_url().'citizen/login">Clink me link to login </a>';
				else
					$data['signup_conf_msg'] = 'Oops! Something went Wrong while confirming your registration.<br /> Kindly try again for registration or contact support.';
			}
			else
				$data['signup_conf_msg'] = 'Sorry you had an Invalid link for confirmation of registration.';
		}
		else
			$data['signup_conf_msg'] = 'Sorry you had an Invalid link for confirmation of registration.';
		$this->load->view('signup_confirm',$data);
	}
	
	/* Authenticates credentials provided for login*/
	public function login() {
		if(!is_logged_in()) {
			$header['welcome'] = "Welcome to site" ;
			$data['header'] = $header;
			$login_rules_array = array(
				array(
					'field'   => 'user_log',
					'label'   => 'Username',
					'rules'   => 'required|min_length[4]'
				),
				array(
					'field'   => 'pass_log',
					'label'   => 'Password',
					'rules'   => 'required|min_length[6]'
				)
			);
			$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '<button class="close" data-dismiss="alert" type="button">�</button></div>');
			$this->form_validation->set_rules($login_rules_array);
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('login',$data);
			}
			else {
				$username = htmlspecialchars($this->input->post("user_log"));
				$password = htmlspecialchars($this->input->post("pass_log"));
				$result = $this->citizenmodel->user_login($username,md5($password));
				if(count($result)==1) {
					$this->session->set_userdata('username',$result->UserName);
					$this->session->set_userdata('userpass',$result->UserPass);
					$this->session->set_userdata('userid',$result->user_id);
					$this->session->set_userdata('location_updated','not_updated');
					$this->index();
				}
				else {
					$data['not_valid'] = '<div class="alert alert-error">Username or Password is Incorrect.<button class="close" data-dismiss="alert" type="button">�</button></div>';
					$this->load->view('login',$data);
				}
			}
		}
		else {
			$this->index();
		}
	}
	
	/*Check availability of newly choosen username (Called through ajax)*/
	public function checkusername() {
		$username = htmlspecialchars(("username"));
		$user_check  = $this->citizenmodel->get_specific_data_row('users','UserName',$username);
		if($user_check)
			echo false;
		else
			echo true;
	}
	
	function collaborative_filtering($key = '') {
		$tags_id = explode(',',$key);
		$result = array();
		$i = 0;
		foreach($tags_id as $id) {
			if(is_numeric($id)) {
				$temp = $this->citizenmodel->like_query_data_array('news_articles','news_tag',$id);
				if($temp) {
					$i = $i + count($temp);
					$result = array_merge($result,$temp);
				}
			}
			if($i>=3) {
				break;
			}
		}
		return $result;
	}
	
	/*Username Callback*/
	public function username_check($username) {
		$user_check  = $this->citizenmodel->get_specific_data_row('users','UserName',$username);
		if($user_check) {
			$this->form_validation->set_message('username_check', 'Username you selected is not availabe.');
			return false;
		}
		else
			return true;
	}
	
	/*Email CallBack*/
	public function email_check($user_email) {
		$email_check  = $this->citizenmodel->get_specific_data_row('users','UserEmail',$user_email);
		if($email_check) {
			$this->form_validation->set_message('email_check', 'Email you Entered is already Registered.');
			return false;
		}
		else
			return true;
	}
	
	/*Generates a random string of given input length*/
	public function randomstring($length = 15) {
		$charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		$str = '';
		$count = strlen($charset);
		while ($length--) {
			$str .= $charset[mt_rand(0, $count-1)];
		}
		return $str;
	}
	
	/*Deleting all the sessions created for a logged in user and logging him out from system*/
	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('userpass');
		$this->session->unset_userdata('userid');
		$this->session->unset_userdata('location_updated');
		$this->session->sess_destroy();
		$this->load->view('login');
	}
	
	/*Contact us page*/
	public function contactus() {
		$data['not_valid'] = '';
		if($this->session->flashdata('contact_valid')) {
			$data['not_valid'] = $this->session->flashdata('contact_valid');
			$this->load->view('contact',$data);
		}
		else {
			$contact_rules_array = array(
				array(
					'field'   => 'full_name',
					'label'   => 'Full Name',
					'rules'   => 'required|min_length[3]'
				),
				array(
					'field'   => 'Email',
					'label'   => 'Email',
					'rules'   => 'required|valid_email'
				),
				array(
					'field'   => 'Subject',
					'label'   => 'Subject',
					'rules'   => 'required|min_length[5]'
				),
				array(
					'field'   => 'message',
					'label'   => 'Message',
					'rules'   => 'required|min_length[5]'
				)
			);
			$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '<button class="close" data-dismiss="alert" type="button">�</button></div>');
			$this->form_validation->set_rules($contact_rules_array);
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('contact',$data);
			}
			else {
				$full_name = htmlspecialchars($this->input->post("full_name"));
				$email = htmlspecialchars($this->input->post("Email"));
				$subject = htmlspecialchars($this->input->post("Subject"));
				$message = htmlspecialchars($this->input->post("message"));
				$insert_data = array(
					'contact_name' => $full_name,
					'contact_email' => $email,
					'contact_subject' => $subject,
					'contact_msg' => $message
				);
				if($this->citizenmodel->insert_record('contactus',$insert_data)) {
					$this->session->set_flashdata('contact_valid', '<div class="alert alert-success">Your Message is posted Successfully; Our team will contact you soon.</div>');
					redirect(base_url().'citizen/contactus','location');
				}
				else {
					$data['not_valid'] = '<div class="alert alert-error">Some thing went wrong while posting your message; Please try again.<button class="close" data-dismiss="alert" type="button">�</button></div>';
					$this->load->view('contact',$data);
				}
			}
		}
	}
	
	public function trends() {
		$data['title'] = 'Trends';
		$this->load->library('alchemyapi');
		$trend_rules_array = array(
			array(
				'field'   => 'tag_name',
				'label'   => 'Tag Name',
				'rules'   => 'required|min_length[3]'
			)
		);
		$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '<button class="close" data-dismiss="alert" type="button">�</button></div>');
		$this->form_validation->set_rules($trend_rules_array);
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('trends',$data);
		}
		else {
			$tag = strtolower($this->input->post('tag_name'));
			if($tag[0]=='@')
				{}
			else
				$tag = '@'.$tag;
			$temp_tag = $this->citizenmodel->get_specific_data_row('tags','TagName',$tag);
			if($temp_tag) {
				$tag_id = ','.$temp_tag->TagID.',';
				$tag_news = $this->citizenmodel->like_query_data_array('news_articles','news_tag',$tag_id);
				if(count($tag_news) > 0) {
					$positive = 0;
					$negative = 0;
					$nutral = 0;
					$ent_positive = 0;
					$ent_negative = 0;
					$ent_nutral = 0;
					$total_news = count($tag_news);
					foreach($tag_news as $t_news) {
						$text = $t_news["news_heading"].' '.$t_news["news_text"];  
						$response = $this->alchemyapi->sentiment('text',$text,null);
						assert($response['status'] == 'OK');
						$result = strtolower($response['docSentiment']['type']);
						if($result == 'positive')
							$positive++;
						else if($result == 'negative')
							$negative++;
						else
							$nutral++;
						$response_1 = $this->alchemyapi->entities('text', $text, null);
						assert($response_1['status'] == 'OK');
						$entities = $response_1['entities'];
						if($entities) {
							foreach($entities as $entity) {
								if(strtolower('@'.$entity['text']) == $tag) {
									$ent_result = strtolower($entity['type']);
									if($ent_result == 'positive')
										$ent_positive++;
									else if($ent_result == 'negative')
										$ent_negative++;
									else
										$ent_nutral++;
								}
							}
						}
					}
					
					$data['result']['positive'] = ($positive/$total_news)*100;
					$data['result']['negative'] = ($negative/$total_news)*100;
					$data['result']['mixed'] = ($nutral/$total_news)*100;
					
					if($ent_positive>0 || $ent_negative>0 || $ent_nutral>0) {
						$data['ent_result']['positive'] = ($ent_positive/$total_news)*100;
						$data['ent_result']['negative'] = ($ent_negative/$total_news)*100;
						$data['ent_result']['mixed'] = ($ent_nutral/$total_news)*100;
					}
				}
				else
					$data['not_valid'] = '<div class="alert alert-error">No News posted for this tag yet.<button class="close" data-dismiss="alert" type="button">�</button></div>';
			}
			else
				$data['not_valid'] = '<div class="alert alert-error">No data for this tag.<button class="close" data-dismiss="alert" type="button">�</button></div>';
			$this->load->view('trends',$data);
		}
	}
	
	public function tag_check($tag_name) {
		if($tag_name[0] != '@') {
			$this->form_validation->set_message('tag_check', 'Enter a valid tag name starting with @.');
			return false;
		}
		else
			return true;
	}
	
	public function people_auto_complete() {
		$search_key  = $_GET['term'];
		if($search_key[0]=='@'){}
		else
			$search_key = '@'.$search_key;
		$search_results = $this->citizenmodel->like_query_data_array('tags','TagName',$search_key);
		foreach($search_results as $result) {
			$results[] = array('label' => $result['TagName']);
		}
		echo json_encode($results);
	} 
	
	public function aboutus() {
		$this->load->view('about');
	}
	
	/*Rss-Feeds of other websites*/
	public function rssfeeds() {
		$this->load->view('rss-feed');
	}
	
	public function update_user_location() {
		if(is_logged_in()) {
			$lat = $this->input->post('lat');
			$lng = $this->input->post('lng');
			$user_id = $this->session->userdata('userid');
			$lat = number_format($lat, 2, '.', '');
			$lng = number_format($lng, 2, '.', '');
			$update_data = array(
				'UserLocationLat' => $lat,
				'UserLocationLng' => $lng
			);
			if($this->citizenmodel->update_record('users',$update_data,'user_id',$user_id)) {
				$this->session->set_userdata('location_updated','updated');
				$this->session->set_userdata('user_lat',$lat);
				$this->session->set_userdata('user_lng',$lng);
				return '1';
			}
			else
				return '0';
		}
	}
	
	public function delete_post($post_id) {
		if(is_logged_in()) {
			$user_id = $this->session->userdata('userid');
			$temp = $this->citizenmodel->get_two_columns_data_array('news_articles','news_id',$post_id,'updated_by',$user_id);
			if($temp) {
				if($this->citizenmodel->delete_record('news_articles','news_id',$post_id)) {
					$this->session->set_flashdata('not_valid','<div class="alert alert-success">Post Deleted Successfully.</div>');
				}
			}
			else {
				$this->session->set_flashdata('not_valid','<div class="alert alert-error">Data does not exist.</div>');
			}
			redirect(base_url().'citizen/myprofile/'.is_logged_in());
		}
		else {
			redirect(base_url().'citizen/login','location');
		}
	}
	
	/*Generating RSS FEEDS */
	public function create_rss_feeds($data , $cat_name = '') {
		//echo "Test";
		$citizenhubfeed = '<?xml version="1.0" encoding="ISO-8859-1"?>';
		$citizenhubfeed .= '<rss version="2.0">';
    	$citizenhubfeed .= '<channel>';
        $citizenhubfeed .= '<title>CITIZEN-HUB RSS feed</title>';
        $citizenhubfeed .= '<link>http://www.citizenhub.com/</link>';
        $citizenhubfeed .= '<description>Citizen-Hub RSS feed for '.$cat_name.'</description>';
        $citizenhubfeed .= '<language>en-us</language>';
        $citizenhubfeed .= '<copyright>Copyright (C) 2013-14 citizenhub.com</copyright>';
		$citizenhubfeed .= '</channel>';
		foreach($data as $item) {
			$citizenhubfeed .= '<item>';
			$citizenhubfeed .= '<title>'.$item['news_heading'].'</title>';
			$citizenhubfeed .= '<description>'.substr($item['news_text'],0,370).'</description>';
			$citizenhubfeed .= '<link>'.base_url().'citizen/singlepost/Latest/'.$item['news_category'].'/'.$item['news_id'].'</link>';
			$citizenhubfeed .= '<pubDate>' . date("D, d M Y", strtotime($item['upload_time'])) . '</pubDate>';
			$citizenhubfeed .= '</item>';
		}
        $citizenhubfeed .= '</rss>';
		write_file('./rss/index.xml', $citizenhubfeed,'w');
	}
	
}
