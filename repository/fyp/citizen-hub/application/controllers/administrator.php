<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administrator extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('adminmodel');
		$this->load->library('form_validation');
		$this->load->helper('admin');
		$this->load->library('recaptcha');
	}
	
	/*Home page function (Main)*/
	public function index($msg = '') {
		if(is_admin_in()) {
			$data['msg'] = $msg;
			$this->load->view('Admin/admin',$data);
		}
		else {
			$this->login();
		}
	}
	
	public function manage_users() {
		if(is_admin_in()) {
			$data['title'] = 'User Management';
			$users = $this->adminmodel->get_data('users','user_id','DESC');
			$user_view = '';
			if($users) {
				$user_view .= '<div class="data-item" id="light-box-bg">
				<div class="block" style="width:200px;" id="left_align">
					<span><strong>UserName</strong></span>
				</div>
				<div class="block" style="width:270px;" id="left_align">
					<span><strong>Full Name</strong></span>
				</div>
				<div class="block" style="width:200px;" id="left_align">
					<span><strong>Email</strong></span>
				</div>
				<div class="block" style="width:150px;" id="left_align">
					<span><strong>Total Posts</strong></span>
				</div>
				<div class="block" style="width:80px;">
					<span><strong>Activation</strong></span>
				</div>
                </div>';
			}
			$i = 0;
			foreach($users as $user) {
				$uri_cat_name = url_title($user['UserName']);
				if($i%2==0)
					$user_view .= '<div class="data-item" id="light-bg">';
				else
					$user_view .= '<div class="data-item">';
				$post_temp = $this->adminmodel->get_specific_data_array('news_articles','updated_by',$user['user_id']);
				$user_view .= '<div class="block" id="left_align" style="width:200px;">'.$user['UserName'].'</div>
				<div class="block" id="left_align" style="width:270px;">'.$user['UserFName'].' '.$user['UserLName'].'</div>
				<div class="block" id="left_align" style="width:200px;">'.$user['UserEmail'].'</div>
				<div class="block" id="left_align" style="width:150px;">'.count($post_temp).'</div>
				<div class="block" style="width:80px;">';
				if($user['UserStatus']=='Ok43kjht09') {
					$user_view .= '<img id="user_icon_'.$user['user_id'].'" onclick="javascript:activate_user('.$user['user_id'].',0)" src="'.base_url().'html/img/delete_icon.png" />';
				}
				else {
					$user_view .= '<img id="user_icon_'.$user['user_id'].'" onclick="javascript:activate_user('.$user['user_id'].',1)" src="'.base_url().'html/img/enable_icon.png" />';
				}
				$user_view .= '</div>
				</div>';
				$i++;
			}
			$data['user_view'] = $user_view;
			$this->load->view('Admin/manage',$data);
		}
		else
			$this->login();
	}
	
	public function manage_posts() {
		if(is_admin_in()) {
			$data['title'] = 'Post Management';
			$posts = $this->adminmodel->get_data('news_articles','upload_time','DESC');
			$posts_view = '';
			if($posts) {
				$posts_view .= '<div class="data-item" id="light-box-bg">
				<div class="block" style="width:420px;" id="left_align">
					<span><strong>Title</strong></span>
				</div>
				<div class="block" style="width:100px;" id="left_align">
					<span><strong>Posted By</strong></span>
				</div>
				<div class="block" style="width:150px;" id="left_align">
					<span><strong>Posted On</strong></span>
				</div>
				<div class="block" style="width:100px;" id="left_align">
					<span><strong>Endorsments</strong></span>
				</div>
				<div class="block" style="width:80px;">
					<span><strong>Rating</strong></span>
				</div>
				<div class="block" style="width:80px;">
					<span><strong>Activation</strong></span>
				</div>
                </div>';
			}
			$i = 0;
			foreach($posts as $post) {
				if($i%2==0)
					$posts_view .= '<div class="data-item" id="light-bg">';
				else
					$posts_view .= '<div class="data-item">';
				$post_temp = $this->adminmodel->get_comments_sorted_array('news_id',$post['news_id'],'1','news_id','DESC');
				$user_temp = $this->adminmodel->get_specific_data_row('users','user_id',$post['updated_by']);
				$posts_view .= '<div class="block" id="left_align" style="width:420px; text-align:left">'.$post['news_heading'].'</div>
				<div class="block" id="left_align" style="width:100px;">'.$user_temp->UserName.'</div>
				<div class="block" id="left_align" style="width:150px;">'.$post['upload_time'].'</div>
				<div class="block" id="left_align" style="width:100px;">'.count($post_temp).'</div>
				<div class="block" style="width:80px;">'.$post['overall_rating'].'</div>
				<div class="block" style="width:80px;">';
				if($post['news_status']==1) {
					$posts_view .= '<img id="news_icon_'.$post['news_id'].'" onclick="javascript:activate_post('.$post['news_id'].',0)" src="'.base_url().'html/img/delete_icon.png" />';
				}
				else {
					$posts_view .= '<img id="news_icon_'.$post['news_id'].'" onclick="javascript:activate_post('.$post['news_id'].',1)" src="'.base_url().'html/img/enable_icon.png" />';
				}
				$posts_view .= '</div>
				</div>';
				$i++;
			}
			$data['user_view'] = $posts_view;
			$this->load->view('Admin/manage',$data);
		}
		else
			$this->login();
	}
	
	public function spam_posts() {
		if(is_admin_in()) {
			$data['title'] = 'Reported Posts Management';
			$reported_posts = $this->adminmodel->abuse_reports();
			$posts_view = '';
			if($reported_posts) {
				$posts_view .= '<div class="data-item" id="light-box-bg">
				<div class="block" style="width:330px;" id="left_align">
					<span><strong>Title</strong></span>
				</div>
				<div class="block" style="width:100px;" id="left_align">
					<span><strong>Posted By</strong></span>
				</div>
				<div class="block" style="width:150px;" id="left_align">
					<span><strong>Posted On</strong></span>
				</div>
				<div class="block" style="width:100px;" id="left_align">
					<span><strong>Endorsments</strong></span>
				</div>
				<div class="block" style="width:80px;">
					<span><strong>Rating</strong></span>
				</div>
				<div class="block" style="width:80px;">
					<span><strong>Reported Count</strong></span>
				</div>
				<div class="block" style="width:80px;">
					<span><strong>Delete</strong></span>
				</div>
                </div>';
			}
			$i = 0;
			foreach($reported_posts as $reported_post) {
				$post = $this->adminmodel->get_specific_data_row('news_articles','news_id',$reported_post['news_id']);
				$posts_view .= '<div id="post_'.$post->news_id.'">';
				if($i%2==0)
					$posts_view .= '<div class="data-item" id="light-bg">';
				else
					$posts_view .= '<div class="data-item">';
				$post_temp = $this->adminmodel->get_comments_sorted_array('news_id',$post->news_id,'1','news_id','DESC');
				$user_temp = $this->adminmodel->get_specific_data_row('users','user_id',$post->updated_by);
				$posts_view .= '<div class="block" id="left_align" style="width:330px; text-align:left">'.$post->news_heading.'</div>
				<div class="block" id="left_align" style="width:100px;">'.$user_temp->UserName.'</div>
				<div class="block" id="left_align" style="width:150px;">'.$post->upload_time.'</div>
				<div class="block" id="left_align" style="width:100px;">'.count($post_temp).'</div>
				<div class="block" style="width:80px;">'.$post->overall_rating.'</div>
				<div class="block" style="width:80px;">'.$reported_post['report_counter'].'</div>
				<div class="block" style="width:80px;">';
				$posts_view .= '<img id="news_icon_'.$post->news_id.'" onclick="javascript:delete_reported_post('.$post->news_id.',0)" src="'.base_url().'html/img/delete_icon.png" />';
				$posts_view .= '</div></div>
				</div>';
				$i++;
			}
			$data['user_view'] = $posts_view;
			$this->load->view('Admin/manage',$data);
		}
		else
			$this->login();
	}
	
	public function usersfeedback() {
		if(is_admin_in()) {
			if($this->session->flashdata('done_msg'))
				$data['done_msg'] = $this->session->flashdata('done_msg');
			$data['title'] = 'Feedback Management';
			$posts = $this->adminmodel->get_data('contactus','contact_time','DESC');
			$posts_view = '';
			if($posts) {
				$posts_view .= '<div class="data-item" id="light-box-bg">
				<div class="block" style="width:100px;" id="left_align">
					<span><strong>Name</strong></span>
				</div>
				<div class="block" style="width:200px;" id="left_align">
					<span><strong>Email</strong></span>
				</div>
				<div class="block" style="width:150px;" id="left_align">
					<span><strong>Subject</strong></span>
				</div>
				<div class="block" style="width:100px;" id="left_align">
					<span><strong>Time</strong></span>
				</div>
				<div class="block" style="width:360px;" id="left_align">
					<span><strong>Message</strong></span>
				</div>
				<div class="block" style="width:30px;">
					<span><strong>Delete</strong></span>
				</div>
                </div>';
			}
			$i = 0;
			foreach($posts as $post) {
				if($i%2==0)
					$posts_view .= '<div class="data-item" id="light-bg" style="font-size:11px;">';
				else
					$posts_view .= '<div class="data-item" style="font-size:11px;">';
				$posts_view .= '<div class="block" id="left_align" style="width:100px;">'.$post['contact_name'].'</div>
				<div class="block" id="left_align" style="width:200px;">'.$post['contact_email'].'</div>
				<div class="block" id="left_align" style="width:150px;">'.$post['contact_subject'].'</div>
				<div class="block" id="left_align" style="width:100px;">'.$post['contact_time'].'</div>
				<div class="block" style="width:360px;">'.$post['contact_msg'].'</div>
				<div class="block" style="width:30px;">';
				if($post['contact_status']==1) {
					$posts_view .= '<a href="'.base_url().'administrator/deleteitem/contact/'.$post['contact_id'].'">
					<img src="'.base_url().'html/img/delete_icon.png" /></a>';
				}
				$posts_view .= '</div>
				</div>';
				$i++;
			}
			$data['user_view'] = $posts_view;
			$this->load->view('Admin/manage',$data);
		}
		else
			$this->login();
	}
	
	public function deleteitem($type='',$id=0) {
		if(is_admin_in()) {
			if($type!='' && $id>0) {
				if($this->adminmodel->delete_record('contactus','contact_id',$id)) {
					$this->session->set_flashdata('done_msg','<div class="alert alert-success">Deleted Successfully.</div>');
					redirect(base_url().'administrator/usersfeedback','location');
				}
				else {
					$this->session->set_flashdata('done_msg','<div class="alert alert-error">Something went wrong while deleting a post; Please try again.</div>');
					redirect(base_url().'administrator/usersfeedback','location');
				}
			}
			else
				$this->index();
		}
		else
			$this->login();
	}
	
	public function user_activation() {
		if(is_admin_in()) {
			$user_id = $this->input->post('user_id');
			$user_status = $this->input->post('status');
			if($user_status == '1')
				$user_status = 'Ok43kjht09';
			else
				$user_status = 'not';
			
			$updata_data = array(
				'UserStatus' => $user_status
			);
			if($this->adminmodel->update_record('users',$updata_data,'user_id',$user_id))
				return 1;
			else
				return 0;
		}
		else
			return 0;
	}
	
	public function post_activation() {
		if(is_admin_in()) {
			$news_id = $this->input->post('news_id');
			$post_status = $this->input->post('status');
			$updata_data = array(
				'news_status' => $post_status
			);
			if($this->adminmodel->update_record('news_articles',$updata_data,'news_id',$news_id))
				return 1;
			else
				return 0;
		}
		else
			return 0;
	}
	
	public function post_delete() {
		if(is_admin_in()) {
			$news_id = $this->input->post('news_id');
			$post_status = $this->input->post('status');
			if($this->adminmodel->delete_record('news_articles','news_id',$news_id))
				if($this->adminmodel->delete_record('spam_reports','news_id',$news_id)) {
					$d = $this->adminmodel->delete_record('user_notifications','news_id',$news_id);
					return 1;
				}
				else
					return 0;
			else
				return 0;
		}
		else
			return 0;
	}
	
	
	public function login() {
		if(!is_admin_in()) {
			$data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();
			$login_rules_array = array(
				array(
					'field'   => 'admin_user_log',
					'label'   => 'Username',
					'rules'   => 'required|min_length[4]'
				),
				array(
					'field'   => 'admin_pass_log',
					'label'   => 'Password',
					'rules'   => 'required|min_length[5]'
				)
			);
			$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '<button class="close" data-dismiss="alert" type="button">×</button></div>');
			$this->form_validation->set_rules($login_rules_array);
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('Admin/login',$data);
			}
			else {
				$resp = $this->recaptcha->recaptcha_check_answer(
                                        $_SERVER["REMOTE_ADDR"],
                                        $_POST["recaptcha_challenge_field"],
                                        $_POST["recaptcha_response_field"]);
				if(!$this->recaptcha->getIsValid()) {
					$data['not_valid'] = '<div class="alert alert-error">Captcha you entered is incorrect! Re-Enter and try again.<button class="close" data-dismiss="alert" type="button">×</button></div>';
					$this->load->view('Admin/login',$data);
				}
				else {
					$username = htmlspecialchars($this->input->post("admin_user_log"));
					$password = htmlspecialchars($this->input->post("admin_pass_log"));
					$result = $this->adminmodel->admin_login($username,md5($password));
					if(count($result)==1) {
						$this->session->set_userdata('adminname',$result->admin_user);
						$this->session->set_userdata('adminpass',$result->admin_pass);
						$this->session->set_userdata('adminid',$result->admin_id);
						$this->index();
					}
					else {
						$data['not_valid'] = '<div class="alert alert-error">Username or Password is Incorrect.<button class="close" data-dismiss="alert" type="button">×</button></div>';
						$this->load->view('Admin/login',$data);
					}
				}
			}
		}
		else
			$this->index();
	}
	
	public function logout() {
		$this->session->unset_userdata('adminname');
		$this->session->unset_userdata('adminpass');
		$this->session->unset_userdata('adminid');
		$this->session->sess_destroy();
		redirect(base_url().'administrator/login','location');
	}
}
