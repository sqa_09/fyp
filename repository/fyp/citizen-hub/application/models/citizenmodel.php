<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CitizenModel extends CI_Model
{
	function __construct()
	{
		parent::__construct();	
	}
	/*
	*	Generic model class contaning generic functions to perform database operations
	*
	*	Keyword used in function arguments
	*		-$table = Name of the table in database on which operation is going to be performed
	*		-$data = An array filled with field names and data belonging to those fields
	*		-$column = single field name of respective table on which operation is going to be performed
	*		-$key = Value of single field to match in where class
	*	
	*/
	
	/*Authenticating user login from Database*/
	function user_login($username,$password)
	{
		$this->db->where('UserName',$username);
		$this->db->where('UserPass',$password);
		$this->db->where('UserStatus','Ok43kjht09');
		$query = $this->db->get('users');
		return $query->row();
	}
	
	/*Extracting Whole data from a table*/
	function get_data($table)
	{
		$query = $this->db->get($table);
		return $query->result_array();
	}
	
	/*Extracting Active data from a table*/
	function get_active_data($table,$column)
	{
		$this->db->where($column, 1);
		$query = $this->db->get($table);
		return $query->result_array();
	}
	
	/*Extracting a specific row from a table*/
	function get_specific_data_row($table,$column,$key)
	{
		$this->db->where($column, $key);
		$query = $this->db->get($table);
		return $query->row();
	}
	
	/*Extracting more then one rows from a table and returning in array*/
	function get_specific_data_array($table,$column,$key)
	{
		$this->db->where($column, $key);
		$query = $this->db->get($table);
		return $query->result_array();
	}
	
	/*Extracting more then one rows from a table using two column match and returning in array*/
	function get_two_columns_data_array($table,$column,$key,$column2,$key2)
	{
		$this->db->where($column, $key);
		$this->db->where($column2, $key2);
		$query = $this->db->get($table);
		return $query->result_array();
	}
	
	function get_notifications($table,$column,$key,$column2,$key2)
	{
		$this->db->where($column, $key);
		$this->db->where($column2, $key2);
		$this->db->order_by('notification_id','DESC');
		$query = $this->db->get($table);
		return $query->result_array();
	}
	
	/*Like Query returning array*/
	function like_query_data_array($table,$column,$key)
	{
		$this->db->like($column, $key);
		$query = $this->db->get($table);
		return $query->result_array();
	}
	/*Sorted results array*/
	function get_specific_data_sorted_array($table,$column,$key,$sortcol,$sorttype)
	{
		$this->db->where($column, $key);
		$this->db->order_by($sortcol, $sorttype); 
		$query = $this->db->get($table);
		return $query->result_array();
	}
	
	/*Get Data with sort and limit*/
	function get_active_sort_data_limit($table,$column,$sortcol,$sorttype,$limit)
	{
		$this->db->where($column, 1);
		$this->db->order_by($sortcol, $sorttype);
		$this->db->limit($limit); 
		$query = $this->db->get($table);
		return $query->result_array();
	}
	function get_active_data_sorted_array($table,$columnstatus,$column,$key,$sortcol,$sorttype,$limit)
	{
		$this->db->where($columnstatus, 1);
		$this->db->where($column, $key);
		$this->db->order_by($sortcol, $sorttype); 
		$this->db->limit($limit); 
		$query = $this->db->get($table);
		return $query->result_array();
	}
	/*Get all comments sorted*/
	function get_comments_sorted_array($column,$key,$endorsement_type,$sortcol,$sorttype)
	{
		$this->db->where($column, $key);
		$this->db->where('edrosement_type', $endorsement_type);
		$this->db->order_by($sortcol, $sorttype); 
		$query = $this->db->get('endorsements');
		return $query->result_array();
	}
	
	/*Check endorsement of a specific user and specific news*/
	function checkendorsement($userid,$news_id,$endorsement_type)
	{
		$this->db->where('news_id', $news_id);
		$this->db->where('endrosed_by', $userid);
		$this->db->where('edrosement_type', $endorsement_type);
		$query = $this->db->get('endorsements');
		return $query->result_array();
	}
	
	/*Check Subscription of a specific user and specific news*/
	function checksubsucription($userid,$news_id)
	{
		$this->db->where('news_id', $news_id);
		$this->db->where('user_id', $userid);
		$query = $this->db->get('user_subscription');
		return $query->result_array();
	}
	
	/*Check spams reports of a specific user and specific news*/
	function check_spam_reports($userid,$news_id)
	{
		$this->db->where('news_id', $news_id);
		$this->db->where('spam_reporter', $userid);
		$this->db->where('spam_status', 1);
		$query = $this->db->get('spam_reports');
		return $query->row();
	}
	
	function get_rating($news_id,$endorsement_type)
	{
		$this->db->select_avg('ratings','avg_rate');
		$this->db->where('news_id', $news_id);
		$this->db->where('edrosement_type', $endorsement_type);
		$query = $this->db->get('endorsements');
		$result = $query->row();
		return round($result->avg_rate,1);
	}
	
	function get_user_rating($user_id)
	{
		$this->db->select_avg('overall_rating','avg_rate');
		$this->db->where('updated_by', $user_id);
		$query = $this->db->get('news_articles');
		$result = $query->row();
		return round($result->avg_rate,1);
	}
	
	/*Searching data*/
	function search_data_array($key)
	{
		$this->db->like('news_heading', $key);
		$this->db->or_like('news_text', $key);
		$query = $this->db->get('news_articles');
		return $query->result_array();
	}
	
	/*Inserting record in database through array (one or more then one column)*/
	function insert_record($table,$data) 
	{
		if($this->db->insert($table, $data))
			return 1;
		else
			return 0;
	}
	
	/*Inserting record in database through array (one or more then one column) 
	and returning the newly generated (through auto increment) primry key */
	function insert_record_key($table,$data) 
	{
		if($this->db->insert($table, $data))
			return $this->db->insert_id();
		else
			return 0;
	}
	
	/*Updating a record in database through array (one or more then one column)*/
	function update_record($table,$data,$column,$key) 
	{
		$this->db->where($column,$key);
		if($this->db->update($table, $data))
			return 1;
		else
			return 0;
	}
	
	/*Deleting a record from database*/
	function delete_record($table,$column,$key) 
	{
		$this->db->where($column, $key);
		if($this->db->delete($table))
			return 1;
		else
			return 0;
	}
	
	/*Deleting data using array from database*/
	function delete_record_array($table,$data) 
	{
		if($this->db->delete($table, $data))
			return 1;
		else
			return 0;
	}
	
	/*Searching people to tag in post*/
	function search_people($table,$key)
	{
		$this->db->like('UserName', $key);
		$this->db->where('UserPrivacy',1);
		$this->db->where('UserStatus','Ok43kjht09');
		$query = $this->db->get($table);
		return $query->result_array();
	}
}