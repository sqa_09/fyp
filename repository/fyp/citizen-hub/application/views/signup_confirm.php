        <?php 
		include "templates/header.php";
		?>
        <div class="main-title">
            <p>Registration Confirmation</p>
        </div>
		<?php 
			echo validation_errors(); 
			if(isset($not_valid)) echo $not_valid;
		?>
		<!-- start div #main -->
	    <div id="main">
            <div class="main-content">
                <div class="full-width" style="min-height:250px;">
                    <!--<div class="title-box">
                        <h1>Login</h1>
                    </div>-->
                    <div class="contact-content">
                        <div class="contact-form">
                        	<?=$signup_conf_msg?>
                        </div>
                    </div>
                </div> 
                <div class="clear"></div>
            </div>	
        </div>
	    <!-- end div #main --> 
    </div>
	<!-- end div #wrapper -->
    <?php 
	include "templates/footer.php";
	?>