		<?php include "templates/header.php"; ?>
		
        <!-- start div #main-title -->
		<div class="main-title">
            <p>Profile</p>
        </div>
        <!-- end div #main-title -->
        
		<!-- start div #main -->
	    <div id="main">
            <div class="main-content">
                <div class="features">
                    <div class="row-fluid">
                    	<div class="span2">
                            <div class="post-content">
                                <div class="rating-box" style="width:90%;">
                                    <div class="item">
                                        <p>Your Rating</p>
                                        <script type="text/javascript">
                                            $(document).ready(function(){
                                                $('.myrating').jRating({
                                                    bigStarsPath : '<?=base_url()?>html/rating/icons/stars.png',
                                                    smallStarsPath: '<?=base_url()?>html/rating/icons/small.png',
                                                    phpPath: '<?=base_url()?>posts/ratepost/',
                                                    isDisabled:true
                                                });
                                            });
                                        </script>
                                        <ul class="rating calc">
                                            <div class="myrating" data-average="<?=$user_rating?>" data-id="1"></div>
                                        </ul>
                                    </div>
                                    <div class="item" style="float:left; width:100%;">
                                    	<div style="width:30%; float:left;"><p>San-Saniness:</p></div>
                                    	<div style="float:right; width:60%; height:30px; border:1px solid #333; margin-top:5px;
                                        -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px;">
                                        	<div style="width:<?=$user_rating*10?>%; background:#CC0; float:left; height:20px; padding:5px 0;
                                            -webkit-border-top-left-radius: 5px;
                                            -webkit-border-bottom-left-radius: 5px;
                                            -moz-border-radius-topleft: 5px;
                                            -moz-border-radius-bottomleft: 5px;
                                            border-top-left-radius: 5px;
                                            border-bottom-left-radius: 5px;">
                                            	<span style="margin-left:10px; font-size:16px; font-weight:bold;"><?=$user_rating*10?>%</span>
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="item" style="float:left; width:100%;">
                                    	<div style="width:30%; float:left;"><p>Accuracy:</p></div>
                                    	<div style="float:right; width:60%; height:30px; border:1px solid #333; margin-top:5px;
                                        -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px;">
                                        	<div style="width:100%; background:#CC0; float:left; height:20px; padding:5px 0;
                                            -webkit-border-top-left-radius: 5px;
                                            -webkit-border-bottom-left-radius: 5px;
                                            -moz-border-radius-topleft: 5px;
                                            -moz-border-radius-bottomleft: 5px;
                                            border-top-left-radius: 5px;
                                            border-bottom-left-radius: 5px;">
                                            	<span style="margin-left:10px; font-size:16px; font-weight:bold;">100%</span>
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="item" style="float:left; width:100%;">
                                    	<div style="width:30%; float:left;"><p>Sharing:</p></div>
                                    	<div style="float:right; width:60%; height:30px; border:1px solid #333; margin-top:5px;
                                        -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px;">
                                        	<div style="width:<?=$user_rating*10/2+5?>%; background:#CC0; float:left; height:20px; padding:5px 0;
                                            -webkit-border-top-left-radius: 5px;
                                            -webkit-border-bottom-left-radius: 5px;
                                            -moz-border-radius-topleft: 5px;
                                            -moz-border-radius-bottomleft: 5px;
                                            border-top-left-radius: 5px;
                                            border-bottom-left-radius: 5px;">
                                            	<span style="margin-left:10px; font-size:16px; font-weight:bold;"><?=$user_rating*10/2+5?>%</span>
                                            </div>
                                        </div>  
                                    </div>
                                    
                                    <div class="total">
                                        <p class="left">total score</p>
                                        <p class="right"><?=$user_rating?></p>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="span2">
                            <div class="alert alert-block" style="padding:10px 20px 10px 10px; margin:0;">
                                <span><img src="<?=base_url()?>data/users/<?=$user_info->UserImage?>" width="120" height="120" /></span>
                                <h3 style="float:right;" id="yellow"><?=$username?></h3>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>

                    <div class="separator"></div>
                    	<?php if(isset($not_valid)) echo $not_valid;?>
                        <div class="span2">
                            <ul class="nav nav-tabs" id="myTab">
                                <li class="active"><a href="#tab-1" data-toggle="tab"><span class="icon"></span>Profile Information</a></li>
                                <li><a href="#tab-2" data-toggle="tab"><span class="icon"></span>My Posts</a></li>
                                <li><a href="#tab-3" data-toggle="tab"><span class="icon"></span>Subscriptions</a></li>
                                <!--<li><a href="#tab-4" data-toggle="tab"><span class="icon"></span>Notifications</a></li>-->
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-1">
                                    <div class="row-fluid">
                                        <div class="span2">
                                        	<h3 id="yellow" style="text-decoration:underline; margin:0px;">Personal Information</h3>
                                        	<table width="100%;" class="info">
                                            	<tr>
                                                	<td width="30%;"><span id="bold">Username:</span></td><td><?=$user_info->UserName?></td>
                                                </tr>
                                                <tr><td><span id="bold">First Name:</span></td>
                                                <td style="text-transform:capitalize"><?=$user_info->UserFName?></td></tr>
                                                <tr><td><span id="bold">Last Name:</span></td>
                                                <td style="text-transform:capitalize"><?=$user_info->UserLName?></td></tr>
                                                <tr><td><span id="bold">Email:</span></td><td><?=$user_info->UserEmail?></td></tr>
                                                <tr><td><span id="bold">Gender:</span></td>
                                                <td style="text-transform:capitalize"><?=$user_info->UserGender?></td></tr>
                                                <tr><td><span id="bold">Street Address:</span></td>
                                                <td style="text-transform:capitalize"><?=$user_info->UserAddress?></td></tr>
                                                <tr><td><span id="bold">City:</span></td>
                                                <td style="text-transform:capitalize"><?=$user_info->UserCity?></td></tr>
                                                <tr><td><span id="bold">Country:</span></td>
                                                <td style="text-transform:capitalize"><?=$user_info->UserCountry?></td></tr>
                                                <tr><td>&nbsp;</td>
                                                <td style="text-transform:capitalize">&nbsp;</td></tr>
                                            </table>
                                        </div>
                                        <div class="span2">
                                            <div class="widget">
                                                <h3 id="yellow" style="text-decoration:underline; margin:0;">Tags</h3>
                                                <ul class="tags">
                                                    <?=get_tags_from_ids($user_info->UserTags)?>
                                                </ul>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-2">
                                    <p> <?=$news_results?></p>
                                </div>
                                <div class="tab-pane" id="tab-3">
                                    <p><?=$subscription_results?></p>
                                </div>
                                <?php
                                /*<div class="tab-pane" id="tab-4">
                                    <p id="demo"><?=$notification_results?></p>
                                </div>*/
								
								?>
                            </div>
                        </div>
                </div>
                <div class="clear"></div>
            </div>	
        </div>
	    <!-- end div #main --> 
    
    </div>
	<!-- end div #wrapper -->    
    <?php include "templates/footer.php";?>
	<script type="text/javascript">
	
	function subscribepost(news_id,subscribe_status) {
	//	alert(endorse_type);
		$.ajax({
			url: '<?php echo base_url()."posts/subscribepost/"; ?>',
			async: false,
			type: "POST",
			data: {news_id:news_id, subscribe_status:subscribe_status},
			dataType: "html",
			success: function(data) {
				if(data=='1') {
					$('#subscribe_'+news_id).html('<a onclick="javascript:subscribepost('+news_id+',2)">Unsubscribe This post</a>');
					var count = $('#subscribe_count_'+news_id).html();
					count++;
					$('#subscribe_count_'+news_id).html(count);
				}
				else if(data=='3') {
					$('#subscribe_'+news_id).html('<a onclick="javascript:subscribepost('+news_id+',1)">Subscribe This Post</a>');
					var count = $('#subscribe_count_'+news_id).html();
					count--;
					$('#subscribe_count_'+news_id).html(count);
				}
				else
					$('#subscribe_'+news_id).html('Error! Refresh Page &amp; try.');
			}
		});
	}
	
	function endorsepost(news_id,endorse_type) {
	//	alert(endorse_type);
		$.ajax({
			url: '<?php echo base_url()."posts/endorsepost/"; ?>',
			async: false,
			type: "POST",
			data: {news_id:news_id, endorse_type:endorse_type},
			dataType: "html",
			success: function(data) {
				if(data=='1') {
					$('#endorse_'+news_id).html('<a>You Endorsed This Post.</a>');
					var count = $('#endorse_count_'+news_id).html();
					count++;
					$('#endorse_count_'+news_id).html(count);
				}
				else
					$('#endorse_'+news_id).html('<a>Error! Refresh Page &amp; try.</a>');
			}
		});
	}
	
	function ssubscribepost(news_id,subscribe_status) {
	//	alert(endorse_type);
		$.ajax({
			url: '<?php echo base_url()."posts/subscribepost/"; ?>',
			async: false,
			type: "POST",
			data: {news_id:news_id, subscribe_status:subscribe_status},
			dataType: "html",
			success: function(data) {
				if(data=='1') {
					$('#ssubscribe_'+news_id).html('<a onclick="javascript:ssubscribepost('+news_id+',2)">Unsubscribe This post</a>');
					var count = $('#ssubscribe_count_'+news_id).html();
					count++;
					$('#ssubscribe_count_'+news_id).html(count);
				}
				else if(data=='3') {
					$('#ssubscribe_'+news_id).html('<a onclick="javascript:ssubscribepost('+news_id+',1)">Subscribe This Post</a>');
					var count = $('#ssubscribe_count_'+news_id).html();
					count--;
					$('#ssubscribe_count_'+news_id).html(count);
				}
				else
					$('#ssubscribe_'+news_id).html('Error! Refresh Page &amp; try.');
			}
		});
	}
	
	function sendorsepost(news_id,endorse_type) {
	//	alert(endorse_type);
		$.ajax({
			url: '<?php echo base_url()."posts/endorsepost/"; ?>',
			async: false,
			type: "POST",
			data: {news_id:news_id, endorse_type:endorse_type},
			dataType: "html",
			success: function(data) {
				if(data=='1') {
					$('#sendorse_'+news_id).html('<a>You Endorsed This Post.</a>');
					var count = $('#sendorse_count_'+news_id).html();
					count++;
					$('#sendorse_count_'+news_id).html(count);
				}
				else
					$('#sendorse_'+news_id).html('<a>Error! Refresh Page &amp; try.</a>');
			}
		});
	}
	
	</script>