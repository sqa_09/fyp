        <?php 
		include "templates/header.php";
		?>
        <div class="main-title">
            <p>Login</p>
        </div>
		<?php 
			echo validation_errors(); 
			if(isset($not_valid)) echo $not_valid;
		?>
		<!-- start div #main -->
	    <div id="main">
            <div class="main-content">
                <div class="full-width">
                    <!--<div class="title-box">
                        <h1>Login</h1>
                    </div>-->
                    <div class="contact-content" align="center">
                        <div class="contact-form">
                        	<?php echo form_open(base_url().'citizen/login'); ?>
                                <div class="top-form">
                                	<span class="parent_head">Username: <span class="red">*</span> </span>
                                    <span class="parent_full name">
                                        <input class="field" type="text" required="required" value="<?php echo set_value('user_log'); ?>" 
                                        name="user_log" placeholder="Enter Username" />
                                        <span class="icon"></span>    
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">Password: <span class="red">*</span> </span>
                                    <span class="parent_full web">
                                        <input class="field" type="password" required="required" name="pass_log" placeholder="Enter Password" />
                                        <span class="icon"></span>    
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                    <span class="parent_full">
                                		<button class="btn btn-icon submit" type="submit"><span class="icon"></span>Login</button>  
                                    </span>
                                </div>
                            </form>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div> 
                <div class="clear"></div>
            </div>	
        </div>
	    <!-- end div #main --> 
    </div>
	<!-- end div #wrapper -->
    <?php 
	include "templates/footer.php";
	?>