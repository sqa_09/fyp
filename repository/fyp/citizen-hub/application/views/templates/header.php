<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"  content="initial-scale=1, width=device-width">
<title>Citizen Hub</title>
<link href="<?=base_url()?>html/css/styles.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>html/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?=base_url()?>html/rating/jRating.jquery.css" type="text/css" />
<link rel="stylesheet" href="<?=base_url()?>html/css/jquery-ui-1.10.3.css" type="text/css" />
<!--[if  IE]>
<link rel="stylesheet" type="text/css" href="css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<![endif]-->
<script type="text/javascript" src="<?=base_url()?>html/js/jquery-ui-1.10.3.js" ></script>
<script type="text/javascript" src="<?=base_url()?>html/js/jquery-1.8.3.min.js" ></script>
<script type="text/javascript" src="<?=base_url()?>html/js/html5shiv.js" ></script>
<script type="text/javascript" src="<?=base_url()?>html/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?=base_url()?>html/js/fancydropdown.js" ></script>
<script type="text/javascript" src="<?=base_url()?>html/js/jquery.easing-1.3.js" ></script>
<script type="text/javascript" src="<?=base_url()?>html/js/functions.js" ></script>
<script type="text/javascript" src="<?=base_url()?>html/rating/jRating.jquery.js"></script>

<!-- CAMERA CODE START -->
<script language="JavaScript" src="<?=base_url()?>html/cam/swfobject.js"></script>
<script language="JavaScript" src="<?=base_url()?>html/cam/scriptcam.js"></script>
<script language="JavaScript" type="text/javascript">
	
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=395415563879019";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	
	$(document).ready(function() {
		$("#webcam").scriptcam({
			path: '<?=base_url()?>'+'html/cam/',
			useMicrophone: false,
			onError:onError,
			cornerRadius:5,
			cornerColor:'e3e5e2',
			onWebcamReady:onWebcamReady,
		});
	});
	function base64_tofield() {
		$('#capture_pic').val($.scriptcam.getFrameAsBase64());
	};
	function base64_toimage() {
		$('#image').attr("src","data:image/png;base64,"+$.scriptcam.getFrameAsBase64());
	};
	function base64_tofield_and_image() {
		$('#capture_pic').val($.scriptcam.getFrameAsBase64());
		//alert(b64);
		$('#image').attr("src","data:image/png;base64,"+$.scriptcam.getFrameAsBase64());
		$('#pic_update_button').show();
		$('#image').show();
		$('#profile_pic').val('');
		$('#err_div').hide();
	};
	function changeCamera() {
		$.scriptcam.changeCamera($('#cameraNames').val());
	}
	function onError(errorId,errorMsg) {
		$( "#btn1" ).attr( "disabled", true );
		$( "#btn2" ).attr( "disabled", true );
		alert(errorMsg);
	}			
	function onWebcamReady(cameraNames,camera,microphoneNames,microphone,volume) {
		$.each(cameraNames, function(index, text) {
			$('#cameraNames').append( $('<option></option>').val(index).html(text) )
		}); 
		$('#cameraNames').val(camera);
	}
</script>
<!-- CAMERA CODE END -->


<script type="text/javascript">
    /* <![CDATA[ */
    /*global $:false */
    $(function() {
        "use strict";
        //caching
        //next and prev buttons
        var $cn_next = $('#cn_next');
        var $cn_prev    = $('#cn_prev');
        //wrapper of the left items
        var $cn_list    = $('#cn_list');
        var $pages      = $cn_list.find('.cn_page');
        //how many pages
        var cnt_pages   = $pages.length;
        //the default page is the first one
        var page        = 1;
        //list of news (left items)
        var $items      = $cn_list.find('.cn_item');
        //the current item being viewed (right side)
        var $cn_preview = $('#cn_preview');
        //index of the item being viewed. 
        //the default is the first one
        var current     = 1;
        /*
        for each item we store its index relative to all the document.
        we bind a click event that slides up or down the current item
        and slides up or down the clicked one. 
        Moving up or down will depend if the clicked item is after or
        before the current one
        */
        $items.each(function(i){
            var $item = $(this);
            $item.data('idx',i+1);
            
            $item.bind('click',function(){
                var $this       = $(this);
                $cn_list.find('.selected').removeClass('selected');
                $this.addClass('selected');
                var idx         = $(this).data('idx');
                var $current    = $cn_preview.find('.cn_content:nth-child('+current+')');
                var $next       = $cn_preview.find('.cn_content:nth-child('+idx+')');
                
                if(idx > current){
                    $current.stop().animate({'top':'-357px'},600,'easeOutBack',function(){
                        $(this).css({'top':'357px'});
                    });
                    $next.css({'top':'357px'}).stop().animate({'top':'0px'},600,'easeOutBack');
                }
                else if(idx < current){
                    $current.stop().animate({'top':'357px'},600,'easeOutBack',function(){
                        $(this).css({'top':'357px'});
                    });
                    $next.css({'top':'-357px'}).stop().animate({'top':'0px'},600,'easeOutBack');
                }
                current = idx;
            });
        });
        /*
        shows next page if exists:
        the next page fades in
        also checks if the button should get disabled
        */
        $cn_next.bind('click',function(e){
            var $this = $(this);
            $cn_prev.removeClass('disabled');
            ++page;
            if(page === cnt_pages)
                { $this.addClass('disabled'); }
            if(page > cnt_pages){ 
                page = cnt_pages;
                return;
            }   
            $pages.hide();
            $cn_list.find('.cn_page:nth-child('+page+')').fadeIn();
            e.preventDefault();
        });
        /*
        shows previous page if exists:
        the previous page fades in
        also checks if the button should get disabled
        */
        $cn_prev.bind('click',function(e){
            var $this = $(this);
            $cn_next.removeClass('disabled');
            --page;
            if(page === 1)
                { $this.addClass('disabled'); }
            if(page < 1){ 
                page = 1;
                return;
            }
            $pages.hide();
            $cn_list.find('.cn_page:nth-child('+page+')').fadeIn();
            e.preventDefault();
        });
    });
    /* ]]> */
</script> 
</head>
<body class="home">    
    <!-- start div #wrapper -->
    <div id="wrapper">
        <!-- start header -->
        <header>
            <div class="border-left"></div>
            <div class="logo">
                <a href="<?=base_url()?>"><img src="<?=base_url()?>html/img/logo.png" alt=""></a>
                <span class="border-bottom"></span>
            </div>
            <div class="search">
                <form action="<?=base_url()?>citizen/search/" method="post">
                    <input class="field" type="text" name="search_key" value="Search Citizen Hub" onFocus="if (this.value==this.defaultValue) this.value = ''" 
                    onblur="if (this.value=='') this.value = this.defaultValue"  />
                    <input class="submit" type="submit" value=""  />
                </form>
            </div>
            <?php 
			if(is_logged_in()) {
				echo '
				<nav class="menu">
					<ul>
						<li><span class="border-bottom"></span><a href="'.base_url().'">City Board</a></li>
						<li><span class="border-bottom"></span><a href="'.base_url().'citizen/myprofile/'.is_logged_in().'">My Board</a>
						<ul class="dropdown">
							<li class="last"><span class="border-bottom"></span><a href="'.base_url().'citizen/profile/'.is_logged_in().'/edit">Edit Profile</a></li>
						</ul>
						</li>
						<li><span class="border-bottom"></span><a href="'.base_url().'posts/makepost">Make Post</a></li>
						<li><span class="border-bottom"></span><a>Categories</a>
							<ul class="dropdown">';
								$categories = get_categories();
								foreach ($categories as $category) {
									echo '<li class="last"><span class="border-bottom"></span><a href="'.base_url().'citizen/category/'.$category['news_cat_text'].'">'.$category['news_cat_text'].'</a></li>';
								}
							echo '</ul>
						</li>
						<li><span class="border-bottom"></span><a href="'.base_url().'">Features</a>
							<ul class="dropdown">
								<li class="last"><span class="border-bottom"></span><a href="'.base_url().'citizen/trends">Sentiment Analyzer</a></li>
								<li class="last"><span class="border-bottom"></span><a href="'.base_url().'citizen/rssfeeds">News Aggregator</a></li>
								<li class="last"><span class="border-bottom"></span><a href="'.base_url().'citizen/contactus">Feedback</a></li>
								<li class="last"><span class="border-bottom"></span><a href="'.base_url().'citizen/aboutus">About Us</a></li>
							</ul>
						</li>
						<li><span class="border-bottom"></span><a href="'.base_url().'citizen/logout">Logout</a></li>
					</ul>                                              
				</nav>
				<select class="mobile-menu" onchange="document.location.href=this.options[this.selectedIndex].value;"> 
					<option value="index-2.html">City Board</option>
					<option value="'.base_url().'citizen/myprofile/'.is_logged_in().'">My Board</option>
					<option value="'.base_url().'citizen/profile/'.is_logged_in().'/edit">-Edit Profile</option>
					<option value="'.base_url().'posts/makepost/">Make Post</option>';
					foreach ($categories as $category) {
						echo '<option value="'.base_url().'citizen/category/'.$category['news_cat_text'].'">-'.$category['news_cat_text'].'</option>';
					}
					echo '<option value="features.html">Features</option>
					<option value="'.base_url().'citizen/trends">-Sentiment Analyzer</option>
					<option value="'.base_url().'citizen/rssfeeds">-News Aggregator</option>
					<option value="blog-single-with-rating.html">-News single with rating box</option>
					<option value="'.base_url().'citizen/contactus">-Feedback</option>
					<option value="'.base_url().'citizen/logout">Logout</option>
				</select>
				';
			}
			else  {
				echo '
				<nav class="menu">
					<ul>
						<li><span class="border-bottom"></span><a href="'.base_url().'">My Board</a></li>
						<li><span class="border-bottom"></span><a>Categories</a>
							<ul class="dropdown">';
								$categories = get_categories();
								foreach ($categories as $category) {
									echo '<li class="last"><span class="border-bottom"></span><a href="'.base_url().'citizen/category/'.$category['news_cat_text'].'">'.$category['news_cat_text'].'</a></li>';
								}
							echo '</ul>
						</li>
						<li><span class="border-bottom"></span><a href="'.base_url().'citizen/login">Login</a></li>
						<li><span class="border-bottom"></span><a href="'.base_url().'citizen/signup">Signup</a></li>
						<li><span class="border-bottom"></span><a href="'.base_url().'">Features</a>
							<ul class="dropdown">
								<li class="last"><span class="border-bottom"></span><a href="'.base_url().'citizen/trends">Sentiment Analyzer</a></li>
								<li class="last"><span class="border-bottom"></span><a href="'.base_url().'citizen/rssfeeds">News Aggregator</a></li>
								<li class="last"><span class="border-bottom"></span><a href="'.base_url().'citizen/contactus">Feedback</a></li>
								<li class="last"><span class="border-bottom"></span><a href="'.base_url().'citizen/aboutus">About Us</a></li>
							</ul>
						</li>
						
					</ul>                                              
				</nav>
				<select class="mobile-menu" onchange="document.location.href=this.options[this.selectedIndex].value;"> 
					<option value="">Go To...</option> 
					<option value="'.base_url().'">My Board</option>';
					foreach ($categories as $category) {
						echo '<option value="'.base_url().'citizen/category/'.$category['news_cat_text'].'">-'.$category['news_cat_text'].'</option>';
					}
					echo '<option value="'.base_url().'citizen/login">Login</option>
					<option value="'.base_url().'citizen/signup">Signup</option>
					<option value="'.base_url().'citizen/trends">Sentiment Analyzer</option>
					<option value="'.base_url().'citizen/rssfeeds">News Aggregator</option>
					<option value="'.base_url().'citizen/contactus">Feedback</option>
					<option value="'.base_url().'citizen/aboutus">About US</option>
				</select>
				';
			}
			if(is_logged_in()) {
				if(current_location_updated()=='updated') {	}
				else {?>
                    <script type="text/javascript">
						$(document).ready(function(e) {
							if (navigator.geolocation) {
								var lng = 0;
								var lat = 0;
								navigator.geolocation.getCurrentPosition(
									function(position) {
										lat = position.coords.latitude;
										lng = position.coords.longitude;
										//alert("Latitude: " + lat +", Longitude: " + lng);
										$.ajax({
											url: '<?php echo base_url()."citizen/update_user_location/"; ?>',
											async: false,
											type: "POST",
											data: {lat:lat, lng:lng},
											dataType: "html",
											success: function(data) {
												if(data=='1') {
												}
												else
													alert("An error occurred while updating your current location! Please try again.");
											},
											error: function() {
											}
										});
								});
							}
							else {
								alert("Geolocation is not supported by this browser.");
							}
						});
					</script>
                    <?php
				}
			}
			?>
            <div class="clear"></div>
        </header>
        <!-- end header -->