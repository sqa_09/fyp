    
    <!-- start footer -->
    <footer>
        <div class="footer-top">
            <div class="logo">
                <a href="index-2.html"><img src="<?=base_url()?>html/img/logo.png" alt=""></a>
                <span class="border-bottom"></span>
            </div>
            <div class="widget" style="float:right;">
                <div class="desc">
                    <ul class="info">
                    <li class="address">FAST-NU, A.K Brohi Road, Islamabad Pakistan</li>
                    <li class="phone">(+92) 000-000000</li>
                    <li class="mail"><a href="mailto:contact@citizenhub.com">contact@citizenhub.com</a></li>
                </ul>
               	</div>
            </div>
            <div class="widget" style="float:right;">
                <div class="desc">
                    <p>Envisioning a brighter fuure of Pakistan by promoting Citizen Journalism</p>
                </div>
            </div>
            <!--
            <div class="widget">
                <div class="news">
                    <article class="twoboxes last">
                        <div class="post-thumb">
                            <a href="blog-single.html"><img src="<?base_url()?>html/img/footer-img-3.jpg" alt=""></a>
                            <div class="overlay">
                                <div class="op"></div>
                                <div class="cat">
                                    <div class="icon"></div>
                                </div>
                            </div>
                        </div>
                        <div class="right-desc">
                            <h3><a href="#">Do Critics Think Jason Statham’s New Action Movie Kicks Ass?</a></h3>
                            <ul class="rating calc">
                                <li class="active nr_1"><a href="javascript:void(0)"></a></li>
                                <li class="active nr_2"><a href="javascript:void(0)"></a></li>
                                <li class="active nr_3"><a href="javascript:void(0)"></a></li>
                                <li class="nr_4"><a href="javascript:void(0)"></a></li>
                                <li class="nr_5"><a href="javascript:void(0)"></a></li>
                            </ul>
                            <div class="clear"></div>
                            <p class="date">12 march 2013</p>    
                        </div>
                        <div class="clear"></div>
                    </article>
                </div>
            </div>
            
            <div class="widget last">
                <div class="title">
                    <h3>most popular</h3>
                </div>
                <div class="news">
                    <article class="twoboxes last">
                        <div class="post-thumb">
                            <a href="blog-single.html"><img src="<?base_url()?>html/img/footer-img-6.jpg" alt=""></a>
                            <div class="overlay">
                                <div class="op"></div>
                                <div class="cat">
                                    <div class="icon"></div>
                                </div>
                            </div>
                        </div>
                        <div class="right-desc">
                            <h3><a href="#">Do Critics Think Jason Statham’s New Action Movie Kicks Ass?</a></h3>
                            <ul class="rating calc">
                                <li class="active nr_1"><a href="javascript:void(0)"></a></li>
                                <li class="active nr_2"><a href="javascript:void(0)"></a></li>
                                <li class="active nr_3"><a href="javascript:void(0)"></a></li>
                                <li class="nr_4"><a href="javascript:void(0)"></a></li>
                                <li class="nr_5"><a href="javascript:void(0)"></a></li>
                            </ul>
                            <div class="clear"></div>
                            <p class="date">12 march 2013</p>    
                        </div>
                        <div class="clear"></div>
                    </article>
                </div>
            </div>
            -->
            <div class="clear"></div>
        </div>
        <div class="footer-bottom">
            <div class="copyright">
                <p>Copyright 2013 @ <span>CITIZENHUB</span>. A New Step toward NEWS.</p>
            </div>
            <div class="clear"></div>
        </div>  
    </footer>
    <!-- end footer -->
</body>
</html>