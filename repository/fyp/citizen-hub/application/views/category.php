		<?php include "templates/header.php";?>        
        <!-- start div #main-title -->
        <div class="main-title">
            <p><?=$cat_name?></p>
        </div>
        <!-- end div #main-title -->
        
		<!-- start div #main -->
	    <div id="main">
            <div class="main-content">
            	<div class="left-container">
                	<div class="marked-title">
                        <h3><?=$cat_name?> news</h3>
                    </div>
                    <div class="row-fluid">
                        <!--<article class="small">
                            <div class="post-thumb">
                                <a href=""><img src="<?base_url()?>data/images/news_default.jpg" alt=""></a>
                                <div class="overlay">
                                    <div class="op"></div>
                                </div>
                                <ul class="rating calc">
                                    <li class="active nr_1"><a href="javascript:void(0)"></a></li>
                                    <li class="active nr_2"><a href="javascript:void(0)"></a></li>
                                    <li class="active nr_3"><a href="javascript:void(0)"></a></li>
                                    <li class="nr_4"><a href="javascript:void(0)"></a></li>
                                    <li class="nr_5"><a href="javascript:void(0)"></a></li>
                                </ul>
                            </div>
                            <div class="cat-post-desc">
                                <h3><a href="blog-single.html">The Hangover 3: The Trilogy Finale Teaser Trailer Leaked Online</a></h3>
                                <p class="date">12 march 2013  // 45 Comments</p>
                                <p class="endorse"><a href="">Endorse this</a>  // 45 endorsments</p>
                                <p>Ever since the first ‘The Hangover’ movie (which also had an equally amazing movie trailer), people across the country have been enamored by the magic of black-out drinking... Ever since the first ‘The Hangover’ movie (which also had an equally amazing movie trailer), people across the country have been enamored by the magic of black-out drinking... Ever since the first ‘The Hangover’ movie (which also had an equally amazing movie trailer), people across the country have been enamored by the magic of black-out drinking...</p>
                            </div>
                        </article>-->
                        <?php
							echo $news_results;
						?>
                    </div>
                    <div class="clear"></div>
                    <!--<div class="post-navi">
                        <ul>
                            <li><a href="#">&lt;</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">&gt;</a></li>
                        </ul>
                    </div>-->
                </div>
                <div class="right-container">
                    <div class="sidebar">
                        <div class="widget">
                            <div class="marked-title">
                                <h3>be social</h3>
                            </div>
                            <ul class="social">
                            	<?=social_widget()?>
                            	<?=rss_feed_widget($cat_name)?>
                            </ul>
                            <div class="clear"></div>
                        </div>
                        <!--<div class="widget">
                            <div class="marked-title">
                                <h3>ads</h3>
                            </div>
                            <img class="ads" src="<?base_url()?>html/img/ads.jpg" alt="">
                            <div class="clear"></div>
                        </div>-->
                        <div class="widget">
                            <div class="marked-title">
                                <h3>Top Tags</h3>
                            </div>
                            <ul class="tags">
                                <?php
								$all_tags = get_all_tags();
								foreach($all_tags as $tag) {
									echo '<li><a href="'.base_url().'citizen/search/tag/'.$tag['TagID'].'/'.$tag['TagName'].'">'.$tag['TagName'].'</a></li>';
								}
								?>
                            </ul>
                            <div class="clear"></div>
                        </div>
                        <?=news_widget();?>
                    </div>
                </div>
                <div class="clear"></div>
            </div>	
        </div>
	    <!-- end div #main -->
    
    </div>
	<!-- end div #wrapper -->    
    <?php include "templates/footer.php";?>
	<script type="text/javascript">
	
	function subscribepost(news_id,subscribe_status) {
		$.ajax({
			url: '<?php echo base_url()."posts/subscribepost/"; ?>',
			async: false,
			type: "POST",
			data: {news_id:news_id, subscribe_status:subscribe_status},
			dataType: "html",
			success: function(data) {
				if(data=='1') {
					$('#subscribe_'+news_id).html('<a onclick="javascript:subscribepost('+news_id+',2)">Unsubscribe This post</a>');
					var count = $('#subscribe_count_'+news_id).html();
					count++;
					$('#subscribe_count_'+news_id).html(count);
				}
				else if(data=='3') {
					$('#subscribe_'+news_id).html('<a onclick="javascript:subscribepost('+news_id+',1)">Subscribe This Post</a>');
					var count = $('#subscribe_count_'+news_id).html();
					count--;
					$('#subscribe_count_'+news_id).html(count);
				}
				else
					$('#subscribe_'+news_id).html('Error! Refresh Page &amp; try.');
			}
		});
	}
	
	function endorsepost(news_id,endorse_type) {
	//	alert(endorse_type);
		$.ajax({
			url: '<?php echo base_url()."posts/endorsepost/"; ?>',
			async: false,
			type: "POST",
			data: {news_id:news_id, endorse_type:endorse_type},
			dataType: "html",
			success: function(data) {
				if(data=='1') {
					$('#endorse_'+news_id).html('<a>You Endorsed This Post.</a>');
					var count = $('#endorse_count_'+news_id).html();
					count++;
					$('#endorse_count_'+news_id).html(count);
				}
				else
					$('#endorse_'+news_id).html('<a>Error! Refresh Page &amp; try.</a>');
			}
		});
	}
	</script>