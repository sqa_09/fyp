        <?php 
		include "templates/header.php";
		?>
        <!-- start slider -->
        <?php if(isset($posted)) echo $posted;?>
        <div class="cn_wrapper">
            <div id="cn_preview" class="cn_preview">
               
                <?php
				$sc = 0;
				foreach($latest_news as $lnews)
				{
					$timestamp = strtotime($lnews['upload_time']);
                    if($sc==0)
						echo '<div class="cn_content bg-1" style="top:0px; background: url('.base_url().'data/images/'.$lnews['main_image'].')">';
                    else	
						echo '<div class="cn_content bg-1" style="background: url('.base_url().'data/images/'.$lnews['main_image'].')">';
					echo '<div class="caption">
                            <h3><a href="'.base_url().'citizen/singlepost/latest/'.$lnews['news_category'].'/'.$lnews['news_id'].'">
							'.substr($lnews['news_heading'],0,60).' ... .</a></h3>
                            <p>'.substr($lnews['news_text'],0,150).' ... .</p>
                            <div class="date">
                                <P>'.date("d", $timestamp).'<br /><br /><span>'.date("F", $timestamp).'</span></P>
                            </div>
	                    </div>
                    </div>';
					$sc++;
				}
				?>
            </div>
            <div id="cn_list" class="cn_list">
                <div class="cn_page" style="display:block;">
                    <?php
					$jc = 0;
					foreach($latest_news as $lnews)
					{
						$timestamp = strtotime($lnews['upload_time']);
						if($jc==0)
							echo '<div class="cn_item selected">';
						else
							echo '<div class="cn_item">';
						echo '<div class="left-box">
								<img src="'.base_url().'data/images/'.$lnews['main_image'].'" alt="'.$lnews['news_heading'].'">
							</div>
							<div class="right-box">
								<h4>'.substr($lnews['news_heading'],0,65).'</h4>
								<p>'.$lnews['total_comments'].' Comments // '.date("d", $timestamp).' '.date("F", $timestamp).'</p>
							</div>
							<div class="clear"></div>
						</div>';
						$jc++;
					}
					?>
                </div>
            </div>
        </div>
        <!-- end slider -->
        
		<!-- start div #main -->
	    <div id="main">
            <div class="main-content">
                <div class="left-container">
                	<div class="row-fluid">
                        <div class="span2">
                            <?php
							if(is_logged_in()) {
								$hot_news = $recommended;
								echo '<div class="marked-title">
									<h3>Recommended</h3>
								</div>';
							}
							else {
								echo '<div class="marked-title">
									<h3>Hot news</h3>
								</div>';
							}
							$hi = 1;
							foreach($hot_news as $hotn)
							{
								$timestamp = strtotime($hotn['upload_time']);
								if($hi == count($hot_news))
									echo '<article class="small last">';
								else
									echo '<article class="small">';
                                echo '<div class="post-thumb">
                                    <a href="'.base_url().'citizen/singlepost/hot/'.$hotn['news_category'].'/'.$hotn['news_id'].'">
									<img src="'.base_url().'data/images/'.$hotn['main_image'].'" alt=""></a>
                                </div>
                                <div class="cat-post-desc">
                                    <p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp)
									.' // '.$hotn['total_comments'].' Comments</p>
                                    <h3><a href="'.base_url().'citizen/singlepost/hot/'.$hotn['news_category'].'/'.$hotn['news_id'].'">
									'.$hotn['news_heading'].'
									</a></h3>
                                </div>
	                            </article>';
								$hi++;
							}
							?>
                        </div>
                        <div class="span2">
                            <div class="marked-title">
                                <h3>popular news</h3>
                            </div>
							<?php
                            $pi = 1;
							foreach($popular_news as $hotn)
							{
								if($pi == count($hot_news))
									echo '<article class="small last">';
								else
									echo '<article class="small">';
                                $timestamp = strtotime($hotn['upload_time']);
								//echo '<article class="small">';
                                echo '<div class="post-thumb">
                                    <a href="'.base_url().'citizen/singlepost/popular/'.$hotn['news_category'].'/'.$hotn['news_id'].'">
									<img src="'.base_url().'data/images/'.$hotn['main_image'].'" alt=""></a>
                                </div>
                                <div class="cat-post-desc">
                                    <p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp)
									.' // '.$hotn['total_comments'].' Comments</p>
                                    <h3><a href="'.base_url().'citizen/singlepost/popular/'.$hotn['news_category'].'/'.$hotn['news_id'].'">
									'.$hotn['news_heading'].'</a></h3>
                                </div>
	                            </article>';
								$pi++;
							}
							?>      
                        </div>
                        <?=$all_cat_news?> 
                    </div>
                    <?php
                    /*<div class="row-fluid">
                        <div class="span2">
                            <div class="marked-title">
                                <h3>entertainment</h3>
                            </div>
                            <?php
							$ec = 0;
							foreach($entertainment_news as $bnews)
							{
								$timestamp = strtotime($bnews['upload_time']);
								if($ec==0) {
								echo '
									<article class="border">
										<div class="post-thumb">
										<a href="'.base_url().'citizen/singlepost/entertainment/'.$bnews['news_category'].'/'.$bnews['news_id'].'">
											<img src="'.base_url().'data/images/'.$bnews['main_image'].'" alt=""></a>
										</div>
										<div class="cat-post-desc">
											<h3>
										<a href="'.base_url().'citizen/singlepost/entertainment/'.$bnews['news_category'].'/'.$bnews['news_id'].'">
											'.$bnews['news_heading'].'</a></h3>
											<p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp).'  //
											'.$bnews['total_comments'].' Comments</p>
											<p>'.substr($bnews['news_text'],0,150).'</p>
										</div>
									</article>';
								}
								else {
									echo '
									<article class="twoboxes">
										<div class="post-thumb">
											<a href="'.base_url().'citizen/singlepost/entertainment/'.$bnews['news_category'].'/'.$bnews['news_id'].'">
											<img src="'.base_url().'data/images/'.$bnews['main_image'].'" alt=""></a>
										</div>
										<div class="right-desc">
											<h3>
											<a href="'.base_url().'citizen/singlepost/entertainment/'.$bnews['news_category'].'/'.$bnews['news_id'].'">
											'.$bnews['news_heading'].'</a></h3>
											<div class="clear"></div>
											<p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp).' // 
											'.$bnews['total_comments'].' Comments</p>    
										</div>
										<div class="clear"></div>
									</article>';
								}
								$ec++;
							}
							?>
                        </div>
                        <div class="span2">
                            <div class="marked-title">
                                <h3>business</h3>
                            </div>
                            <?php
							$bc = 0;
							foreach($business_news as $bnews)
							{
								$timestamp = strtotime($bnews['upload_time']);
								if($bc==0) {
								echo '
									<article class="border">
										<div class="post-thumb">
											<a href="'.base_url().'citizen/singlepost/Business/'.$bnews['news_category'].'/'.$bnews['news_id'].'">
											<img src="'.base_url().'data/images/'.$bnews['main_image'].'" alt=""></a>
										</div>
										<div class="cat-post-desc">
											<h3>
											<a href="'.base_url().'citizen/singlepost/Business/'.$bnews['news_category'].'/'.$bnews['news_id'].'">
											'.$bnews['news_heading'].'</a></h3>
											<p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp).'  //
											'.$bnews['total_comments'].' Comments</p>
											<p>'.substr($bnews['news_text'],0,150).'</p>
										</div>
									</article>';
								}
								else {
									echo '
									<article class="twoboxes">
										<div class="post-thumb">
											<a href="'.base_url().'citizen/singlepost/Business/'.$bnews['news_category'].'/'.$bnews['news_id'].'">
											<img src="'.base_url().'data/images/'.$bnews['main_image'].'" alt=""></a>
										</div>
										<div class="right-desc">
											<h3>
											<a href="'.base_url().'citizen/singlepost/Business/'.$bnews['news_category'].'/'.$bnews['news_id'].'">
											'.$bnews['news_heading'].'</a></h3>
											<div class="clear"></div>
											<p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp).' // 
											'.$bnews['total_comments'].' Comments</p>    
										</div>
										<div class="clear"></div>
									</article>';
								}
								$bc++;
							}
							?>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="marked-title">
                            <h3>science</h3>
                        </div>
                    </div>
                    <div class="row-fluid">
						<?php
                        $sc = 0;
                        foreach($science_news as $bnews)
                        {
                            $timestamp = strtotime($bnews['upload_time']);
                            if($sc==0) {
                            echo '
								<div class="span2">
                                <article>
                                    <div class="post-thumb">
                                        <a href="'.base_url().'citizen/singlepost/science/'.$bnews['news_category'].'/'.$bnews['news_id'].'">
                                        <img src="'.base_url().'data/images/'.$bnews['main_image'].'" alt=""></a>
                                    </div>
                                    <div class="cat-post-desc">
                                        <h3>
                                        <a href="'.base_url().'citizen/singlepost/science/'.$bnews['news_category'].'/'.$bnews['news_id'].'">
                                        '.$bnews['news_heading'].'</a></h3>
                                        <p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp).'  //
                                        '.$bnews['total_comments'].' Comments</p>
                                        <p>'.substr($bnews['news_text'],0,150).'</p>
                                    </div>
                                </article>
								</div>';
                            }
                            else {
                                echo '
								<div class="span2">
                                <article class="twoboxes">
                                    <div class="post-thumb">
                                        <a href="'.base_url().'citizen/singlepost/science/'.$bnews['news_category'].'/'.$bnews['news_id'].'">
                                        <img src="'.base_url().'data/images/'.$bnews['main_image'].'" alt=""></a>
                                    </div>
                                    <div class="right-desc">
                                        <h3>
                                        <a href="'.base_url().'citizen/singlepost/science/'.$bnews['news_category'].'/'.$bnews['news_id'].'">
                                        '.$bnews['news_heading'].'</a></h3>
                                        <div class="clear"></div>
                                        <p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp).' // 
                                        '.$bnews['total_comments'].' Comments</p>    
                                    </div>
                                    <div class="clear"></div>
                                </article>
								</div>';
                            }
                            $sc++;
                        }
                        ?>
                    </div>
                    */?>
                    <div class="clear"></div>
                </div>
                <div class="right-container">
                    <div class="sidebar">
                    	<?php if(is_logged_in()) {
							echo '<div class="widget">
									<div class="marked-title">
										<h3>Notifications</h3>
									</div>
									<div style="height:300px; overflow;auto;">
										'.$notification_results.'
									</div>
									<div class="clear"></div>
								</div>';
                        }?>
                        <div class="widget">
                            <div class="marked-title">
                                <h3>be social</h3>
                            </div>
                            <ul class="social">
                            	<?=social_widget()?>
                            	<?=rss_feed_widget('home')?>
                            </ul>
                            <div class="clear"></div>
                        </div>
                        <div class="widget">
                            <div class="marked-title">
                                <h3>Top Tags</h3>
                            </div>
                            <ul class="tags">
                                <?php
								$all_tags = get_all_tags();
								foreach($all_tags as $tag) {
									echo '<li><a href="'.base_url().'citizen/search/tag/'.$tag['TagID'].'/'.$tag['TagName'].'">'.$tag['TagName'].'</a></li>';
								}
								?>
                            </ul>
                            <div class="clear"></div>
                        </div>
                       	<?=news_widget();?>
                    </div>
                </div>
                <div class="clear"></div>
            </div>	
        </div>
	    <!-- end div #main -->
    
    </div>
	<!-- end div #wrapper -->    
    <?php 
	include "templates/footer.php";
	?>