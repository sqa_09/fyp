        <?php 
			include "templates/header.php";
		?>
        <!-- start div #main-title -->
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script type="text/javascript">
			$(function() {
				$( "#full_name" ).autocomplete({
					source: "<?=base_url()?>client/people_auto_complete/",
					minLength: 2
				});
			});
		</script>
        <div class="main-title">
            <p>Contact Citizens</p>
        </div>
        <?php
			echo validation_errors(); 
			if(isset($done_msg)) echo $done_msg;
		?>
	    <div id="main">
            <div class="main-content contact" id="admin">
            	<div class="main-content contact">
                <div class="contact-content">
                    <div class="marked-title">
                        <h3>Drop <span>a messsage to citizen</span></h3>
                    </div>
                    <div class="contact-form">
                        <form action="<?=base_url()?>client/contact_citizens" method="post">
                            <div class="top-form">
                                <span class="parent name">
                                    <input required="required" class="field" type="text" name="full_name" id="full_name" value="<?=set_value('full_name')?>" placeholder="Enter username" />
                                    <span class="icon"></span>    
                                </span>
                                <span class="parent web last">
                                    <input required="required" class="field" type="text" name="Subject" value="<?=set_value('Subject')?>" placeholder="Enter your subject"  />
                                    <span class="icon"></span>    
                                </span>
                                <div class="clear"></div>
                            </div>
                            <div class="bottom-form">
                                <textarea required="required" name="message" rows="10" placeholder="Enter your Message"><?=set_value('message')?></textarea>
                            </div>
                            <button class="btn btn-icon submit" type="submit"><span class="icon"></span>Drop message</button>
                        </form>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            </div>	
        </div>
	    <!-- end div #main -->
    
    </div>
	<!-- end div #wrapper -->
    <?php 
		include "templates/footer.php";
	?>
	<script type="text/javascript">
		
	</script>
