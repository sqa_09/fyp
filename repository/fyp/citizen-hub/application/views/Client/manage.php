        <?php 
			include "templates/header.php";
		?>
        <!-- start div #main-title -->
        <div class="main-title">
            <p><?=$title?></p>
        </div>
        <?php
		if(isset($done_msg)) echo $done_msg;
		?>
	    <div id="main">
            <div class="main-content contact" id="admin">
            	<?=$user_view;?>
            </div>	
        </div>
	    <!-- end div #main -->
    
    </div>
	<!-- end div #wrapper -->
    <?php 
		include "templates/footer.php";
	?>
	<script type="text/javascript">
	function delete_reported_post(news_id,status) {
		var path = '<?=base_url()?>';
        $.ajax({
            url: path + 'client/post_delete/',
            async: false,
            type: "POST",
            data: {news_id:news_id, status:status},
            dataType: "html",
            success: function() {
				$('#post_'+news_id).remove();
            },
            error: function(data){
                alert("Something Went wrong; Refresh your page and try again.");
            }
        });
	}
	</script>
