		<?php include "templates/header.php";?>
        <!-- start div #main-title -->
		<div class="main-title">
            <p>Make a new post</p>
        </div>
        <?php 
			echo validation_errors(); 
			if(isset($post_error_msg)) echo $post_error_msg;
		?>
        <div class="alert alert-error" id="err_div" style="display:none;"></div>
        <!-- end div #main-title -->
        
		<!-- start div #main -->
	    <div id="main">
            <div class="main-content">
                <div class="features">
                    <div class="row-fluid">
                   	<?php echo form_open_multipart(base_url().'client/submitpost/'); ?>
                        <div class="span2">
                        	<div class="accordion" id="accordion">
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            <span class="icon"></span>News Details
                                        </a>
                                    </div>
                                    <div id="collapseOne" class="accordion-body collapse in">
                                        <div class="accordion-inner">
                                            <span style="font-weight:bold;">Category</span>
                                            <?php $categories = get_categories();?>
                                            <select name="news_category" class="slelect_box">
                                            <?php
											foreach ($categories as $category) {
												echo '<option value="'.$category['news_cat_id'].'">'.$category['news_cat_text'].'</option>';
											}
											?>
                                			</select>
                                            <span style="font-weight:bold;">Heading</span>
                            				<input type="text" value="<?php echo set_value('news_heading'); ?>" class="input_field" name="news_heading" required="required" />
                                           <!-- <span style="font-weight:bold;">Search People to tag</span>
                            				<input type="text" class="input_field" name="people_search" id="people_search" />
                                            <div class="quick_result" id="quick_result">
                                            </div>-->
                                            <span style="font-weight:bold;">Details</span>
                            				<textarea type="text" rows="20" class="input_textarea" required="required" name="news_description" id="news_description"><?php echo set_value('news_description'); ?></textarea>
                                        </div><br />
                                        <div style="margin-left:10px;"><?=$recaptcha_html?></div><br />
                                        <div class="right" style="margin-right:10px;">
	                                        <button class="btn btn-icon submit" type="submit"><span class="icon"></span>Post News</button><br /><br />
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="span2">
                            <ul class="nav nav-tabs" id="myTab">
                                <li class="active"><a href="#tab-1" data-toggle="tab"><span class="icon"></span>Photo</a></li>
                                <!--<li><a href="#tab-3" data-toggle="tab"><span class="icon"></span>Link</a></li>-->
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-1">
                                   <span style="font-weight:bold;">Attach Photo with News</span>
                            	   <input type="file" class="input_field" name="news_image" id="profile_pic" />
                                   <div class="top-form">
                                    <div style="width:350px;float:left;">
                                        <div id="webcam"></div>
                                        <div style="margin:5px;">
                                            <img src="webcamlogo.png" style="vertical-align:text-top"/>
                                        </div>
                                    </div>
                                    <div style="width:135px;float:left;">
                                        <p>
                                        <button class="btn" id="btn2" type="button" onclick="base64_tofield_and_image()">Capture Image</button>
                                        </p> 
                                        <div style="width:150px;height:120px; float:left;">
                                        <div style="width:150px; height:120px; margin:0;border:1px solid #666;">
                                        	<p><input id="capture_pic" name="capture_pic" type="hidden" />
                                        	<img id="image" style="display:none;" height="120" width="150" /></p>
                                        </div>
                                    </div>
                                    </div>
                                   
                                </div>
                                </div>
                                <!--<div class="tab-pane" id="tab-3">
                                    <span style="font-weight:bold;">Enter Link of News</span>
                            		<input type="text" class="input_field" name="news_link" />
                                </div>-->
                            </div>
                            <div class="tag_result" id="tag_results">
                            
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="separator"></div>
                    
                </div>
                <div class="clear"></div>
            </div>	
        </div>
	    <!-- end div #main --> 
    
    </div>
	<!-- end div #wrapper -->    
    <?php include "templates/footer.php";?>
	<script type="text/javascript">
	function getChar(event) {
		if (event.which == null) {
			return String.fromCharCode(event.keyCode) // IE
		} else if (event.which!=0 && event.charCode!=0) {
			return String.fromCharCode(event.which)   // the rest
		} else {
			return null // special key
		}
	}
	
	var globalcontrol;
	var currentSearchValue;
	var currentSearchFlag;
	var temp_search_key;
	var searchtype;
	var urlpath;

	document.getElementById('news_description').onkeypress = function(event) {
		var char = getChar(event || window.event);
		if(char == "@" || char == '#') { 
			if(char == "@")
				searchtype = "tag"
			else
				searchtype = "people"
			currentSearchValue = ""; 
			currentSearchFlag = true; 
		} else if (char == " " && currentSearchFlag) { 
			currentSearchFlag = false; 
			//alert(currentSearchValue); 
			currentSearchValue = ""; 
		} 
		if(currentSearchFlag == true) {
			if(event.keyCode==8) {
				char = ''; currentSearchValue = currentSearchValue.slice(0, -1);
			} 
			else { 
				currentSearchValue = currentSearchValue + char;
			}
			if(currentSearchValue.length>1) {
				temp_search_key = currentSearchValue;
				if(searchtype == "tag") {
					temp_search_key = temp_search_key.replace("@", "");
					urlpath = '<?php echo base_url()."client/search_tags/"; ?>';
				}
				else if(searchtype == "people") {
					temp_search_key = temp_search_key.replace("#", "");
					urlpath = '<?php echo base_url()."client/search_people/"; ?>';
				}
				$.ajax({
					url: urlpath,
					async: false,
					type: "POST",
					data: {search_key:temp_search_key},
					dataType: "html",
					success: function(data) {
						$('#tag_results').show();
						
						if(data!='') {
							$('#tag_results').html(data);
						}
						else
							$('#tag_results').html('<div class="result">No Tag Found</div>');
					}
				});
			}
		}
		else { 
			$('#tag_results').hide(); 
		}
	}
	
	function add_tag_to_text(key) {
		var temp_desc = $('#news_description').val();
		var tag = $('#res_'+key).html();
		tag = tag.toUpperCase();
		if (tag.indexOf("@") == -1)
			tag = '@'+tag;
		temp_desc = temp_desc.replace(currentSearchValue, tag);
		$('#news_description').val(temp_desc);
		$("#tag_results").hide();
	}
	
	function add_people_to_text(key) {
		var temp_desc = $('#news_description').val();
		var people = $('#pres_'+key).html();
		people = people.toUpperCase();
		if (people.indexOf("#") == -1)
			people = '#'+people;
		temp_desc = temp_desc.replace(currentSearchValue, people);
		$('#news_description').val(temp_desc);
		$("#tag_results").hide();
	}
	</script>