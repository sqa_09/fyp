        <?php 
			include "templates/header.php";
		?>
        <!-- start div #main-title -->
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script type="text/javascript">
			$(function() {
				$( "#tag_name" ).autocomplete({
					source: "<?=base_url()?>citizen/people_auto_complete/",
					minLength: 2
				});
			});
		</script>
        <script type="application/javascript" src="<?=base_url()?>html/js/highcharts.js"></script>
		<script type='text/javascript'>
        <?php
		if(isset($result)) {
		echo "
		$(function () {
			$('#graph_container').highcharts({
				chart: {
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false
				},
				title: {
					text: 'Overall Trends'
				},
				tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				},
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer',
						dataLabels: {
							enabled: true,
							color: '#000000',
							connectorColor: '#000000',
							format: '<b>{point.name}</b>: {point.percentage:.1f} %'
						},
						showInLegend: true
					}
				},
				series: [{
					type: 'pie',
					name: 'Share',
					data: [
						{
							name: 'Positive',
							y: ".$result['positive'].",
							sliced: true,
							selected: true
						},
						['Negative',".$result['negative']."],
						['Mixed',".$result['mixed']."]
					]
				}]
			});
		});";
		}
		if(isset($ent_result)) {
		echo "
		$(function () {
			$('#graph_container_1').highcharts({
				chart: {
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false
				},
				title: {
					text: 'Entities'
				},
				tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				},
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer',
						dataLabels: {
							enabled: true,
							color: '#000000',
							connectorColor: '#000000',
							format: '<b>{point.name}</b>: {point.percentage:.1f} %'
						},
						showInLegend: true
					}
				},
				series: [{
					type: 'pie',
					name: 'Share',
					data: [
						{
							name: 'Positive',
							y: ".$ent_result['positive'].",
							sliced: true,
							selected: true
						},
						['Negative',".$ent_result['negative']."],
						['Mixed',".$ent_result['mixed']."]
					]
				}]
			});
		});";
		}
        ?>
		</script>
        <div class="main-title">
            <p><?=$title?></p>
        </div>
        <!-- end div #main-title -->
        <?php 
			echo validation_errors(); 
			if(isset($not_valid)) echo $not_valid;
		?>
  		<!-- start div #main -->
	    <div style="text-align:center; width:100%; float:left; padding:10px 0;">
            <div style="float:left; width:34%; text-align:right; padding:20px 20px 0 0;">
            	<span style="font-weight:bold; font-size:18px;">TAG NAME : </span>
            </div>
            <div style="float:left; width:47%; text-align:left;">
            	<form action="<?=base_url()?>citizen/trends/" method="post">
                <div style="float:left; width:350px;">
                    <input required="required" placeholder="Enter Tag Name starting with '@'" type="text" name="tag_name" id="tag_name" class="input_field" style="width:300px;" />
                </div>
                <div style="float:left; width:100px; padding:11px 0 0 0;">
                    <input type="submit" value="Search" class="btn" />
                </div>
                </form>
            </div>
        </div>
        <div id="main">
        	<div class="main-content contact" style="text-align:center; margin-top:100px;">
            	<div id="graph_container" style="height: 500px; min-width: 600px"></div>
            </div>	
            <div class="main-content contact" style="text-align:center; margin-top:50px;">
            	<div id="graph_container_1" style="height: 500px; min-width: 600px"></div>
            </div>	
        </div>
	    <!-- end div #main -->
    
    </div>
	<!-- end div #wrapper -->
    <?php 
		include "templates/footer.php";
	?>