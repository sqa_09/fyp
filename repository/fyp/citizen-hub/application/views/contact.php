        <?php 
			include "templates/header.php";
		?>
        <!-- start div #main-title -->
        <div class="main-title">
            <p>get in touch</p>
        </div>
        <?php 
			echo validation_errors(); 
			if(isset($not_valid)) echo $not_valid;
		?>
        <!-- start div #main -->
	    <div id="main">
            <div class="main-content contact">
            	<div class="contact-iframe">
                    <iframe height="450" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=National+University+Of+Computer+And+Emerging+Sciences,+A.K.+Brohi+Road,+Islamabad,+Islamabad+Capital+Territory,+Pakistan&amp;aq=1&amp;oq=national+university+of+compu&amp;sll=33.663782,73.015137&amp;sspn=0.038219,0.084543&amp;ie=UTF8&amp;hq=national+university+of+computer+and+emerging+sciences+ak+brohi+road+islamabad+islamabad+capital+territory+pakistan&amp;hnear=Pakistan&amp;ll=33.656392,73.016544&amp;spn=0.038216,0.084543&amp;t=m&amp;z=14&amp;iwloc=A&amp;cid=299570142903416891&amp;output=embed&amp;iwloc=near"></iframe>
                </div>
                <div class="infos">
                	<h1>contact@citizenhub.com</h1>
                    <h2>Citizen Hub // (+92) 000-000000</h2>
                </div>
                <div class="contact-content">
                    <div class="marked-title">
                        <h3>Drop us <span>a line</span></h3>
                    </div>
                    <div class="contact-form">
                        <form action="<?=base_url()?>citizen/contactus" method="post">
                            <div class="top-form">
                                <span class="parent name">
                                    <input required="required" class="field" type="text" name="full_name" value="<?=set_value('full_name')?>" placeholder="Enter your name" />
                                    <span class="icon"></span>    
                                </span>
                                <span class="parent email">
                                    <input required="required" class="field" type="text" name="Email" value="<?=set_value('Email')?>" placeholder="Enter your email" />
                                    <span class="icon"></span>    
                                </span>
                                <span class="parent web last">
                                    <input required="required" class="field" type="text" name="Subject" value="<?=set_value('Subject')?>" placeholder="Enter your subject"  />
                                    <span class="icon"></span>    
                                </span>
                                <div class="clear"></div>
                            </div>
                            <div class="bottom-form">
                                <textarea required="required" name="message" rows="10" placeholder="Enter your Message"><?=set_value('message')?></textarea>
                            </div>
                            <button class="btn btn-icon submit" type="submit"><span class="icon"></span>Drop message</button>
                        </form>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>	
        </div>
	    <!-- end div #main -->
    
    </div>
	<!-- end div #wrapper -->
    <?php 
		include "templates/footer.php";
	?>