        <?php 
			include "templates/header.php";
		?>
        <!-- start div #main-title -->
        <div class="main-title">
            <p><?=$title?></p>
        </div>
        <?php
		if(isset($done_msg)) echo $done_msg;
		?>
	    <div id="main">
            <div class="main-content contact" id="admin">
            	<?=$user_view;?>
            </div>	
        </div>
	    <!-- end div #main -->
    
    </div>
	<!-- end div #wrapper -->
    <?php 
		include "templates/footer.php";
	?>
	<script type="text/javascript">
    function activate_user(user_id,status) {
		var path = '<?=base_url()?>';
        $.ajax({
            url: path + 'administrator/user_activation/',
            async: false,
            type: "POST",
            data: {user_id:user_id, status:status},
            dataType: "html",
            success: function() {
                if(status == 1) {
                    $('#user_icon_'+user_id).attr('src',path+'html/img/delete_icon.png');
                    $('#user_icon_'+user_id).attr('onclick','javascript:activate_user('+user_id+',0)');
                }
                else {
                    $('#user_icon_'+user_id).attr('src',path+'html/img/enable_icon.png');
                    $('#user_icon_'+user_id).attr('onclick','javascript:activate_user('+user_id+',1)');
                }
            },
            error: function(data){
                alert("Something Went wrong; Refresh your page and try again.");
            }
        });
	}
	
	function activate_post(news_id,status) {
		var path = '<?=base_url()?>';
        $.ajax({
            url: path + 'administrator/post_activation/',
            async: false,
            type: "POST",
            data: {news_id:news_id, status:status},
            dataType: "html",
            success: function() {
                if(status == 1) {
                    $('#news_icon_'+news_id).attr('src',path+'html/img/delete_icon.png');
                    $('#news_icon_'+news_id).attr('onclick','javascript:activate_post('+news_id+',0)');
                }
                else {
                    $('#news_icon_'+news_id).attr('src',path+'html/img/enable_icon.png');
                    $('#news_icon_'+news_id).attr('onclick','javascript:activate_post('+news_id+',1)');
                }
            },
            error: function(data){
                alert("Something Went wrong; Refresh your page and try again.");
            }
        });
	}
	function delete_reported_post(news_id,status) {
		var path = '<?=base_url()?>';
        $.ajax({
            url: path + 'administrator/post_delete/',
            async: false,
            type: "POST",
            data: {news_id:news_id, status:status},
            dataType: "html",
            success: function() {
				$('#post_'+news_id).remove();
            },
            error: function(data){
                alert("Something Went wrong; Refresh your page and try again.");
            }
        });
	}
	</script>
