        <?php 
			include "templates/header.php";
		?>
        <!-- start div #main-title -->
        <div class="main-title">
            <p>Administrator</p>
        </div>
        <!-- end div #main-title -->
                
		<!-- start div #main -->
	    <div id="main">
            <div class="main-content contact" style="text-align:center; height:auto; overflow:auto; padding-bottom:120px; padding-top:80px;">
            	<?php /*<div class="admin_icon">
                	<a href="">
                        <img src="<?=base_url()?>html/img/icon-categories.png" /><br />
                        Manage Categories
                    </a>
                </div>*/?>
                <div class="admin_icon">
                    <a href="<?=base_url()?>administrator/manage_users">
                        <img src="<?=base_url()?>html/img/icon-users.png" /><br />
                        Manage Users
                   	</a>
                </div>
                <div class="admin_icon">
                    <a href="<?=base_url()?>administrator/manage_posts">
                        <img src="<?=base_url()?>html/img/icon-news.png" /><br />
                        Manage Posts
                    </a>
                </div>
                <div class="admin_icon">
                    <a href="<?=base_url()?>administrator/spam_posts">
                        <img src="<?=base_url()?>html/img/icon-news.png" /><br />
                        Reported Posts
                    </a>
                </div>
                <div class="admin_icon">
                    <a href="<?=base_url()?>administrator/usersfeedback">
                        <img src="<?=base_url()?>html/img/icon-comments.png" /><br />
                        Manage Feedback
                    </a>
                </div>
            </div>	
        </div>
	    <!-- end div #main -->
    
    </div>
	<!-- end div #wrapper -->
    <?php 
		include "templates/footer.php";
	?>