<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"  content="initial-scale=1, width=device-width">
<title>Citizen Hub</title>
<link href="<?=base_url()?>html/css/styles.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>html/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?=base_url()?>html/rating/jRating.jquery.css" type="text/css" />
<link rel="stylesheet" href="<?=base_url()?>html/css/jquery-ui-1.10.3.css" type="text/css" />
<!--[if  IE]>
<link rel="stylesheet" type="text/css" href="css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<![endif]-->
<script type="text/javascript" src="<?=base_url()?>html/js/jquery-ui-1.10.3.js" ></script>
<script type="text/javascript" src="<?=base_url()?>html/js/jquery-1.8.3.min.js" ></script>
<script type="text/javascript" src="<?=base_url()?>html/js/html5shiv.js" ></script>
<script type="text/javascript" src="<?=base_url()?>html/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="<?=base_url()?>html/js/fancydropdown.js" ></script>
<script type="text/javascript" src="<?=base_url()?>html/js/jquery.easing-1.3.js" ></script>
<script type="text/javascript" src="<?=base_url()?>html/js/functions.js" ></script>
<script type="text/javascript" src="<?=base_url()?>html/rating/jRating.jquery.js"></script>


<script type="text/javascript">
    /* <![CDATA[ */
    /*global $:false */
    $(function() {
        "use strict";
        //caching
        //next and prev buttons
        var $cn_next = $('#cn_next');
        var $cn_prev    = $('#cn_prev');
        //wrapper of the left items
        var $cn_list    = $('#cn_list');
        var $pages      = $cn_list.find('.cn_page');
        //how many pages
        var cnt_pages   = $pages.length;
        //the default page is the first one
        var page        = 1;
        //list of news (left items)
        var $items      = $cn_list.find('.cn_item');
        //the current item being viewed (right side)
        var $cn_preview = $('#cn_preview');
        //index of the item being viewed. 
        //the default is the first one
        var current     = 1;
        /*
        for each item we store its index relative to all the document.
        we bind a click event that slides up or down the current item
        and slides up or down the clicked one. 
        Moving up or down will depend if the clicked item is after or
        before the current one
        */
        $items.each(function(i){
            var $item = $(this);
            $item.data('idx',i+1);
            
            $item.bind('click',function(){
                var $this       = $(this);
                $cn_list.find('.selected').removeClass('selected');
                $this.addClass('selected');
                var idx         = $(this).data('idx');
                var $current    = $cn_preview.find('.cn_content:nth-child('+current+')');
                var $next       = $cn_preview.find('.cn_content:nth-child('+idx+')');
                
                if(idx > current){
                    $current.stop().animate({'top':'-357px'},600,'easeOutBack',function(){
                        $(this).css({'top':'357px'});
                    });
                    $next.css({'top':'357px'}).stop().animate({'top':'0px'},600,'easeOutBack');
                }
                else if(idx < current){
                    $current.stop().animate({'top':'357px'},600,'easeOutBack',function(){
                        $(this).css({'top':'357px'});
                    });
                    $next.css({'top':'-357px'}).stop().animate({'top':'0px'},600,'easeOutBack');
                }
                current = idx;
            });
        });
        /*
        shows next page if exists:
        the next page fades in
        also checks if the button should get disabled
        */
        $cn_next.bind('click',function(e){
            var $this = $(this);
            $cn_prev.removeClass('disabled');
            ++page;
            if(page === cnt_pages)
                { $this.addClass('disabled'); }
            if(page > cnt_pages){ 
                page = cnt_pages;
                return;
            }   
            $pages.hide();
            $cn_list.find('.cn_page:nth-child('+page+')').fadeIn();
            e.preventDefault();
        });
        /*
        shows previous page if exists:
        the previous page fades in
        also checks if the button should get disabled
        */
        $cn_prev.bind('click',function(e){
            var $this = $(this);
            $cn_next.removeClass('disabled');
            --page;
            if(page === 1)
                { $this.addClass('disabled'); }
            if(page < 1){ 
                page = 1;
                return;
            }
            $pages.hide();
            $cn_list.find('.cn_page:nth-child('+page+')').fadeIn();
            e.preventDefault();
        });
    });
    /* ]]> */
</script> 
</head>
<body class="home">    
    <!-- start div #wrapper -->
    <div id="wrapper">
        <!-- start header -->
        <header>
            <div class="border-left"></div>
            <div class="logo">
                <a href="<?=base_url()?>administrator"><img src="<?=base_url()?>html/img/logo.png" alt=""></a>
                <span class="border-bottom"></span>
            </div>
            <?php 
			if(is_admin_in()) {
				echo '
				<nav class="menu">
					<ul>
						<li><span class="border-bottom"></span><a href="'.base_url().'administrator">Home</a></li>';
						/*echo '<li><span class="border-bottom"></span><a>Categories</a>
							<ul class="dropdown">';
								$categories = get_categories();
								foreach ($categories as $category) {
									echo '<li class="last"><span class="border-bottom"></span><a href="'.base_url().'administrator/category/'.$category['news_cat_text'].'">'.$category['news_cat_text'].'</a></li>';
								}
							echo '</ul>
						</li>';*/
						echo '<li><span class="border-bottom"></span><a href="'.base_url().'administrator/logout">Logout</a></li>
					</ul>                                              
				</nav>
				<select class="mobile-menu" onchange="document.location.href=this.options[this.selectedIndex].value;"> 
					<option value="index-2.html">Home</option>';
					foreach ($categories as $category) {
						echo '<option value="'.base_url().'administrator/category/'.$category['news_cat_text'].'">-'.$category['news_cat_text'].'</option>';
					}
					echo '<option value="'.base_url().'administrator/logout">Logout</option>
				</select>
				';
			}
			else  {
				echo '
				<nav class="menu">
					<ul>
						<li><span class="border-bottom"></span><a href="'.base_url().'administrator">Home</a></li>
					</ul>                                              
				</nav>
				<select class="mobile-menu" onchange="document.location.href=this.options[this.selectedIndex].value;"> 
					<option value="">Go To...</option> 
					<option value="'.base_url().'">Home</option>
				</select>';
			}
			?>
            <div class="clear"></div>
        </header>
        <!-- end header -->
