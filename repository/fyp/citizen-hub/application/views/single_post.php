		<?php include "templates/header.php";?>        
        <!-- start div #main-title -->
        <div class="main-title">
            <p><?=$cat_name?> News</p>
        </div>
        <?php 
			echo validation_errors(); 
			if(isset($comment_post_valid)) echo $comment_post_valid;
		?>
        <!-- end div #main-title -->
        
		<!-- start div #main -->
	    <div id="main">
            <div class="main-content">
            	<div class="left-container">
                	<article>
                    	<div class="title-box">
                            <h3><?=$post->news_heading?></h3>
                        </div>
                        <div class="post-thumb">
                        	<img src="<?=base_url()?>data/images/<?=$post->main_image?>" alt="<?=$post->news_heading?>">
                        </div>
                        <div class="post-info">
                            <ul>
                            	<?php $u_info = get_userinfo($post->updated_by)?>
                                <li>category<br><span><?=$cat_name?></span></li>
                                <li><strong div id="no_of_comments_1"><?=$post->total_comments?></strong><br><span>Comments</span></li>
                                <?php $timestamp = strtotime($post->upload_time);?>
                                <li><?=date("d", $timestamp)?><br><span><?=date("F", $timestamp)?></span></li>
                                <?php //$user_name = get_username($post->updated_by); ?>
                                <li>author<br><span><a href="<?=base_url()?>citizen/viewprofile/<?=$u_info->UserName?>" style="color:#666;">
								<?=$u_info->UserName?></a></span></li>
                                <li style="padding:5px;"><a href="<?=base_url()?>citizen/viewprofile/<?=$u_info->UserName?>">
                                <img src="<?=base_url()?>data/users/<?=$u_info->UserImage?>" width="70" height="69" /></a></li>
                            </ul>
                            <div class="clear"></div>    
                        </div>
                        <div class="post-content">
                            <div class="rating-box">
                                <div class="item">
                                    <p>Rate This Post</p>
                                    <script type="text/javascript">
										$(document).ready(function(){
											$('.awesome').jRating({
												bigStarsPath : '<?=base_url()?>html/rating/icons/stars.png',
												smallStarsPath: '<?=base_url()?>html/rating/icons/small.png',
												phpPath: '<?=base_url()?>posts/ratepost/',
												<?php
												if(is_logged_in() && $rating_check)
													echo 'isDisabled:false';
												else
													echo 'isDisabled:true';
												?>
											});
										});
									</script>
                                    <ul class="rating calc">
                                        <div class="awesome" data-average="<?=$post->overall_rating?>" data-id="<?=$post->news_id?>"></div>
                                    </ul>
                                </div>
                                <div class="total">
                                    <p class="left">total score</p>
                                    <p class="right"><?=$post->overall_rating?></p>
                                    <div class="clear"></div>
                                </div>
                                <div class="total">
                                    <?php
									//Endorsements
									echo '<p class="endorse">';
									$news_id = $post->news_id; 
                                    if(is_logged_in()) {
                                        $user_id = $this->session->userdata('userid');
                                        echo '<span id="endorse_'.$news_id.'">';
                                        $endorsement_check = $this->citizenmodel->checkendorsement($user_id,$news_id,1);
                                        if(count($endorsement_check)==1)
                                            { echo '<a>You Endorsed This post.</a>'; }
                                        else
                                            { echo '<a onclick="javascript:endorsepost('.$news_id.',1)">Endorse this</a>'; }
                                        echo '</span>  // ';
                                    }
                                    $rating = $this->citizenmodel->get_rating($news_id,3);
                                    echo '<span id="endorse_count_'.$news_id.'">'.
                                    count($this->citizenmodel->get_comments_sorted_array('news_id',$news_id,1,'post_time','asc')) . 
                                    '</span> endorsments</p>';
									
									//Subscriptions
									
									echo '<p class="endorse">';
									$news_id = $post->news_id; 
                                    if(is_logged_in()) {
                                        $user_id = $this->session->userdata('userid');
                                        echo '<span id="subscribe_'.$news_id.'">';
                                        $endorsement_check = $this->citizenmodel->checksubsucription($user_id,$news_id);
                                        if(count($endorsement_check)>0)
                                            { echo '<a onclick="javascript:subscribepost('.$news_id.',2)">Unsubscribe This post</a>'; }
                                        else
                                            { echo '<a onclick="javascript:subscribepost('.$news_id.',1)">Subscribe this Post</a>'; }
                                        echo '</span>  // ';
                                    }
                                    $rating = $this->citizenmodel->get_rating($news_id,3);
                                    echo '<span id="subscribe_count_'.$news_id.'">'.
                                    count($this->citizenmodel->get_specific_data_array('user_subscription','news_id',$news_id)) . 
                                    '</span> Subscriptions</p>';
									if(is_logged_in()) {
										echo '<p class="endorse">';
										echo '<span id="report_'.$news_id.'">';
										$spam_check = $this->citizenmodel->check_spam_reports($user_id,$news_id);
                                        if(!$spam_check)
											echo '<a onclick="javascript:report_abuse('.$news_id.',1)">Report Abuse</a>';
										else
											echo '<a>Reported Abuse</a> ';
										echo '</span>';?>
										 / <a href="http://www.google.com" 
                                          onclick="
                                            window.open(
                                              'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 
                                              'facebook-share-dialog', 
                                              'width=626,height=436'); 
                                            return false;">
                                            <img src="<?=base_url()?>html/img/fb-share.jpg" /> 
                                            </a>
                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?=base_url()?>citizen/singlepost/<?=$cat_name?>/<?=$post->news_category?>/<?=$post->news_id?>" data-count="none">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script> 
                                        <?php
                                        echo '</p>';
									}
									else {
										echo '<p class="endorse">';
										?>
										<a href="#" 
                                          onclick="
                                            window.open(
                                              'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 
                                              'facebook-share-dialog', 
                                              'width=626,height=436'); 
                                            return false;">
                                            <img src="<?=base_url()?>html/img/fb-share.jpg" /> 
                                            </a>
                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?=base_url()?>citizen/singlepost/<?=$cat_name?>/<?=$post->news_category?>/<?=$post->news_id?>" data-count="none">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                                        <?php
                                        echo '</p>';
									}
									?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <p style="min-height:200px;"><?=$post->news_text?></p>
                            <div class="marked-title first">
                                <h3>you may also want to read</h3>
                            </div>
                            <?php
							if(count($related_news)>0) {
								echo '<div class="row-fluid">';
								foreach($related_news as $rnews) {
									$timestamp = strtotime($rnews['upload_time']);
									echo '
										<div class="span4">
											<article class="small single">
											<div class="post-thumb">
												<a href="'.base_url().'citizen/singlepost/recommended/'.$rnews['news_category'].'/'.$rnews['news_id'].'">
												<img src="'.base_url().'data/images/'.$rnews['main_image'].'" alt=""></a>
											</div>
												<div class="cat-post-desc">
													<p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp).'</p>
													<h3>
			<a href="'.base_url().'citizen/singlepost/recommended/'.$rnews['news_category'].'/'.$rnews['news_id'].'">'.$rnews['news_heading'].'</a>
													</h3>
												</div>
											</article>    
										</div>
										';
								}
								echo '</div>';
							}
							?>
                            <div class="comments">
                            	<div class="marked-title">
                                    <h3><span id="no_of_comment_2"><?=$post->total_comments?></span> comments</h3>
                                </div>	
                                <ul class="level-1" id="comments_div">
                                	<?php
									foreach($comments as $comment)
									{
										$post_time= strtotime($comment['post_time']);
										$comment_by = $comment['endrosed_by'];
										echo ' 
										<li>
											<span class="comment">
												<span class="date">
													<span>('.date("H", $post_time).':'.date("i", $post_time).')<br />'.date("d", $post_time).' '.date("M", $post_time).' '.date("Y", $post_time).'</span>
												</span>
												<span class="content"><span class="title"><b>'.get_username($comment_by).'</b>, wrote:</span><br>'.$comment['comments'].'</span>
												<span class="clear"></span>
											</span>
										</li>';
									}
									?>
                                </ul>
                            </div>
                            <?php
							if(is_logged_in()) {
								echo '
                            <div class="contact-content">
                                <div class="marked-title">
                                    <h3>Leave a comment</h3>
                                </div>
                                <div class="contact-form">
                                	<div id="comment_response"></div>
                                    	<input type="hidden" name="no_of_comments" id="no_of_comments" value="'.$post->total_comments.'" />  
											<div class="bottom-form">
						<textarea rows="10" id="postcomment" name="postcomment" placeholder="Enter your Comment" required="required"></textarea>
											</div>
										<button class="btn btn-icon submit" onclick="javascript:submitcomment('.$post->news_id.',2)" type="button">
											<span class="icon"></span>Post Comment</button>
                                    <div class="clear"></div>
                                </div>
                            </div>';
							}?>
                        </div>
                    </article>
                    <div class="clear"></div>
                </div>
                <div class="right-container">
                	<div class="sidebar">
                        <div class="widget">
                            <div class="marked-title">
                                <h3>be social</h3>
                            </div>
                            <ul class="social">
                            	<?=social_widget()?>
                            	<?=rss_feed_widget($cat_name)?>
                            </ul>
                            <div class="clear"></div>
                        </div>
                        <div class="widget">
                            <div class="marked-title">
                                <h3>Tags in this post</h3>
                            </div>
                            <ul class="tags">
                            	<?=get_tags_from_ids($post->news_tag)?>
                            </ul>
                            <div class="clear"></div>
                        </div>
                        <?=news_widget();?>
                    </div>
                </div>
                <div class="clear"></div>
            </div>	
        </div>
	    <!-- end div #main -->
    
    </div>
	<!-- end div #wrapper -->
    <?php include "templates/footer.php";?>
	<script type="text/javascript">
	function subscribepost(news_id,subscribe_status) {
		$.ajax({
			url: '<?php echo base_url()."posts/subscribepost/"; ?>',
			async: false,
			type: "POST",
			data: {news_id:news_id, subscribe_status:subscribe_status},
			dataType: "html",
			success: function(data) {
				if(data=='1') {
					$('#subscribe_'+news_id).html('<a onclick="javascript:subscribepost('+news_id+',2)">Unsubscribe This post</a>');
					var count = $('#subscribe_count_'+news_id).html();
					count++;
					$('#subscribe_count_'+news_id).html(count);
				}
				else if(data=='3') {
					$('#subscribe_'+news_id).html('<a onclick="javascript:subscribepost('+news_id+',1)">Subscribe This Post</a>');
					var count = $('#subscribe_count_'+news_id).html();
					count--;
					$('#subscribe_count_'+news_id).html(count);
				}
				else
					$('#subscribe_'+news_id).html('Error! Refresh Page &amp; try.');
			}
		});
	}
	
	function report_abuse(news_id) {
		var r=confirm("You are reporting this news as spam.");
		if (r==true) {
			$.ajax({
				url: '<?php echo base_url()."posts/report_abuse/"; ?>',
				async: false,
				type: "POST",
				data: {news_id:news_id},
				dataType: "html",
				success: function(data) {
					if(data=='1') {
						$('#report_'+news_id).html('<a>Reported Abuse</a>');
					}
					else
						$('#report_'+news_id).html('<a>Error! Try again</a>');
				}
			});
		}
	}
	
	function endorsepost(news_id,endorse_type) {
	//	alert(endorse_type);
		$.ajax({
			url: '<?php echo base_url()."posts/endorsepost/"; ?>',
			async: false,
			type: "POST",
			data: {news_id:news_id, endorse_type:endorse_type},
			dataType: "html",
			success: function(data) {
				if(data=='1') {
					$('#endorse_'+news_id).html('<a>You Endorsed This Post.</a>');
					var count = $('#endorse_count_'+news_id).html();
					count++;
					$('#endorse_count_'+news_id).html(count);
				}
				else
					$('#endorse_'+news_id).html('<a>Error! Refresh Page &amp; try.</a>');
			}
		});
	}
	
	function submitcomment(news_id,endorse_type) {
		var postcomment = $('#postcomment').val();
		var no_of_comments = $('#no_of_comments').val();
		if(postcomment!="") {
			$.ajax({
				url: '<?php echo base_url()."posts/endorsepost/"; ?>',
				async: false,
				type: "POST",
				data: {news_id:news_id, endorse_type:endorse_type, postcomment:postcomment, no_of_comments:no_of_comments},
				dataType: "html",
				success: function(data) {
					if(data!='0') {
						$('#comments_div').append(data);
						var count = $('#no_of_comments').val();
						count++;
						$('#no_of_comments').val(count);
						$('#no_of_comment_2').html(count);
						$('#no_of_comments_1').html(count);
						$('#comment_response').html('<div class="alert alert-success">Your comment is Successfully Posted<button class="close" data-dismiss="alert" type="button">×</button></div>');
						$('#postcomment').val('');
					}
					else
						$('#comment_response').html('<div class="alert alert-error">Error! while posting your comment.<button class="close" data-dismiss="alert" type="button">×</button></div>');
				}
			});
		}
		else {
			$('#comment_response').html('<div class="alert alert-error">Enter some text in text field.<button class="close" data-dismiss="alert" type="button">×</button></div>');
		}
	}
	</script>