        <?php
		/* A view for the signup page*/ 
		include "templates/header.php";
		?>
        <div class="main-title">
            <p>Sign UP</p>
        </div>
        <?php 
			echo validation_errors(); 
			if(isset($not_valid)) echo $not_valid;
		?>
		<!-- start div #main -->
	    <div id="main">
            <div class="main-content">
                <div class="full-width">
                    <!--<div class="title-box">
                        <h1>Sign up</h1>
                      </div>-->
                    <div class="contact-content" align="center">
                        <div class="contact-form">
                        	<?php echo form_open(base_url().'citizen/signup'); ?>
                            	<div class="top-form">
                                	<span class="parent_head"><br />Username: <span class="red">*</span> </span>
                                	<span class="parent_full name">
                                        <span style="float:right;" id="availability"></span><br />
                                        <input class="field" type="text" required="required" value="<?php echo set_value('user_name'); ?>" 
                                        name="user_name" id="user_name" placeholder="Enter Username" /> 
                                    	
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">Email: <span class="red">*</span> </span>
                                	<span class="parent_full name">
                                        <span style="float:right;" id="availability"></span>
                                        <input class="field" type="text" required="required" value="<?php echo set_value('user_email'); ?>" 
                                        name="user_email" placeholder="Enter Email" /> 
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">First Name: <span class="red">*</span> </span>
                                	<span class="parent_full name">
                                        <input class="field" type="text" required="required" value="<?php echo set_value('user_fname'); ?>" 
                                        name="user_fname" placeholder="Enter First Name" />   
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">Last Name: <span class="red">*</span> </span>
                                	<span class="parent_full name">
                                        <input class="field" type="text" required="required" value="<?php echo set_value('user_lname'); ?>" 
                                        name="user_lname" placeholder="Enter Last Name" />   
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">Gender: <span class="red">*</span> </span>
                                	<span class="parent_full name" style="text-align:left;">
                                        Male <input type="radio" checked="checked" value="male" name="user_gender" /> 
                                        Female <input type="radio" value="female" name="user_gender" />
                                    </span><br />
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">City: <span class="red">*</span> </span>
                                	<span class="parent_full name">
                                        <input class="field" type="text" required="required" value="<?php echo set_value('user_city'); ?>" 
                                        name="user_city" placeholder="Enter City" />   
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">Country: <span class="red">*</span> </span>
                                	<span class="parent_full name">
                                        <input class="field" type="text" required="required" value="<?php echo set_value('user_country'); ?>" 
                                        name="user_country" placeholder="Enter Country" />   
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">Password: <span class="red">*</span> </span>
                                	<span class="parent_full name">
                                        <input class="field" type="password" required="required" name="user_password" placeholder="Enter Password" />   
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">Confirm Password: <span class="red">*</span> </span>
                                	<span class="parent_full name">
                                        <input class="field" type="password" required="required" name="user_con_password" placeholder="Confirm Password" />   
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">Captcha: <span class="red">*</span> </span>
                                	<span class="parent_full name">
                                        <?=$recaptcha_html;?>
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">&nbsp;</span>
                                	<span class="parent_full name">&nbsp;</span>
                                </div>
                                <div class="top-form">
                                    <span class="parent_full" style="float:right;">
                                		<button class="btn btn-icon submit" type="submit"><span class="icon"></span>Sign UP</button>  
                                    </span>
                                </div>
                            </form>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div> 
                <div class="clear"></div>
            </div>	
        </div>
	    <!-- end div #main --> 
    </div>
	<!-- end div #wrapper -->
    <?php 
	include "templates/footer.php";
	?>
	<script type="text/javascript">
	$(document).ready(function() {
		$("#user_name").keypress(function(e) {
			if(($('#user_name').val().length)>3 && !($.isNumeric($('#user_name').val()))) {
				var username = $('#user_name').val();
				$.ajax({
				url: '<?php echo base_url()."citizen/checkusername/"; ?>',
				async: false,
				type: "POST",
				data: {username:username},
				dataType: "html",
				success: function(data) {
						if(data==true)
							$("#availability").html("<span class='green'>Available</span>");
						else
							$("#availability").html("<span class='red'>Not Available</span>");
					}
				});
			}
			else
				$("#availability").html("<span class='red'>Not Available</span>");
		});
	});
	</script>