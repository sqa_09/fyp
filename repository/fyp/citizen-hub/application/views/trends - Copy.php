        <?php 
			include "templates/header.php";
		?>
        <!-- start div #main-title -->
        <script type="application/javascript" src="<?=base_url()?>html/js/highstock.js"></script>
        <script type="text/javascript">
		$(function () {
			$('#graph_container').highcharts('PieChart',{
				chart: {
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false
				},
				title: {
					text: ''
				},
				tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage:.1f} </b><br />'
				},
				legend: {
					layout: 'vertical',
					align: 'right',
					verticalAlign: 'middle',
					borderWidth: 0
				},
				series: [{
					name: 'Islamabad',
					data: [[1160697600000,75.02],
							[1160956800000,75.40],
							[1161043200000,74.29],
							[1161129600000,74.53],
							[1161216000000,78.99],
							[1161302400000,79.95],
							[1161561600000,81.46],
							[1161648000000,81.05],
							[1161734400000,81.68],
							[1161820800000,82.19],
							[1161907200000,80.41],
							[1162166400000,80.42],
							[1162252800000,81.08]]
				}, {
					name: 'Lahore',
					data: [[1162339200000,79.16],
							[1162425600000,78.98],
							[1162512000000,78.29],
							[1162771200000,79.71],
							[1162857600000,80.51],
							[1162944000000,82.45],
							[1163030400000,83.34],
							[1161561600000,81.46],
							[1161648000000,81.05],
							[1161734400000,81.68],
							[1161820800000,82.19],
							[1161907200000,80.41],
							[1162166400000,80.42],
							[1162252800000,81.08]]
				}, {
					name: 'Karachi',
					data: [[1165795200000,88.75],
							[1165881600000,86.14],
							[1165968000000,89.05],
							[1166054400000,88.55],
							[1166140800000,87.72],
							[1166400000000,85.47],
							[1166486400000,86.31],
							[1166572800000,84.76],
							[1166659200000,82.90],
							[1166745600000,82.20],
							[1167091200000,81.51],
							[1167177600000,81.52],
							[1167264000000,80.87],
							[1167350400000,84.84]]
				}]
			});
		});
		</script>
        <div class="main-title">
            <p>Trends</p>
        </div>
        <!-- end div #main-title -->
                
		<!-- start div #main -->
	    <div id="main">
            <div class="main-content contact" style="text-align:center;">
          		<div id="graph_container" style="height: 500px; min-width: 600px"></div>
            </div>	
        </div>
	    <!-- end div #main -->
    
    </div>
	<!-- end div #wrapper -->
    <?php 
		include "templates/footer.php";
	?>