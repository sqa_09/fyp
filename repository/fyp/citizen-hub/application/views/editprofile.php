        <?php
		/* A view for the signup page*/ 
		include "templates/header.php";
		?>
        <div class="main-title">
            <p>Profile</p>
        </div>
        <?php 
			echo validation_errors(); 
			if(isset($profile_update_msg)) echo $profile_update_msg;
		?>
        <div class="alert alert-error" id="err_div" style="display:none;">
        	<button class="close" data-dismiss="alert" type="button">×</button>
        </div>
        <!-- start div #main -->
	    <div id="main">
            <div class="main-content">
                <div class="full-width">
                    <!--<div class="title-box">
                        <h1>Sign up</h1>
                      </div>-->
                    <div class="contact-content" align="center" style="">
                        <div class="contact-form">
                        	<div class="sidebar">
                        	<?php echo form_open_multipart(base_url().'citizen/profile/'.is_logged_in().'/update'); ?>
                            	<div class="right-form">
                                	<div class="widget">
                                    <div class="marked-title">
                                        <h3>Personal Information</h3>
                                    </div>
                                <div class="top-form">
                                	<span class="parent_head">Profile Image: </span>
                                	<span class="parent_full" style="margin-left:7%">
                                        <input class="field" type="file" name="profile_pic" id="profile_pic" />
                                    </span>
                                </div>
                                <div class="top-form">
                                    <!--<div style="width:350px;float:left;">
                                        <div id="webcam"></div>
                                        <div style="margin:5px;">
                                            <img src="webcamlogo.png" style="vertical-align:text-top"/>
                                        </div>
                                    </div>
                                    <div style="width:135px;float:left;">
                                        <p><button class="btn" id="btn2" type="button" onclick="base64_tofield_and_image()">Capture Image</button></p>
                                    </div>-->
                                    <div style="width:200px;height:250px; float:left; margin-left:20px;">
                                        <div style="width:200px; height:150px; margin:0;border:1px solid #666;">
                                        	<p><input id="capture_pic" name="capture_pic" type="hidden" />
                                        	<img id="image" style="display:none;" height="150" width="200" /></p>
                                            <p><button style="display:none;" id="pic_update_button" class="btn btn-icon submit" type="submit">
                                            <span class="icon"></span>Update</button></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="top-form">
                                    <span class="parent_head"></span>
                                    <span class="parent_full">
                                    	<table width="100%" style="float:right;">
                                        <tr>
                                        <td style="width:50%;">
                                    	<img id="display_pic" style="border:1px solid #666;" src="<?=base_url()?>data/users/<?=$userdetail->UserImage?>" height="80" /><br /><br />
                                        </td><td>&nbsp;
                                        </td></tr></table>
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">Username: <span class="red">*</span> </span>
                                	<span class="parent_full">
                                        <input class="field" disabled="disabled" type="text" required="required" value="<?=is_logged_in()?>" 
                                        name="user_name" id="user_name" placeholder="Enter Username" />
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">Email: <span class="red">*</span> </span>
                                	<span class="parent_full name">
                                        <span style="float:right;" id="availability"></span>
                                        <input class="field" type="text" required="required" value="<?=$userdetail->UserEmail?>" 
                                        name="user_email" placeholder="Enter Email" /> 
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">First Name: <span class="red">*</span> </span>
                                	<span class="parent_full name">
                                        <input class="field" type="text" required="required" value="<?=$userdetail->UserFName?>" 
                                        name="user_fname" placeholder="Enter First Name" />   
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">Last Name: <span class="red">*</span> </span>
                                	<span class="parent_full name">
                                        <input class="field" type="text" required="required" value="<?=$userdetail->UserLName?>" 
                                        name="user_lname" placeholder="Enter Last Name" />   
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">Gender: <span class="red">*</span> <br /><br /></span>
                                	<span class="parent_full name" style="text-align:left;">
                                        Male <input <?php if($userdetail->UserGender=='male') echo 'checked="checked"'?> 
                                        type="radio" value="male" name="user_gender" /> 
                                        Female <input <?php if($userdetail->UserGender=='female') echo 'checked="checked"'?> 
                                        type="radio" value="female" name="user_gender" />
                                    </span><br />
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">Address: </span>
                                	<span class="parent_full name">
                                        <textarea rows="3" name="user_address" placeholder="Enter Street Address" ><?=$userdetail->UserAddress?></textarea>
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">City: <span class="red">*</span> </span>
                                	<span class="parent_full name">
                                        <input class="field" type="text" required="required" value="<?=$userdetail->UserCity?>" 
                                        name="user_city" placeholder="Enter City" />   
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">Country: <span class="red">*</span> </span>
                                	<span class="parent_full name">
                                        <input class="field" type="text" required="required" value="<?=$userdetail->UserCountry?>" 
                                        name="user_country" placeholder="Enter Country" />   
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                </div>
                                <div class="widget"><br />
                                    <div class="marked-title">
                                        <h3>Update Tags</h3>
                                    </div>
                                    <div class="top-form">
                                	<span class="parent_full" style="width:95%; text-align:left;">Highlighted tags are added in your profile. Click on any tag to add or remove.</span>
                                	<div class="clear"></div>
                                </div><br />
                                    <ul class="tags">
                                    <input type="hidden" value="<?=$userdetail->UserTags?>" name="user_tags" id="user_tags" />
                                    	<?php
										$user_tags = $userdetail->UserTags;
										foreach($all_tags as $tag) {
											$pos = strpos($user_tags, $tag['TagID']);
											if($pos!==false)
												echo '<li><a id="tag_'.$tag['TagID'].'" onclick="javascript:add_remove_tag('.$tag['TagID'].')" class="selected">';
											else
												echo '<li><a id="tag_'.$tag['TagID'].'" onclick="javascript:add_remove_tag('.$tag['TagID'].')">';
											echo $tag['TagName'].'</a></li>';
										}
										?>
                                    </ul>
                                    <div class="clear"></div>
                                </div>
                                <div class="widget">
                                <div class="marked-title">
                                    <h3>Change Password</h3>
                                </div>
                                <div class="top-form">
                                	<span class="parent_full" style="width:95%; text-align:left;">If you want to change password enter the following details.</span>
                                	<div class="clear"></div>
                                </div><br />
                                <div class="top-form">
                                	<span class="parent_head">Old Password: </span>
                                	<span class="parent_full name">
                                        <input class="field" type="password" name="old_password" placeholder="Enter old Password" />
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">New Password: </span>
                                	<span class="parent_full name">
                                        <input class="field" type="password" name="new_password" placeholder="Enter New Password" />
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="top-form">
                                	<span class="parent_head">Confirm Password: </span>
                                	<span class="parent_full name">
                                        <input class="field" type="password" name="user_con_password" placeholder="Confirm Password" />   
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                </div>
                                <div class="top-form" style="text-align:right;">
                                    <span class="parent_full">
                                		<button class="btn btn-icon submit" type="submit"><span class="icon"></span>Save</button>  
                                    </span>
                                </div>
                            </form>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div> 
                <div class="clear"></div>
            </div>	
        </div>
	    <!-- end div #main --> 
    </div>
	<!-- end div #wrapper -->
    <?php 
	include "templates/footer.php";
	?>
	<script type="text/javascript">
	
	
	
	$(document).ready(function() {
		
		$("#user_name").keyup(function(e) {
			if(($('#user_name').val().length)>3 && !($.isNumeric($('#user_name').val()))) {
				var username = $('#user_name').val();
				$.ajax({
				url: '<?php echo base_url()."citizen/checkusername/"; ?>',
				async: false,
				type: "POST",
				data: {username:username},
				dataType: "html",
				success: function(data) {
						if(data==true)
							$("#availability").html("<span class='green'>Available</span>");
						else
							$("#availability").html("<span class='red'>Not Available</span>");
					}
				});
			}
			else
				$("#availability").html("<span class='red'>Not Available</span>");
		});
	});
	</script>