<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('is_logged_in'))
{
    function is_logged_in()
    {
		$CI =& get_instance();
		if($CI->session->userdata('username') && $CI->session->userdata('userpass') && $CI->session->userdata('userid'))
			return $CI->session->userdata('username');
		else
			return false;
    }  
}

function current_location_updated() {
	$ci =& get_instance();
	if($ci->session->userdata('location_updated')=='updated')
		return 'updated';
	else
		return 'not_updated';
}

function get_categories() {
	$ci =& get_instance();
	$ci->load->database();
	$cat_data = "";
	$cats = $ci->citizenmodel->get_active_data('news_categories','news_cat_status');
	return $cats;
}

function get_tags_from_ids($key = '') {
	$tags_id = explode(',',$key);
	$tag_str = '';
	$ci =& get_instance();
	$ci->load->database();
	foreach($tags_id as $id) {
		$tag = $ci->citizenmodel->get_specific_data_row('tags','TagID',$id);
		if($tag)
			$tag_str = $tag_str . '<li><a href="'.base_url().'citizen/search/tag/'.$tag->TagID.'/'.$tag->TagName.'">'.$tag->TagName.'</a></li>';
	}
	if($tag_str=='')
		return "There are no tags in this post.";
	else
		return $tag_str;
}

function get_all_tags() {
	$ci =& get_instance();
	$ci->load->database();
	//$tags = $ci->citizenmodel->get_active_data('tags','TagStatus');
	$tags = $ci->citizenmodel->get_active_sort_data_limit('tags','TagStatus','TagOccurance','DESC',10);
	return $tags;
}

function social_widget() {
	$social_data = '';
	$social_data = $social_data . '
		<li>
			<a>
				<span class="icon fb"></span><div class="fb-like" data-href="https://www.facebook.com/cithubpk" data-height="" data-colorscheme="light" data-layout="button_count" data-action="like" data-show-faces="true" data-send="false"></div>
			</a>
		</li>
		<li>
			<a href="http://www.twitter.com">
				<span class="icon tw"></span>
				<span>tweet</span>   
			</a>
		</li>';
	return $social_data;
}
function rss_feed_widget($type = '') {
	$ci =& get_instance();
	$rss_data = '
	<li class="third">
		<a href="'.base_url().'rss/index.xml">
			<span class="icon rss"></span><span>subscribe</span>   
		</a>
	</li>';
	return $rss_data;
}
function news_widget() {
	$ci =& get_instance();
	$news_widget = '
	<div class="widget">
		<ul class="nav nav-tabs" id="myTab">
			<li class="active"><a href="#tab-1" data-toggle="tab">latest</a></li>
			<li><a href="#tab-2" data-toggle="tab">popular</a></li>
			<li><a href="#tab-3" data-toggle="tab">commented</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab-1">
				<div class="news">';
	$latest_news = $ci->citizenmodel->get_active_sort_data_limit('news_articles','news_status','upload_time','DESC',5);
	foreach($latest_news as $news) {
		$timestamp = strtotime($news['upload_time']);
		$news_widget = $news_widget . '<div class="item">
				<h3><a href="'.base_url().'citizen/singlepost/science/'.$news['news_category'].'/'.$news['news_id'].'">'.substr($news['news_heading'],0,150).'</a></h3>
				<p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp).' // '.$news['total_comments'].' comments</p>
				<div class="clear"></div>
			</div>';
	}
	$news_widget = $news_widget . '
		</div>
		</div>
		<div class="tab-pane" id="tab-2">
			<div class="news">';
	$hot_news = $ci->citizenmodel->get_active_sort_data_limit('news_articles','news_status','overall_rating','DESC',5);
	foreach($hot_news as $news) {
		$timestamp = strtotime($news['upload_time']);
		$news_widget = $news_widget . '<div class="item">
				<h3><a href="'.base_url().'citizen/singlepost/science/'.$news['news_category'].'/'.$news['news_id'].'">'.substr($news['news_heading'],0,150).'</a></h3>
				<p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp).' // '.$news['total_comments'].' comments</p>
				<div class="clear"></div>
			</div>';
	}
	$news_widget = $news_widget . '
		</div>
		</div>
		<div class="tab-pane" id="tab-3">
        <div class="news">';
	$commented_news = $ci->citizenmodel->get_active_sort_data_limit('news_articles','news_status','total_comments','DESC',5);
	foreach($commented_news as $news) {
		$timestamp = strtotime($news['upload_time']);
		$news_widget = $news_widget . '<div class="item">
				<h3><a href="'.base_url().'citizen/singlepost/science/'.$news['news_category'].'/'.$news['news_id'].'">'.substr($news['news_heading'],0,150).'</a></h3>
				<p class="date">'.date("d", $timestamp).' '.date("F", $timestamp).' '.date("Y", $timestamp).' // '.$news['total_comments'].' comments</p>
				<div class="clear"></div>
			</div>';
	}
	$news_widget = $news_widget . '
		</div>
		</div>
		</div>
		<div class="clear"></div>
	</div>';
	return $news_widget;
}

function get_username($user_id=3) {
	$ci =& get_instance();
	$ci->load->database();
	$user = $ci->citizenmodel->get_specific_data_row('users','user_id',$user_id);
	return $user->UserName;
}

function get_userinfo($user_id=3) {
	$ci =& get_instance();
	$user = $ci->citizenmodel->get_specific_data_row('users','user_id',$user_id);
	return $user;
}