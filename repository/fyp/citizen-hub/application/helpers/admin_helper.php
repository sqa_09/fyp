<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('is_admin_in'))
{
    function is_admin_in()
    {
		$CI =& get_instance();
		if($CI->session->userdata('adminname') && $CI->session->userdata('adminpass') && $CI->session->userdata('adminid'))
			return $CI->session->userdata('adminname');
		else
			return false;
    }  
}

function get_categories() {
	$ci =& get_instance();
	$ci->load->database();
	$cat_data = "";
	$cats = $ci->adminmodel->get_active_data('news_categories','news_cat_status');
	return $cats;
}

function get_tags_from_ids($key = '') {
	$tags_id = explode(',',$key);
	$tag_str = '';
	$ci =& get_instance();
	$ci->load->database();
	foreach($tags_id as $id) {
		$tag = $ci->adminmodel->get_specific_data_row('tags','TagID',$id);
		if($tag)
			$tag_str = $tag_str . '<li><a href="'.base_url().'citizen/search/tag/'.$tag->TagID.'/'.$tag->TagName.'">'.$tag->TagName.'</a></li>';
	}
	if($tag_str=='')
		return "There are no tags in this post.";
	else
		return $tag_str;
}

function get_all_tags() {
	$ci =& get_instance();
	$ci->load->database();
	//$tags = $ci->citizenmodel->get_active_data('tags','TagStatus');
	$tags = $ci->adminmodel->get_active_sort_data_limit('tags','TagStatus','TagOccurance','DESC',10);
	return $tags;
}