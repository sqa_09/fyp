<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Plugin jRating : Ajax rating system with jQuery</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="jquery/jRating.jquery.css" type="text/css" />
</head>
<body>

<div class="exemple">
	<div class="exemple5" data-average="5" data-id="5"></div>
</div>
<div class="datasSent">
	Datas sent to the server :
	<p></p>
</div>
<div class="serverResponse">
	Server response :
	<p></p>
</div>
<?php $a = true;?>
	<script type="text/javascript" src="jquery/jquery.js"></script>
	<script type="text/javascript" src="jquery/jRating.jquery.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.exemple5').jRating({
				length:5,
				decimalLength:1,
				rateMax:10,
				isDisabled:false
				/*onSuccess : function(){
					alert('Success : your rate has been saved :)');
				},
				onError : function(){
					alert('Error : please retry');
				}*/
			});
		});
	</script>
</body>
</html>
