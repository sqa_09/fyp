/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import junit.framework.TestCase;

/**
 *
 * @author nb
 */
public class VectorsJUnit3Test extends TestCase {

    public VectorsJUnit3Test(String testName) {
        super(testName);
    }

    /**
     * Test of equal method, of class Vectors.
     */
    public void testEqual() {
        System.out.println("* VectorsJUnit3Test: testEqual()");
        assertTrue(Vectors.equal(new int[]{}, new int[]{}));
        assertTrue(Vectors.equal(new int[]{0}, new int[]{0}));
        assertTrue(Vectors.equal(new int[]{0, 0}, new int[]{0, 0}));
        assertTrue(Vectors.equal(new int[]{0, 0, 0}, new int[]{0, 0, 0}));
        assertTrue(Vectors.equal(new int[]{5, 6, 7}, new int[]{5, 6, 7}));
        assertTrue(Vectors.equal(new int[]{15, 16, 37}, new int[]{35, 45, 7}));
        assertTrue(Vectors.equal(new int[]{3, 8, 7}, new int[]{15, 36, 87}));
        assertTrue(Vectors.equal(new int[]{90, 16, 12}, new int[]{8, 2, 1}));
        assertTrue(Vectors.equal(new int[]{9, 8, 7}, new int[]{5, 6, 12}));

        assertFalse(Vectors.equal(new int[]{0, 0, 0}, new int[]{0, 0, 1}));
        assertFalse(Vectors.equal(new int[]{0, 0, 0}, new int[]{0, 1, 0}));
        assertFalse(Vectors.equal(new int[]{0, 0, 0}, new int[]{1, 0, 0}));
        assertFalse(Vectors.equal(new int[]{0, 0, 1}, new int[]{0, 0, 3}));
    }

    /**
     * Test of scalarMultiplication method, of class Vectors.
     */
    public void testScalarMultiplication() {
        System.out.println("* VectorsJUnit3Test: testScalarMultiplication()");
        assertEquals(0, Vectors.scalarMultiplication(new int[]{0, 0}, new int[]{0, 0}));
        assertEquals(39, Vectors.scalarMultiplication(new int[]{3, 4}, new int[]{5, 6}));
        assertEquals(-39, Vectors.scalarMultiplication(new int[]{-3, 4}, new int[]{5, -6}));
        assertEquals(0, Vectors.scalarMultiplication(new int[]{5, 9}, new int[]{-9, 5}));
        assertEquals(100, Vectors.scalarMultiplication(new int[]{-6, -8}, new int[]{6, 8}));
        assertEquals(50, Vectors.scalarMultiplication(new int[]{10, 18}, new int[]{6, 8}));
        assertEquals(100, Vectors.scalarMultiplication(new int[]{24, 58}, new int[]{6, 8}));
        assertEquals(20, Vectors.scalarMultiplication(new int[]{106, 81}, new int[]{6, 8}));

    }
}
