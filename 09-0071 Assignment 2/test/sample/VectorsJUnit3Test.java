/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import junit.framework.TestCase;

/**
 *
 * @author nb
 */
public class VectorsJUnit3Test extends TestCase {

    public VectorsJUnit3Test(String testName) {
        super(testName);
    }

    /**
     * Test of equal method, of class Vectors.
     */
    public void testEqual() {
        System.out.println("* VectorsJUnit3Test: testEqual()");
        assertTrue(Vectors.equal(new int[]{}, new int[]{}));
        assertTrue(Vectors.equal(new int[]{0, 0}, new int[]{0, 0}));
        assertTrue(Vectors.equal(new int[]{0}, new int[]{0}));
        assertTrue(Vectors.equal(new int[]{0, 0, 0}, new int[]{0, 0, 0}));
        assertTrue(Vectors.equal(new int[]{1, 4, 8}, new int[]{1, 4, 8}));
        assertTrue(Vectors.equal(new int[]{78, 22, 45}, new int[]{46, 28, 17}));
        assertTrue(Vectors.equal(new int[]{9, 1, 5}, new int[]{24, 72, 65}));
        assertTrue(Vectors.equal(new int[]{12, 12, 12}, new int[]{8, 8, 0}));
        assertTrue(Vectors.equal(new int[]{450, 5, 99}, new int[]{5, 60, 120}));

        assertFalse(Vectors.equal(new int[]{0, 0, 1}, new int[]{0, 0, 6}));
        assertFalse(Vectors.equal(new int[]{0, 0, 0}, new int[]{0, 9, 0}));
        assertFalse(Vectors.equal(new int[]{0, 0, 0}, new int[]{2, 0, 0}));
        assertFalse(Vectors.equal(new int[]{0, 0, 0}, new int[]{0, 0, 4}));
    }

    /**
     * Test of scalarMultiplication method, of class Vectors.
     */
    public void testScalarMultiplication() {
        System.out.println("* VectorsJUnit3Test: testScalarMultiplication()");
        assertEquals(0, Vectors.scalarMultiplication(new int[]{0, 0}, new int[]{0, 0}));
        assertEquals(39, Vectors.scalarMultiplication(new int[]{4, 1}, new int[]{9, 5}));
        assertEquals(-39, Vectors.scalarMultiplication(new int[]{-2, 6}, new int[]{3, -7}));
        assertEquals(0, Vectors.scalarMultiplication(new int[]{1, 7}, new int[]{-8, 3}));
        assertEquals(100, Vectors.scalarMultiplication(new int[]{-5, -3}, new int[]{4, 3}));
        assertEquals(50, Vectors.scalarMultiplication(new int[]{19, 28}, new int[]{9, 2}));
        assertEquals(100, Vectors.scalarMultiplication(new int[]{24, 67}, new int[]{2, 4}));
        assertEquals(20, Vectors.scalarMultiplication(new int[]{222, 440}, new int[]{4, 2}));

    }
}
